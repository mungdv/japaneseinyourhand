﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JYHWebUI.Models.ViewRequest;
using JYHWebUI.Handlers;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Controllers
{
    public class BaseController : NormalController
    {
        UserHandler userHandler = new UserHandler();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);     
            if (Session[Common.Common.user] == null)
            {
                filterContext.Result = new RedirectResult(Url.Action("Login", "Account"));
            }
        }
    }
}