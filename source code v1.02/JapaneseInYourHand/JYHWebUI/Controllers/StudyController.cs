﻿using JYHWebUI.Handlers;
using JYHWebUI.Models.DTOs;
using JYHWebUI.Models.ViewRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JYHWebUI.Controllers
{
    public class StudyController : BaseController
    {
        CourseHandler courseHandler = new CourseHandler();

        LessonHandler lessonHandler = new LessonHandler();
        // GET: Study
        public ActionResult Index()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            ViewBag.CourseList = courseHandler.Get(session.ID);
            return View();
        }

        [HttpPost]
        public ActionResult Course(int courseID)
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            ViewBag.Course = courseHandler.CourseDetail(courseID, session.ID);
            return View("Course");
        }

        [HttpPost]
        public ActionResult Lesson(int lessonID, int courseID)
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            ViewBag.LessonID = lessonID;
            ViewBag.CourseID = courseID;
            ViewBag.LessonCulture = new LessonHandler().GetCulture(lessonID);
            ViewBag.LessonAudio = new LessonHandler().GetAudio(lessonID);
            ViewBag.LessonDialogue = new LessonHandler().GetDialogue(lessonID);
            ViewBag.LessonKanji = new LessonKanjiHandler().Get(lessonID);
            ViewBag.LessonWord = new LessonWordHandler().Get(lessonID);
            ViewBag.LessonGrammar = new LessonGrammarHandler().Get(lessonID);
            ViewBag.LessonQuiz = new LessonQuizHandler().Get(lessonID);
            return View("Lesson");
        }

        [HttpPost]
        public ActionResult LessonKanji(int lessonID)
        {
           ViewBag.LessonKanji = new LessonKanjiHandler().Get(lessonID);
            return View();
        }

        [HttpPost]
        public ActionResult LessonGrammar(int lessonID)
        {
            ViewBag.LessonGrammar = new LessonGrammarHandler().Get(lessonID);
            return View();
        }

        [HttpPost]
        public ActionResult LessonWord(int lessonID)
        {
            ViewBag.LessonWord = new LessonWordHandler().Get(lessonID);
            return View();
        }

        [HttpPost]
        public ActionResult LessonQuiz(int lessonID)
        {
            ViewBag.LessonQuiz = new LessonQuizHandler().Get(lessonID);
            return View();
        }

        [HttpPost]
        public ActionResult LessonCulture(int lessonID)
        {
            ViewBag.LessonCulture = new LessonHandler().GetCulture(lessonID);
            return View();
        }

        [HttpPost]
        public ActionResult LessonAudio(int lessonID)
        {
            ViewBag.LessonAudio = new LessonHandler().GetAudio(lessonID);
            return View();
        }

        [HttpPost]
        public void AddUserKanji(List<int> listKanji, int lessonID)
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            //add new user lesson
            LessonDto lesson = new LessonHandler().LessonDetail(lessonID);
            UserCourseHandler userCourseHandler = new UserCourseHandler();
            if(!userCourseHandler.CheckExist(lesson.CourseID, session.ID))
            {
                UserCourseDto userCourseDto = new UserCourseDto { CourseID = lesson.CourseID, UserID = session.ID};
                userCourseHandler.Add(userCourseDto);
            }

            UserLessonHandler userLessonHandler = new UserLessonHandler();
            if (!userLessonHandler.CheckExist(session.ID, lessonID))
            {
                UserLessonDto userLessonDto = new UserLessonDto { LessonID = lessonID, UserID = session.ID, Progress = -1, Score = -1 };
                userLessonHandler.Add(userLessonDto);
            }
            //end add
            UserKanjiHandler userKanjiHandler = new UserKanjiHandler();
            // listKanji would be null if user answers all questions incorrectly -> exception thrown
            try
            {
                foreach (int kanjiID in listKanji)
                {
                    UserKanjiDto userKanjiDto = new UserKanjiDto();
                    userKanjiDto.KanjiID = kanjiID;
                    userKanjiDto.UserID = session.ID;
                    userKanjiHandler.Add(userKanjiDto);
                }
            }
            catch (Exception ex)
            {

            }
            userLessonHandler.CaculatorProgress(session.ID, lessonID);
            UserStatHandler userStatHandler = new UserStatHandler();
            var userStat = userStatHandler.Get(session.ID);
            try
            {
                userStat.CorrectKanji += listKanji.Count;
                userStat.WrongKanji += new LessonKanjiHandler().Get(lessonID).Count - listKanji.Count;
            }
            catch (Exception ex)
            {
                userStat.WrongKanji += new LessonKanjiHandler().Get(lessonID).Count;
            }
            
            userStatHandler.Update(userStat);
        }

        [HttpPost]
        public void AddUserWord(List<int> listWord, int lessonID)
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            UserWordHandler userWordHandler = new UserWordHandler();
            //add new user lesson
            LessonDto lesson = new LessonHandler().LessonDetail(lessonID);
            UserCourseHandler userCourseHandler = new UserCourseHandler();
            if (!userCourseHandler.CheckExist(lesson.CourseID, session.ID))
            {
                UserCourseDto userCourseDto = new UserCourseDto { CourseID = lesson.CourseID, UserID = session.ID };
                userCourseHandler.Add(userCourseDto);
            }
            UserLessonHandler userLessonHandler = new UserLessonHandler();
            if (!userLessonHandler.CheckExist(session.ID, lessonID))
            {
                UserLessonDto userLessonDto = new UserLessonDto { LessonID = lessonID, UserID = session.ID, Progress = -1, Score = -1 };
                userLessonHandler.Add(userLessonDto);
            }
            //end add
            try { 
                foreach (int wordID in listWord)
                {
                    UserWordDto userWordDto = new UserWordDto();
                    userWordDto.WordID = wordID;
                    userWordDto.UserID = session.ID;
                    userWordHandler.Add(userWordDto);
                }
            }
            catch (Exception ex)
            {

            }
            userLessonHandler.CaculatorProgress(session.ID, lessonID);

            UserStatHandler userStatHandler = new UserStatHandler();
            var userStat = userStatHandler.Get(session.ID);
            try {
                userStat.CorrectWord += listWord.Count;
                userStat.WrongWord += new LessonWordHandler().Get(lessonID).Count - listWord.Count;
            }
            catch (Exception ex)
            {
                userStat.WrongWord += new LessonWordHandler().Get(lessonID).Count;
            }
            userStatHandler.Update(userStat);
        }

        [HttpPost]
        public void AddUserGrammar(int grammarID, int lessonID)
        {

            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;

            //add new user lesson
            LessonDto lesson = new LessonHandler().LessonDetail(lessonID);
            UserCourseHandler userCourseHandler = new UserCourseHandler();
            if (!userCourseHandler.CheckExist(lesson.CourseID, session.ID))
            {
                UserCourseDto userCourseDto = new UserCourseDto { CourseID = lesson.CourseID, UserID = session.ID };
                userCourseHandler.Add(userCourseDto);
            }

            UserLessonHandler userLessonHandler = new UserLessonHandler();
            if (!userLessonHandler.CheckExist(session.ID, lessonID))
            {
                UserLessonDto userLessonDto = new UserLessonDto { LessonID = lessonID, UserID = session.ID, Progress = -1, Score = -1 };
                userLessonHandler.Add(userLessonDto);
            }
            //end add

            UserGrammarDto userGrammarDto = new UserGrammarDto();
            userGrammarDto.GrammarID = grammarID;
            userGrammarDto.UserID = session.ID;
            userLessonHandler.CaculatorProgress(session.ID, lessonID);
            new UserGrammarHandler().Add(userGrammarDto);

            UserStatHandler userStatHandler = new UserStatHandler();
            var userStat = userStatHandler.Get(session.ID);
            userStat.CorrectGrammar += 1;
            userStatHandler.Update(userStat);
        }
        [HttpPost]
        public void UpdateScore(List<int> listQuizCorrect, int lessonID)
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;

            //add new user lesson
            LessonDto lesson = new LessonHandler().LessonDetail(lessonID);
            UserCourseHandler userCourseHandler = new UserCourseHandler();
            if (!userCourseHandler.CheckExist(lesson.CourseID, session.ID))
            {
                UserCourseDto userCourseDto = new UserCourseDto { CourseID = lesson.CourseID, UserID = session.ID };
                userCourseHandler.Add(userCourseDto);
            }

            UserLessonHandler userLessonHandler1 = new UserLessonHandler();
            if (!userLessonHandler1.CheckExist(session.ID, lessonID))
            {
                UserLessonDto userLessonDto1 = new UserLessonDto { LessonID = lessonID, UserID = session.ID, Progress = -1, Score = -1 };
                userLessonHandler1.Add(userLessonDto1);
            }
            //end add

            
            UserStatHandler userStatHandler = new UserStatHandler();
            var userStat = userStatHandler.Get(session.ID);
            List<LessonQuizDto> list = new LessonQuizHandler().Get(lessonID);
            foreach(LessonQuizDto lessonQ in list)
            {
                switch(lessonQ.Type)
                {
                    case 1:
                        if(listQuizCorrect.Contains(lessonQ.ID))
                        {
                            userStat.CorrectKanji += 1;
                        }
                        else {
                            userStat.WrongKanji += 1;
                        }
                        break;
                    case 2:
                        if (listQuizCorrect.Contains(lessonQ.ID))
                        {
                            userStat.CorrectWord += 1;
                        }
                        else { 
                            userStat.WrongWord += 1;
                        }
                        break;
                    case 3:
                        if (listQuizCorrect.Contains(lessonQ.ID))
                        {
                            userStat.CorrectGrammar += 1;
                        }
                        else {
                            userStat.WrongGrammar += 1;
                        }
                        break;
                    case 4:
                        if (listQuizCorrect.Contains(lessonQ.ID))
                        {
                            userStat.CorrectReading += 1;
                        }
                        else {
                            userStat.WrongReading += 1;
                        }
                        break;
                    case 5:
                        if (listQuizCorrect.Contains(lessonQ.ID))
                        {
                            userStat.CorrectListening += 1;
                        }
                        else {
                            userStat.WrongListening += 1;
                        }
                        break;
                    default:
                        break;

                }
            }
            userStatHandler.Update(userStat);

            UserLessonDto userLessonDto = new UserLessonDto {
                UserID = session.ID,
                Score = (listQuizCorrect != null && list.Count > 0) ? (listQuizCorrect.Count * 100) / list.Count : 0,
                LessonID = lessonID
            };
            UserLessonHandler userLessonHandler = new UserLessonHandler();
            userLessonHandler.Update(userLessonDto);
            userLessonHandler.CaculatorProgress(session.ID, lessonID);
        }

        [HttpPost]
        public void UpdateTime()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            if(session != null)
            {
                new UserStatHandler().UpdateTime(session.ID);
            }
            
        }

        public ActionResult LearnConjugate()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            return View();
        }

        public ActionResult PracticeConjugate(int type)
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            ViewBag.ConjugType = type;
            if (type == 1)
            {
                ViewBag.VName = "THỂ LỊCH SỰ";
                ViewBag.JName = "ます<ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 2)
            {
                ViewBag.VName = "THỂ て";
                ViewBag.JName = "て<ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 3)
            {
                ViewBag.VName = "THỂ QUÁ KHỨ";
                ViewBag.JName = "た<ruby>形<rt>けい</rt></ruby>・<ruby>過<rt>か</rt></ruby><ruby>去<rt>こ</rt></ruby><ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 4)
            {
                ViewBag.VName = "THỂ PHỦ ĐỊNH";
                ViewBag.JName = "ない<ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 5)
            {
                ViewBag.VName = "THỂ ĐIỀU KIỆN";
                ViewBag.JName = "<ruby>条<rt>じょう</rt></ruby><ruby>件<rt>けん</rt></ruby><ruby>形<rt>けい</rt></ruby>・ば<ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 6)
            {
                ViewBag.VName = "THỂ KHẢ NĂNG";
                ViewBag.JName = "<ruby>可<rt>か</rt></ruby><ruby>能<rt>のう</rt></ruby><ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 7)
            {
                ViewBag.VName = "THỂ Ý HƯỚNG";
                ViewBag.JName = "<ruby>意<rt>い</rt></ruby><ruby>向<rt>こう</rt></ruby><ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 8)
            {
                ViewBag.VName = "THỂ MỆNH LỆNH";
                ViewBag.JName = "<ruby>命<rt>めい</rt></ruby><ruby>令<rt>れい</rt></ruby><ruby>形<rt>けい</rt></ruby>";
            }
            else if (type == 9)
            {
                ViewBag.VName = "THỂ BỊ ĐỘNG / TÔN KÍNH";
                ViewBag.JName = "<ruby>受<rt>うけ</rt></ruby><ruby>身<rt>み</rt></ruby>・<ruby>尊<rt>そん</rt></ruby><ruby>敬<rt>けい</rt></ruby>";
            }
            else if (type == 10)
            {
                ViewBag.VName = "THỂ SAI KHIẾN";
                ViewBag.JName = "<ruby>使<rt>し</rt></ruby><ruby>役<rt>えき</rt></ruby>";
            }
            return View();
        }

        public JsonResult Conjugate(string word, int wordType, int conjugType)
        {
            return Json(new WordHandler().Conjugate(word, wordType, conjugType), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRandomVerb()
        {
            var list = new WordHandler().GetRandomVerb();
            int index = new Random().Next(list.Count);
            return Json(list[index], JsonRequestBehavior.AllowGet);
        }

        public JsonResult AnswerConjugPractice(string word, string reading, int wordType, int conjugType, string inp)
        {
            return Json(new WordHandler().AnswerConjugPractice(word, reading, wordType, conjugType, inp), JsonRequestBehavior.AllowGet);
        }
    }
}