﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JYHWebUI.Models.ViewRequest;
using JYHWebUI.Handlers;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Controllers
{
    public class NormalController : Controller
    {
        UserHandler userHandler = new UserHandler();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            UserDto user = HttpContext.Session[Common.Common.user] as UserDto;
            if (user == null)
            {
                HttpCookie aCookie = Request.Cookies[Common.Common.account];
                HttpCookie iCookie = Request.Cookies[Common.Common.identifier];
                HttpCookie tCookie = Request.Cookies[Common.Common.token];

                if (aCookie != null && iCookie != null && tCookie != null)
                {
                    CookieDto cookie = new CookieDto();
                    cookie.Account = aCookie.Value;
                    cookie.IDEC = iCookie.Value;
                    cookie.token = tCookie.Value;
                    if (userHandler.CheckSession(cookie))
                    {
                        user = userHandler.Get(cookie.Account);
                        if (user != null)
                            Session[Common.Common.user] = user;
                            UserStatHandler userStatHandler = new UserStatHandler();
                            if (userStatHandler.Get(user.ID) == null)
                            {
                                userStatHandler.Add(user.ID);
                            }
                        }
                    }
             }

            if(user != null)
            {
                if(string.IsNullOrEmpty(user.FullName))
                {
                    ViewBag.username = user.Email;
                }else
                {
                    ViewBag.username = user.FullName;
                }
                var userRole = new UserRoleHandler().Get(user.ID);
                if (userRole == null || userRole.Count == 0)
                {
                    ViewBag.Role = 0;
                }else
                {
                    ViewBag.Role = 1;
                }
            }
        }
    }
}