﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JYHWebUI.Handlers;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Controllers
{
    public class AutoCompleteController : Controller
    {
        [HttpPost]
        public JsonResult Index(string Prefix, int type)
        {
            if(type == 1)
            {
                return Json(new KanjiHandler().Get(Prefix), JsonRequestBehavior.AllowGet);
            }
            else if(type == 2)
            {
                return Json(new WordHandler().Get(Prefix), JsonRequestBehavior.AllowGet);
            }else if (type == 3)
            {
                return Json(new GrammarHandler().Get(Prefix), JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new LessonQuizHandler().GetQuiz(Int32.Parse(Prefix)), JsonRequestBehavior.AllowGet);
        }


    }
}