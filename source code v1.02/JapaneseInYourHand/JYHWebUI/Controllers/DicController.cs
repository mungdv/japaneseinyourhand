﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JYHWebUI.Handlers;
using JYHWebUI.Models.Dic;

namespace JYHWebUI.Controllers
{
    public class DicController : Controller
    {
        // GET: Dic
        public ActionResult Index()
        {
            return View();
        }

        #region dictonary
        [HttpGet]
        public JsonResult DicSearch(string textSearch)
        {
            Dictionary dic = new Dictionary();
            dic.ListWord = new WordHandler().DicSearch(textSearch);
            dic.ListKanji = new KanjiHandler().DicSearch(textSearch);
            dic.ListGrammar = new GrammarHandler().DicSearch(textSearch);
            return Json(dic, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DicSearchByID(int id, int type)
        {
            switch(type)
            {
                //search theo id len se khong co search all
                /*case 0:
                    Dictionary dic = new Dictionary();
                    dic.ListWord = new WordHandler().DicSearch(id);
                    dic.ListKanji = new KanjiHandler().DicSearch(id);
                    dic.ListGrammar = new GrammarHandler().DicSearch(id);
                    return Json(dic, JsonRequestBehavior.AllowGet);*/
                case 1:
                    ViewBag.WordDetail = new WordHandler().DicSearch(id);
                    return PartialView("WordDetail");
                case 2:
                    ViewBag.KanjiDetail = new KanjiHandler().DicSearch(id);
                    return PartialView("KanjiDetail");
                case 3:
                    ViewBag.GrammarDetail = new GrammarHandler().DicSearch(id);
                    return PartialView("GrammarDetail");
                default:
                    break;
            }
            return PartialView();
        }

        #endregion dictonary
    }
}