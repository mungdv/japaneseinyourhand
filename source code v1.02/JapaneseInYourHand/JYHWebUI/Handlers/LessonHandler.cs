﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.DbService;
using JYHWebUI.Models.DTOs;
using JYHDataAccess.Entities;
using System.Data.Entity;


namespace JYHWebUI.Handlers
{
    public class LessonHandler
    {
        JYHDbContext _jyhDbContext;
        public LessonHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public LessonHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public bool CheckExist(string name, int courseID)
        {
            if (_jyhDbContext.Lessons.Any(l => (l.Name.Equals(name)) && (l.CourseID == courseID)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckExist(int id)
        {
            if (_jyhDbContext.Lessons.Any(l => (l.ID == id)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //this function use to check when user modify a lesson
        public bool CheckExistOtherName(LessonDto lessonDto)
        {
            Lesson lesson = _jyhDbContext.Lessons.FirstOrDefault(l => (l.ID == lessonDto.ID) && (l.CourseID == lessonDto.CourseID));
            var listlessonWithModifyName = _jyhDbContext.Lessons.ToList().Where(l => (l.Name.Equals(lessonDto.Name)) &&(l.CourseID == lessonDto.CourseID));
            bool checkNameExist = false;
            foreach (Lesson l in listlessonWithModifyName)
            {
                if (l.ID != lesson.ID)
                {
                    checkNameExist = true;
                }
            }
            if (checkNameExist == true)
            {
                return true;
            }
            return false;
        }

        public List<LessonDto> Get()
        {
            try
            {
                var listLesson = _jyhDbContext.Lessons.ToList().Where(l => l.Available == true).Select(l => new LessonDto
                {
                    ID = l.ID,
                    CourseID = l.CourseID,
                    CultureNotes = l.CultureNotes,
                    Description = l.Description,
                    Dialogue = l.Dialogue,
                    DialogueAudio = l.DialogueAudio,
                    Name = l.Name,
                    Image = l.Image
                }).ToList();
                if(listLesson == null)
                {
                    return new List<LessonDto>();
                }
                return listLesson;
            }
            catch
            {
                return new List<LessonDto>();
            }
        }

        public List<LessonDto> Get(int courseID)
        {
            try
            {
                var listLesson = _jyhDbContext.Lessons.ToList().Where(l => l.CourseID == courseID && l.Available == true).Select(l => new LessonDto
                {
                    ID = l.ID,
                    CourseID = l.CourseID,
                    CultureNotes = l.CultureNotes,
                    Description = l.Description,
                    Dialogue = l.Dialogue,
                    DialogueAudio = l.DialogueAudio,
                    Name = l.Name,
                    Image = l.Image
                }).ToList();
                if (listLesson == null)
                {
                    return new List<LessonDto>();
                }
                return listLesson;
            }
            catch
            {
                return new List<LessonDto>();
            }
        }

        public string GetNumberLearned(int lessonID, int userID)
        {
            var lesson = new UserLessonHandler().GetUserLesson(userID, lessonID);
            if(lesson == null)
            {
                return "You can start this lesson";
            }
            try
            {
                string s = new UserKanjiHandler().Get(userID, lessonID).Count + new LessonKanjiHandler().Get(lessonID).ToList().Count + "\b"
                     + new UserWordHandler().Get(userID, lessonID).Count + new LessonWordHandler().Get(lessonID).ToList().Count + "\b"
                     + new UserGrammarHandler().Get(userID, lessonID).Count + new LessonGrammarHandler().Get(lessonID).ToList().Count;
                return s;
            }catch (Exception e)
            {
                return "Error: " + e.Message;
            }
            
        }

        public List<LessonDto> Get(int courseID, int userID)
        {
            try
            {
                var list = (
                from lessons in _jyhDbContext.Lessons.Where(l => l.CourseID == courseID && l.Available == true)
                join userlesson in _jyhDbContext.UserLessons.Where(ul => ul.UserID == userID)
                on lessons.ID equals userlesson.LessonID into gj
                from x in gj.DefaultIfEmpty()
                select new LessonDto
                {
                    ID = lessons.ID,
                    CourseID = lessons.CourseID,
                    CultureNotes = lessons.CultureNotes,
                    Description = lessons.Description,
                    Dialogue = lessons.Dialogue,
                    DialogueAudio = lessons.DialogueAudio,
                    Name = lessons.Name,
                    Image = lessons.Image,
                    Progress = (x == null ? -1 : x.Progress),
                    Score = (x == null ? -1 : x.Score),
                }).ToList();
                foreach (LessonDto lesson in list)
                {
                    lesson.LessonKanjis = new LessonKanjiHandler().Get(lesson.ID);
                    lesson.LessonGrammars = new LessonGrammarHandler().Get(lesson.ID);
                    lesson.LessonWords = new LessonWordHandler().Get(lesson.ID);
                    if(lesson.Progress != -1)
                    {
                        lesson.UserKanjis = new UserKanjiHandler().Get(userID, lesson.ID);
                        lesson.UserGrammars = new UserGrammarHandler().Get(userID, lesson.ID);
                        lesson.UserWords = new UserWordHandler().Get(userID, lesson.ID);
                    }
                }
                if (list == null)
                {
                    return new List<LessonDto>();
                }
                return list;
            }
            catch
            {
                return new List<LessonDto>();
            }
        }

        public List<LessonDto> GetD(int courseID)
        {
            try
            {
                var listLesson = _jyhDbContext.Lessons.ToList().Where(l => l.CourseID == courseID && l.Available == true).Select(l => new LessonDto
                {
                    ID = l.ID,
                    CourseID = l.CourseID,
                    CultureNotes = l.CultureNotes,
                    Description = l.Description,
                    Dialogue = l.Dialogue,
                    DialogueAudio = l.DialogueAudio,
                    Name = l.Name,
                    Image = l.Image,
                    LessonWords = new LessonWordHandler().Get(l.ID).ToList(),
                    LessonKanjis = new LessonKanjiHandler().Get(l.ID).ToList(),
                    LessonGrammars = new LessonGrammarHandler().Get(l.ID).ToList(),
                    LessonQuizs = new LessonQuizHandler().Get(l.ID).ToList()
                }).ToList();
                if (listLesson == null)
                {
                    return new List<LessonDto>();
                }


                return listLesson;
            }
            catch
            {
                return new List<LessonDto>();
            }
        }

        public LessonDto LessonDetail(int lessonID)
        {
            try
            {
                LessonDto lesson = _jyhDbContext.Lessons.Where(t => t.ID == lessonID && t.Available == true).Select(l => new LessonDto
                {
                    ID = l.ID,
                    CourseID = l.CourseID,
                    CultureNotes = l.CultureNotes,
                    Description = l.Description,
                    Dialogue = l.Dialogue,
                    DialogueAudio = l.DialogueAudio,
                    Name = l.Name,
                    Image = l.Image

                }).FirstOrDefault();
                if(lesson == null)
                {
                    return new LessonDto();
                }
                return lesson;
            }
            catch
            {
                return new LessonDto();
            }
        }

        public LessonDto LessonDetailD(int lessonID)
        {
            try
            {
                Lesson l = _jyhDbContext.Lessons.FirstOrDefault(t => t.ID == lessonID && t.Available == true);
                if (l == null)
                {
                    return new LessonDto();
                }
                LessonDto a = new LessonDto()
                {
                    ID = l.ID,
                    CourseID = l.CourseID,
                    CultureNotes = l.CultureNotes,
                    Description = l.Description,
                    Dialogue = l.Dialogue,
                    DialogueAudio = l.DialogueAudio,
                    Name = l.Name,
                    Image = l.Image,
                    LessonWords = new LessonWordHandler().Get(l.ID).ToList(),
                    LessonKanjis = new LessonKanjiHandler().Get(l.ID).ToList(),
                    LessonGrammars = new LessonGrammarHandler().Get(l.ID).ToList(),
                    LessonQuizs = new LessonQuizHandler().Get(l.ID).ToList()
                };


               
                return a;
            }
            catch
            {
                return new LessonDto();
            }
        }

        public bool Add(LessonDto lessonDto)
        {
            if (CheckExist(lessonDto.Name, lessonDto.CourseID))
            {
                return false;
            }
            Lesson lesson = new Lesson
            {
                Name = lessonDto.Name,
                CourseID = lessonDto.CourseID,
                Description = lessonDto.Description,
                Dialogue = lessonDto.Dialogue,
                DialogueAudio = lessonDto.DialogueAudio,
                CultureNotes = lessonDto.CultureNotes,
                Available = true

            };
            _jyhDbContext.Lessons.Add(lesson);
            _jyhDbContext.SaveChanges();
            return true;

        }

        public bool Update(LessonDto lessonDto)
        {
            if (!CheckExist(lessonDto.ID))
            {
                return false;
            }
            Lesson lesson = _jyhDbContext.Lessons.FirstOrDefault(c => c.ID == lessonDto.ID);
            lesson.Name = lessonDto.Name;
            lesson.Description = lessonDto.Description;
            lesson.Dialogue = lessonDto.Dialogue;
            lesson.DialogueAudio = lessonDto.DialogueAudio;
            lesson.CultureNotes = lessonDto.CultureNotes;
            _jyhDbContext.Entry(lesson).State = EntityState.Modified;
            _jyhDbContext.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (!CheckExist(id))
            {
                return false;
            }
            Lesson lesson = _jyhDbContext.Lessons.FirstOrDefault(l => l.ID == id);
            lesson.Available = false;
            _jyhDbContext.Entry(lesson).State = EntityState.Modified;
            _jyhDbContext.SaveChanges();
            return true;
        }

        public string GetCulture(int lessonID)
        {
            if (!CheckExist(lessonID))
            {
                return "";
            }
            try
            {
                var lesson = _jyhDbContext.Lessons.Where(l => l.ID == lessonID && l.Available == true).FirstOrDefault();
                if(lesson == null)
                {
                    return "";
                }
                return lesson.CultureNotes;
            }catch
            {
                return "";
            }
            
        }

        public string GetAudio(int lessonID)
        {
            if (!CheckExist(lessonID))
            {
                return "";
            }
            try
            {
                var lesson = _jyhDbContext.Lessons.Where(l => l.ID == lessonID && l.Available == true).FirstOrDefault();
                if (lesson == null)
                {
                    return "";
                }
                return lesson.DialogueAudio;
            }
            catch
            {
                return "";
            }

        }

        public string GetDialogue(int lessonID)
        {
            if (!CheckExist(lessonID))
            {
                return "";
            }
            try
            {
                var lesson = _jyhDbContext.Lessons.Where(l => l.ID == lessonID && l.Available == true).FirstOrDefault();
                if (lesson == null)
                {
                    return "";
                }
                return lesson.Dialogue;
            }
            catch
            {
                return "";
            }

        }
    }
}