﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHWebUI.Models.ViewRequest;

namespace JYHWebUI.Handlers
{
    public class AlphabetHandler
    {
        private List<string> listAlphabet;
        public AlphabetHandler ()
        {
            Initialize();
        }

        private void Initialize()
        {
            listAlphabet = new List<string>( new string[] {
                "あ", "い", "う", "え", "お",
                "か", "き", "く", "け", "こ",
                "さ", "し", "す", "せ", "そ",
                "た", "ち", "つ", "て", "と",
                "な", "に", "ぬ", "ね", "の",
                "は", "ひ", "ふ", "へ", "ほ",
                "ま", "み", "む", "め", "も",
                "や", "", "ゆ", "", "よ",
                "ら", "り", "る", "れ", "ろ",
                "わ", "", "", "", "を",
                "ん", "", "", "", "",
                "が", "ぎ", "ぐ", "げ", "ご",
                "ざ", "じ", "ず", "ぜ", "ぞ",
                "だ", "ぢ", "づ", "で", "ど",
                "ば", "び", "ぶ", "べ", "ぼ",
                "ぱ", "ぴ", "ぷ", "ぺ", "ぽ",
                "", "", "",
                "きゃ", "きゅ", "きょ",
                "しゃ", "しゅ", "しょ",
                "ちゃ", "ちゅ", "ちょ",
                "にゃ", "にゅ", "にょ",
                "ひゃ", "ひゅ", "ひょ",
                "みゃ", "みゅ", "みょ",
                "", "", "",
                "りゃ", "りゅ", "りょ",
                "", "", "",
                "", "", "",
                "ぎゃ", "ぎゅ", "ぎょ",
                "じゃ", "じゅ", "じょ",
                "", "", "",
                "びゃ", "びゅ", "びょ",
                "ぴゃ", "ぴゅ", "ぴょ",

                "ア", "イ", "ウ", "エ", "オ",
                "カ", "キ", "ク", "ケ", "コ",
                "サ", "シ", "ス", "セ", "ソ",
                "タ", "チ", "ツ", "テ", "ト",
                "ナ", "ニ", "ヌ", "ネ", "ノ",
                "ハ", "ヒ", "フ", "ヘ", "ホ",
                "マ", "ミ", "ム", "メ", "モ",
                "ヤ", "", "ユ", "", "ヨ",
                "ラ", "リ", "ル", "レ", "ロ",
                "ワ", "", "", "", "ヲ",
                "ン", "", "", "", "",
                "ガ", "ギ", "グ", "ゲ", "ゴ",
                "ザ", "ジ", "ズ", "ゼ", "ゾ",
                "ダ", "ヂ", "ヅ", "デ", "ド",
                "バ", "ビ", "ブ", "ベ", "ボ",
                "パ", "ピ", "プ", "ペ", "ポ",
                "", "", "",
                "キャ", "キュ", "キョ",
                "シャ", "シュ", "ショ",
                "チャ", "チュ", "チョ",
                "ニャ", "ニュ", "ニョ",
                "ヒャ", "ヒュ", "ヒョ",
                "ミャ", "ミュ", "ミョ",
                "", "", "",
                "リャ", "リュ", "リョ",
                "", "", "",
                "", "", "",
                "ギャ", "ギュ", "ギョ",
                "ジャ", "ジュ", "ジョ",
                "", "", "",
                "ビャ", "ビュ", "ビョ",
                "ピャ", "ピュ", "ピョ",

                "a", "i", "u", "e", "o",
                "ka", "ki", "ku", "ke", "ko",
                "sa", "shi", "su", "se", "so",
                "ta", "chi", "tsu", "te", "to",
                "na", "ni", "nu", "ne", "no",
                "ha", "hi", "fu", "he", "ho",
                "ma", "mi", "mu", "me", "mo",
                "ya", "", "yu", "", "yo",
                "ra", "ri", "ru", "re", "ro",
                "wa", "", "", "", "(w)o",
                "n", "", "", "", "",
                "ga", "gi", "gu", "ge", "go",
                "za", "ji", "zu", "ze", "zo",
                "da", "ji", "zu", "de", "do",
                "ba", "bi", "bu", "be", "bo",
                "pa", "pi", "pu", "pe", "po",
                "", "", "",
                "kya", "kyu", "kyo",
                "sha", "shu", "sho",
                "cha", "chu", "cho",
                "nya", "nyu", "nyo",
                "hya", "hyo", "hyo",
                "mya", "myu", "myo",
                "", "", "",
                "rya", "ryu", "ryo",
                "", "", "",
                "", "", "",
                "gya", "gyu", "gyo",
                "ja", "ju", "jo",
                "", "", "",
                "bya", "byu", "byo",
                "pya", "pyu", "pyo"
                });

        }

        public List<string> GetAlphabet()
        {
            return listAlphabet;
        }

        public string GetRandomHiragana()
        {
            List<string> listHiragana = new List<string>(new string[] {
                "あ", "い", "う", "え", "お",
                "か", "き", "く", "け", "こ",
                "さ", "し", "す", "せ", "そ",
                "た", "ち", "つ", "て", "と",
                "な", "に", "ぬ", "ね", "の",
                "は", "ひ", "ふ", "へ", "ほ",
                "ま", "み", "む", "め", "も",
                "や", "ゆ", "よ",
                "ら", "り", "る", "れ", "ろ",
                "わ", "を",
                "ん",
                "が", "ぎ", "ぐ", "げ", "ご",
                "ざ", "じ", "ず", "ぜ", "ぞ",
                "だ", "ぢ", "づ", "で", "ど",
                "ば", "び", "ぶ", "べ", "ぼ",
                "ぱ", "ぴ", "ぷ", "ぺ", "ぽ",
                "きゃ", "きゅ", "きょ",
                "しゃ", "しゅ", "しょ",
                "ちゃ", "ちゅ", "ちょ",
                "にゃ", "にゅ", "にょ",
                "ひゃ", "ひゅ", "ひょ",
                "みゃ", "みゅ", "みょ",
                "りゃ", "りゅ", "りょ",
                "ぎゃ", "ぎゅ", "ぎょ",
                "じゃ", "じゅ", "じょ",
                "びゃ", "びゅ", "びょ",
                "ぴゃ", "ぴゅ", "ぴょ"
                });
            Random rnd = new Random();
            int num = rnd.Next(listHiragana.Count);
            return listHiragana[num];
        }

        public string GetRandomKatakana()
        {
            List<string> listKatakana = new List<string>(new string[] {
                "ア", "イ", "ウ", "エ", "オ",
                "カ", "キ", "ク", "ケ", "コ",
                "サ", "シ", "ス", "セ", "ソ",
                "タ", "チ", "ツ", "テ", "ト",
                "ナ", "ニ", "ヌ", "ネ", "ノ",
                "ハ", "ヒ", "フ", "ヘ", "ホ",
                "マ", "ミ", "ム", "メ", "モ",
                "ヤ", "ユ", "ヨ",
                "ラ", "リ", "ル", "レ", "ロ",
                "ワ", "ヲ",
                "ン",
                "ガ", "ギ", "グ", "ゲ", "ゴ",
                "ザ", "ジ", "ズ", "ゼ", "ゾ",
                "ダ", "ヂ", "ヅ", "デ", "ド",
                "バ", "ビ", "ブ", "ベ", "ボ",
                "パ", "ピ", "プ", "ペ", "ポ",
                "キャ", "キュ", "キョ",
                "シャ", "シュ", "ショ",
                "チャ", "チュ", "チョ",
                "ニャ", "ニュ", "ニョ",
                "ヒャ", "ヒュ", "ヒョ",
                "ミャ", "ミュ", "ミョ",
                "リャ", "リュ", "リョ",
                "ギャ", "ギュ", "ギョ",
                "ジャ", "ジュ", "ジョ",
                "ビャ", "ビュ", "ビョ",
                "ピャ", "ピュ", "ピョ"
                });
            Random rnd = new Random();
            int num = rnd.Next(listKatakana.Count);
            return listKatakana[num];
        }

        public bool CompareAnswer(string kana, string roma)
        {
            roma = roma.Trim();
            roma = roma.ToLower();
            string ans1 = "";
            string ans2 = "";
            string ans3 = "";
            switch (kana)
            {
                case "あ": ans1 = "a"; break;
                case "い": ans1 = "i"; break;
                case "う": ans1 = "u"; break;
                case "え": ans1 = "e"; break;
                case "お": ans1 = "o"; break;
                case "か": ans1 = "ka"; break;
                case "き": ans1 = "ki"; break;
                case "く": ans1 = "ku"; break;
                case "け": ans1 = "ke"; break;
                case "こ": ans1 = "ko"; break;
                case "きゃ": ans1 = "kya"; break;
                case "きゅ": ans1 = "kyu"; break;
                case "きょ": ans1 = "kyo"; break;
                case "さ": ans1 = "sa"; break;
                case "し": ans1 = "shi"; ans2 = "si"; break;
                case "す": ans1 = "su"; break;
                case "せ": ans1 = "se"; break;
                case "そ": ans1 = "so"; break;
                case "しゃ": ans1 = "sha"; ans2 = "sya"; break;
                case "しゅ": ans1 = "shu"; ans2 = "syu"; break;
                case "しょ": ans1 = "sho"; ans2 = "syo"; break;
                case "た": ans1 = "ta"; break;
                case "ち": ans1 = "chi"; ans2 = "ti"; break;
                case "つ": ans1 = "tsu"; ans2 = "tu"; break;
                case "て": ans1 = "te"; break;
                case "と": ans1 = "to"; break;
                case "ちゃ": ans1 = "cha"; ans2 = "cya"; ans3 = "tya"; break;
                case "ちゅ": ans1 = "chu"; ans2 = "cyu"; ans3 = "tyu"; break;
                case "ちょ": ans1 = "cho"; ans2 = "cyo"; ans3 = "tyo"; break;
                case "な": ans1 = "na"; break;
                case "に": ans1 = "ni"; break;
                case "ぬ": ans1 = "nu"; break;
                case "ね": ans1 = "ne"; break;
                case "の": ans1 = "no"; break;
                case "にゃ": ans1 = "nya"; break;
                case "にゅ": ans1 = "nyu"; break;
                case "にょ": ans1 = "nyo"; break;
                case "は": ans1 = "ha"; break;
                case "ひ": ans1 = "hi"; break;
                case "ふ": ans1 = "fu"; ans2 = "hu"; break;
                case "へ": ans1 = "he"; break;
                case "ほ": ans1 = "ho"; break;
                case "ひゃ": ans1 = "hya"; break;
                case "ひゅ": ans1 = "hyu"; break;
                case "ひょ": ans1 = "hyo"; break;
                case "ま": ans1 = "ma"; break;
                case "み": ans1 = "mi"; break;
                case "む": ans1 = "mu"; break;
                case "め": ans1 = "me"; break;
                case "も": ans1 = "mo"; break;
                case "みゃ": ans1 = "mya"; break;
                case "みゅ": ans1 = "myu"; break;
                case "みょ": ans1 = "myo"; break;
                case "や": ans1 = "ya"; break;
                case "ゆ": ans1 = "yu"; break;
                case "よ": ans1 = "yo"; break;
                case "ら": ans1 = "ra"; break;
                case "り": ans1 = "ri"; break;
                case "る": ans1 = "ru"; break;
                case "れ": ans1 = "re"; break;
                case "ろ": ans1 = "ro"; break;
                case "りゃ": ans1 = "rya"; break;
                case "りゅ": ans1 = "ryu"; break;
                case "りょ": ans1 = "ryo"; break;
                case "わ": ans1 = "wa"; break;
                case "を": ans1 = "(w)o"; ans2 = "o"; ans3 = "wo"; break;
                case "ん": ans1 = "n"; ans2 = "nn"; ans3 = "m"; break;
                case "が": ans1 = "ga"; break;
                case "ぎ": ans1 = "gi"; break;
                case "ぐ": ans1 = "gu"; break;
                case "げ": ans1 = "ge"; break;
                case "ご": ans1 = "go"; break;
                case "ぎゃ": ans1 = "gya"; break;
                case "ぎゅ": ans1 = "gyu"; break;
                case "ぎょ": ans1 = "gyo"; break;
                case "ざ": ans1 = "za"; break;
                case "じ": ans1 = "ji"; ans2 = "zi"; break;
                case "ず": ans1 = "zu"; break;
                case "ぜ": ans1 = "ze"; break;
                case "ぞ": ans1 = "zo"; break;
                case "じゃ": ans1 = "ja"; ans2 = "jya"; ans3 = "zya"; break;
                case "じゅ": ans1 = "ju"; ans2 = "jyu"; ans3 = "zyu"; break;
                case "じょ": ans1 = "jo"; ans2 = "jyo"; ans3 = "zyo"; break;
                case "だ": ans1 = "da"; break;
                case "ぢ": ans1 = "ji"; ans2 = "di"; ans3 = "dzi"; break;
                case "づ": ans1 = "zu"; ans2 = "du"; ans3 = "dzu"; break;
                case "で": ans1 = "de"; break;
                case "ど": ans1 = "do"; break;
                case "ぢゃ": ans1 = "ja"; ans2 = "dya"; ans3 = "zya"; break;
                case "ぢゅ": ans1 = "ju"; ans2 = "dyu"; ans3 = "zyu"; break;
                case "ぢょ": ans1 = "jo"; ans2 = "dyo"; ans3 = "zyo"; break;
                case "ば": ans1 = "ba"; break;
                case "び": ans1 = "bi"; break;
                case "ぶ": ans1 = "bu"; break;
                case "べ": ans1 = "be"; break;
                case "ぼ": ans1 = "bo"; break;
                case "びゃ": ans1 = "bya"; break;
                case "びゅ": ans1 = "byu"; break;
                case "びょ": ans1 = "byo"; break;
                case "ぱ": ans1 = "pa"; break;
                case "ぴ": ans1 = "pi"; break;
                case "ぷ": ans1 = "pu"; break;
                case "ぺ": ans1 = "pe"; break;
                case "ぽ": ans1 = "po"; break;
                case "ぴゃ": ans1 = "pya"; break;
                case "ぴゅ": ans1 = "pyu"; break;
                case "ぴょ": ans1 = "pyo"; break;
                case "ア": ans1 = "a"; break;
                case "イ": ans1 = "i"; break;
                case "ウ": ans1 = "u"; break;
                case "エ": ans1 = "e"; break;
                case "オ": ans1 = "o"; break;
                case "カ": ans1 = "ka"; break;
                case "キ": ans1 = "ki"; break;
                case "ク": ans1 = "ku"; break;
                case "ケ": ans1 = "ke"; break;
                case "コ": ans1 = "ko"; break;
                case "キャ": ans1 = "kya"; break;
                case "キュ": ans1 = "kyu"; break;
                case "キョ": ans1 = "kyo"; break;
                case "サ": ans1 = "sa"; break;
                case "シ": ans1 = "shi"; ans2 = "si"; break;
                case "ス": ans1 = "su"; break;
                case "セ": ans1 = "se"; break;
                case "ソ": ans1 = "so"; break;
                case "シャ": ans1 = "sha"; ans2 = "sya"; break;
                case "シュ": ans1 = "shu"; ans2 = "syu"; break;
                case "ショ": ans1 = "sho"; ans2 = "syo"; break;
                case "タ": ans1 = "ta"; break;
                case "チ": ans1 = "chi"; ans2 = "ti"; break;
                case "ツ": ans1 = "tsu"; ans2 = "tu"; break;
                case "テ": ans1 = "te"; break;
                case "ト": ans1 = "to"; break;
                case "チャ": ans1 = "cha"; ans2 = "cya"; ans3 = "tya"; break;
                case "チュ": ans1 = "chu"; ans2 = "cyu"; ans3 = "tyu"; break;
                case "チョ": ans1 = "cho"; ans2 = "cyo"; ans3 = "tyo"; break;
                case "ナ": ans1 = "na"; break;
                case "ニ": ans1 = "ni"; break;
                case "ヌ": ans1 = "nu"; break;
                case "ネ": ans1 = "ne"; break;
                case "ノ": ans1 = "no"; break;
                case "ニャ": ans1 = "nya"; break;
                case "ニュ": ans1 = "nyu"; break;
                case "ニョ": ans1 = "nyo"; break;
                case "ハ": ans1 = "ha"; break;
                case "ヒ": ans1 = "hi"; break;
                case "フ": ans1 = "fu"; ans2 = "hu"; break;
                case "ヘ": ans1 = "he"; break;
                case "ホ": ans1 = "ho"; break;
                case "ヒャ": ans1 = "hya"; break;
                case "ヒュ": ans1 = "hyu"; break;
                case "ヒョ": ans1 = "hyo"; break;
                case "マ": ans1 = "ma"; break;
                case "ミ": ans1 = "mi"; break;
                case "ム": ans1 = "mu"; break;
                case "メ": ans1 = "me"; break;
                case "モ": ans1 = "mo"; break;
                case "ミャ": ans1 = "mya"; break;
                case "ミュ": ans1 = "myu"; break;
                case "ミョ": ans1 = "myo"; break;
                case "ヤ": ans1 = "ya"; break;
                case "ユ": ans1 = "yu"; break;
                case "ヨ": ans1 = "yo"; break;
                case "ラ": ans1 = "ra"; break;
                case "リ": ans1 = "ri"; break;
                case "ル": ans1 = "ru"; break;
                case "レ": ans1 = "re"; break;
                case "ロ": ans1 = "ro"; break;
                case "リャ": ans1 = "rya"; break;
                case "リュ": ans1 = "ryu"; break;
                case "リョ": ans1 = "ryo"; break;
                case "ワ": ans1 = "wa"; break;
                case "ヲ": ans1 = "(w)o"; ans2 = "o"; ans3 = "wo"; break;
                case "ン": ans1 = "n"; ans2 = "nn"; ans3 = "m"; break;
                case "ガ": ans1 = "ga"; break;
                case "ギ": ans1 = "gi"; break;
                case "グ": ans1 = "gu"; break;
                case "ゲ": ans1 = "ge"; break;
                case "ゴ": ans1 = "go"; break;
                case "ギャ": ans1 = "gya"; break;
                case "ギュ": ans1 = "gyu"; break;
                case "ギョ": ans1 = "gyo"; break;
                case "ザ": ans1 = "za"; break;
                case "ジ": ans1 = "ji"; ans2 = "zi"; break;
                case "ズ": ans1 = "zu"; break;
                case "ゼ": ans1 = "ze"; break;
                case "ゾ": ans1 = "zo"; break;
                case "ジャ": ans1 = "ja"; ans2 = "jya"; ans3 = "zya"; break;
                case "ジュ": ans1 = "ju"; ans2 = "jyu"; ans3 = "zyu"; break;
                case "ジョ": ans1 = "jo"; ans2 = "jyo"; ans3 = "zyo"; break;
                case "ダ": ans1 = "da"; break;
                case "ヂ": ans1 = "ji"; ans2 = "di"; ans3 = "dzi"; break;
                case "ヅ": ans1 = "zu"; ans2 = "du"; ans3 = "dzu"; break;
                case "デ": ans1 = "de"; break;
                case "ド": ans1 = "do"; break;
                case "ヂャ": ans1 = "ja"; ans2 = "dya"; ans3 = "zya"; break;
                case "ヂュ": ans1 = "ju"; ans2 = "dyu"; ans3 = "zyu"; break;
                case "ヂョ": ans1 = "jo"; ans2 = "dyo"; ans3 = "zyo"; break;
                case "バ": ans1 = "ba"; break;
                case "ビ": ans1 = "bi"; break;
                case "ブ": ans1 = "bu"; break;
                case "ベ": ans1 = "be"; break;
                case "ボ": ans1 = "bo"; break;
                case "ビャ": ans1 = "bya"; break;
                case "ビュ": ans1 = "byu"; break;
                case "ビョ": ans1 = "byo"; break;
                case "パ": ans1 = "pa"; break;
                case "ピ": ans1 = "pi"; break;
                case "プ": ans1 = "pu"; break;
                case "ペ": ans1 = "pe"; break;
                case "ポ": ans1 = "po"; break;
                case "ピャ": ans1 = "pya"; break;
                case "ピュ": ans1 = "pyu"; break;
                case "ピョ": ans1 = "pyo"; break;

            }
            return (roma.Equals(ans1) || roma.Equals(ans2) || roma.Equals(ans3));
        }

        public string GetAnswer(string kana)
        {
            string ans1 = "";
            string ans2 = "";
            string ans3 = "";
            switch (kana)
            {
                case "あ": ans1 = "a"; break;
                case "い": ans1 = "i"; break;
                case "う": ans1 = "u"; break;
                case "え": ans1 = "e"; break;
                case "お": ans1 = "o"; break;
                case "か": ans1 = "ka"; break;
                case "き": ans1 = "ki"; break;
                case "く": ans1 = "ku"; break;
                case "け": ans1 = "ke"; break;
                case "こ": ans1 = "ko"; break;
                case "きゃ": ans1 = "kya"; break;
                case "きゅ": ans1 = "kyu"; break;
                case "きょ": ans1 = "kyo"; break;
                case "さ": ans1 = "sa"; break;
                case "し": ans1 = "shi"; ans2 = "si"; break;
                case "す": ans1 = "su"; break;
                case "せ": ans1 = "se"; break;
                case "そ": ans1 = "so"; break;
                case "しゃ": ans1 = "sha"; ans2 = "sya"; break;
                case "しゅ": ans1 = "shu"; ans2 = "syu"; break;
                case "しょ": ans1 = "sho"; ans2 = "syo"; break;
                case "た": ans1 = "ta"; break;
                case "ち": ans1 = "chi"; ans2 = "ti"; break;
                case "つ": ans1 = "tsu"; ans2 = "tu"; break;
                case "て": ans1 = "te"; break;
                case "と": ans1 = "to"; break;
                case "ちゃ": ans1 = "cha"; ans2 = "cya"; ans3 = "tya"; break;
                case "ちゅ": ans1 = "chu"; ans2 = "cyu"; ans3 = "tyu"; break;
                case "ちょ": ans1 = "cho"; ans2 = "cyo"; ans3 = "tyo"; break;
                case "な": ans1 = "na"; break;
                case "に": ans1 = "ni"; break;
                case "ぬ": ans1 = "nu"; break;
                case "ね": ans1 = "ne"; break;
                case "の": ans1 = "no"; break;
                case "にゃ": ans1 = "nya"; break;
                case "にゅ": ans1 = "nyu"; break;
                case "にょ": ans1 = "nyo"; break;
                case "は": ans1 = "ha"; break;
                case "ひ": ans1 = "hi"; break;
                case "ふ": ans1 = "fu"; ans2 = "hu"; break;
                case "へ": ans1 = "he"; break;
                case "ほ": ans1 = "ho"; break;
                case "ひゃ": ans1 = "hya"; break;
                case "ひゅ": ans1 = "hyu"; break;
                case "ひょ": ans1 = "hyo"; break;
                case "ま": ans1 = "ma"; break;
                case "み": ans1 = "mi"; break;
                case "む": ans1 = "mu"; break;
                case "め": ans1 = "me"; break;
                case "も": ans1 = "mo"; break;
                case "みゃ": ans1 = "mya"; break;
                case "みゅ": ans1 = "myu"; break;
                case "みょ": ans1 = "myo"; break;
                case "や": ans1 = "ya"; break;
                case "ゆ": ans1 = "yu"; break;
                case "よ": ans1 = "yo"; break;
                case "ら": ans1 = "ra"; break;
                case "り": ans1 = "ri"; break;
                case "る": ans1 = "ru"; break;
                case "れ": ans1 = "re"; break;
                case "ろ": ans1 = "ro"; break;
                case "りゃ": ans1 = "rya"; break;
                case "りゅ": ans1 = "ryu"; break;
                case "りょ": ans1 = "ryo"; break;
                case "わ": ans1 = "wa"; break;
                case "を": ans1 = "(w)o"; ans2 = "o"; ans3 = "wo"; break;
                case "ん": ans1 = "n"; ans2 = "nn"; ans3 = "m"; break;
                case "が": ans1 = "ga"; break;
                case "ぎ": ans1 = "gi"; break;
                case "ぐ": ans1 = "gu"; break;
                case "げ": ans1 = "ge"; break;
                case "ご": ans1 = "go"; break;
                case "ぎゃ": ans1 = "gya"; break;
                case "ぎゅ": ans1 = "gyu"; break;
                case "ぎょ": ans1 = "gyo"; break;
                case "ざ": ans1 = "za"; break;
                case "じ": ans1 = "ji"; ans2 = "zi"; break;
                case "ず": ans1 = "zu"; break;
                case "ぜ": ans1 = "ze"; break;
                case "ぞ": ans1 = "zo"; break;
                case "じゃ": ans1 = "ja"; ans2 = "jya"; ans3 = "zya"; break;
                case "じゅ": ans1 = "ju"; ans2 = "jyu"; ans3 = "zyu"; break;
                case "じょ": ans1 = "jo"; ans2 = "jyo"; ans3 = "zyo"; break;
                case "だ": ans1 = "da"; break;
                case "ぢ": ans1 = "ji"; ans2 = "di"; ans3 = "dzi"; break;
                case "づ": ans1 = "zu"; ans2 = "du"; ans3 = "dzu"; break;
                case "で": ans1 = "de"; break;
                case "ど": ans1 = "do"; break;
                case "ぢゃ": ans1 = "ja"; ans2 = "dya"; ans3 = "zya"; break;
                case "ぢゅ": ans1 = "ju"; ans2 = "dyu"; ans3 = "zyu"; break;
                case "ぢょ": ans1 = "jo"; ans2 = "dyo"; ans3 = "zyo"; break;
                case "ば": ans1 = "ba"; break;
                case "び": ans1 = "bi"; break;
                case "ぶ": ans1 = "bu"; break;
                case "べ": ans1 = "be"; break;
                case "ぼ": ans1 = "bo"; break;
                case "びゃ": ans1 = "bya"; break;
                case "びゅ": ans1 = "byu"; break;
                case "びょ": ans1 = "byo"; break;
                case "ぱ": ans1 = "pa"; break;
                case "ぴ": ans1 = "pi"; break;
                case "ぷ": ans1 = "pu"; break;
                case "ぺ": ans1 = "pe"; break;
                case "ぽ": ans1 = "po"; break;
                case "ぴゃ": ans1 = "pya"; break;
                case "ぴゅ": ans1 = "pyu"; break;
                case "ぴょ": ans1 = "pyo"; break;
                case "ア": ans1 = "a"; break;
                case "イ": ans1 = "i"; break;
                case "ウ": ans1 = "u"; break;
                case "エ": ans1 = "e"; break;
                case "オ": ans1 = "o"; break;
                case "カ": ans1 = "ka"; break;
                case "キ": ans1 = "ki"; break;
                case "ク": ans1 = "ku"; break;
                case "ケ": ans1 = "ke"; break;
                case "コ": ans1 = "ko"; break;
                case "キャ": ans1 = "kya"; break;
                case "キュ": ans1 = "kyu"; break;
                case "キョ": ans1 = "kyo"; break;
                case "サ": ans1 = "sa"; break;
                case "シ": ans1 = "shi"; ans2 = "si"; break;
                case "ス": ans1 = "su"; break;
                case "セ": ans1 = "se"; break;
                case "ソ": ans1 = "so"; break;
                case "シャ": ans1 = "sha"; ans2 = "sya"; break;
                case "シュ": ans1 = "shu"; ans2 = "syu"; break;
                case "ショ": ans1 = "sho"; ans2 = "syo"; break;
                case "タ": ans1 = "ta"; break;
                case "チ": ans1 = "chi"; ans2 = "ti"; break;
                case "ツ": ans1 = "tsu"; ans2 = "tu"; break;
                case "テ": ans1 = "te"; break;
                case "ト": ans1 = "to"; break;
                case "チャ": ans1 = "cha"; ans2 = "cya"; ans3 = "tya"; break;
                case "チュ": ans1 = "chu"; ans2 = "cyu"; ans3 = "tyu"; break;
                case "チョ": ans1 = "cho"; ans2 = "cyo"; ans3 = "tyo"; break;
                case "ナ": ans1 = "na"; break;
                case "ニ": ans1 = "ni"; break;
                case "ヌ": ans1 = "nu"; break;
                case "ネ": ans1 = "ne"; break;
                case "ノ": ans1 = "no"; break;
                case "ニャ": ans1 = "nya"; break;
                case "ニュ": ans1 = "nyu"; break;
                case "ニョ": ans1 = "nyo"; break;
                case "ハ": ans1 = "ha"; break;
                case "ヒ": ans1 = "hi"; break;
                case "フ": ans1 = "fu"; ans2 = "hu"; break;
                case "ヘ": ans1 = "he"; break;
                case "ホ": ans1 = "ho"; break;
                case "ヒャ": ans1 = "hya"; break;
                case "ヒュ": ans1 = "hyu"; break;
                case "ヒョ": ans1 = "hyo"; break;
                case "マ": ans1 = "ma"; break;
                case "ミ": ans1 = "mi"; break;
                case "ム": ans1 = "mu"; break;
                case "メ": ans1 = "me"; break;
                case "モ": ans1 = "mo"; break;
                case "ミャ": ans1 = "mya"; break;
                case "ミュ": ans1 = "myu"; break;
                case "ミョ": ans1 = "myo"; break;
                case "ヤ": ans1 = "ya"; break;
                case "ユ": ans1 = "yu"; break;
                case "ヨ": ans1 = "yo"; break;
                case "ラ": ans1 = "ra"; break;
                case "リ": ans1 = "ri"; break;
                case "ル": ans1 = "ru"; break;
                case "レ": ans1 = "re"; break;
                case "ロ": ans1 = "ro"; break;
                case "リャ": ans1 = "rya"; break;
                case "リュ": ans1 = "ryu"; break;
                case "リョ": ans1 = "ryo"; break;
                case "ワ": ans1 = "wa"; break;
                case "ヲ": ans1 = "(w)o"; ans2 = "o"; ans3 = "wo"; break;
                case "ン": ans1 = "n"; ans2 = "nn"; ans3 = "m"; break;
                case "ガ": ans1 = "ga"; break;
                case "ギ": ans1 = "gi"; break;
                case "グ": ans1 = "gu"; break;
                case "ゲ": ans1 = "ge"; break;
                case "ゴ": ans1 = "go"; break;
                case "ギャ": ans1 = "gya"; break;
                case "ギュ": ans1 = "gyu"; break;
                case "ギョ": ans1 = "gyo"; break;
                case "ザ": ans1 = "za"; break;
                case "ジ": ans1 = "ji"; ans2 = "zi"; break;
                case "ズ": ans1 = "zu"; break;
                case "ゼ": ans1 = "ze"; break;
                case "ゾ": ans1 = "zo"; break;
                case "ジャ": ans1 = "ja"; ans2 = "jya"; ans3 = "zya"; break;
                case "ジュ": ans1 = "ju"; ans2 = "jyu"; ans3 = "zyu"; break;
                case "ジョ": ans1 = "jo"; ans2 = "jyo"; ans3 = "zyo"; break;
                case "ダ": ans1 = "da"; break;
                case "ヂ": ans1 = "ji"; ans2 = "di"; ans3 = "dzi"; break;
                case "ヅ": ans1 = "zu"; ans2 = "du"; ans3 = "dzu"; break;
                case "デ": ans1 = "de"; break;
                case "ド": ans1 = "do"; break;
                case "ヂャ": ans1 = "ja"; ans2 = "dya"; ans3 = "zya"; break;
                case "ヂュ": ans1 = "ju"; ans2 = "dyu"; ans3 = "zyu"; break;
                case "ヂョ": ans1 = "jo"; ans2 = "dyo"; ans3 = "zyo"; break;
                case "バ": ans1 = "ba"; break;
                case "ビ": ans1 = "bi"; break;
                case "ブ": ans1 = "bu"; break;
                case "ベ": ans1 = "be"; break;
                case "ボ": ans1 = "bo"; break;
                case "ビャ": ans1 = "bya"; break;
                case "ビュ": ans1 = "byu"; break;
                case "ビョ": ans1 = "byo"; break;
                case "パ": ans1 = "pa"; break;
                case "ピ": ans1 = "pi"; break;
                case "プ": ans1 = "pu"; break;
                case "ペ": ans1 = "pe"; break;
                case "ポ": ans1 = "po"; break;
                case "ピャ": ans1 = "pya"; break;
                case "ピュ": ans1 = "pyu"; break;
                case "ピョ": ans1 = "pyo"; break;

            }
            return ans1;
        }

        public Alphabet AnswerPractice(string kana, string roma, int type)
        {
            Alphabet alpha = new Alphabet();
            alpha.Answer = GetAnswer(kana);
            if (CompareAnswer(kana, roma))
            {
                alpha.Result = "OK";
            }
            else
            {
                alpha.Result = "<div class=\"jp-prac-result\"><p class=\"jp-prac-text\">" + kana + "</p><p class=\"jp-prac-wrong\">" + roma + "</p><p class=\"jp-prac-correct\">" + alpha.Answer + "</p></div>";
            }

            if(type == 1)
            {
                alpha.New = GetRandomHiragana();
            }else
            {
                alpha.New = GetRandomKatakana();
            }

            return alpha;
        }

        // ==============================================
        // CONVERT VIETNAMESE WORD TO KATAKANA
        // Description: This function converts a Vietnamese word (e.g. a person's first name) into katakana (for example: Minh -> ミン)
        // 
        // ベトナム語の１語をカタカナに変換する
        // 説明：このメソッドは、ベトナム語の１語をカタカナに変換する。（例：Minh　→　ミン)
        // ==============================================
        private string ConvertName(string str)
        {
            str.Trim();

            // Return empty string if input is empty
            // strパラメーターが""なら、戻り値も""
            if (str.Equals(""))
            {
                return "";
            }

            // Convert string to lowercase
            // strを小文字にする
            str = str.ToLower();

            // Fix for Unicode tổ hợp
            // Unicode tổ hợp入力を修正
            str = str.Replace("\u0300", "");
            str = str.Replace("\u0301", "");
            str = str.Replace("\u0303", "");
            str = str.Replace("\u0309", "");
            str = str.Replace("\u0323", "");

            // Remove diacritics
            // ダイアクリティックを削除する
            str = str.Replace("á", "a").Replace("à", "a").Replace("ả", "a").Replace("ã", "a").Replace("ạ", "a");
            str = str.Replace("ắ", "ă").Replace("ằ", "ă").Replace("ẳ", "ă").Replace("ẵ", "ă").Replace("ặ", "ă");
            str = str.Replace("ấ", "â").Replace("ầ", "â").Replace("ẩ", "â").Replace("ẫ", "â").Replace("ậ", "â");
            str = str.Replace("é", "e").Replace("è", "e").Replace("ẻ", "e").Replace("ẽ", "e").Replace("ẹ", "e");
            str = str.Replace("ế", "ê").Replace("ề", "ê").Replace("ể", "ê").Replace("ễ", "ê").Replace("ệ", "e");
            str = str.Replace("í", "i").Replace("ì", "i").Replace("ỉ", "i").Replace("ĩ", "i").Replace("ị", "i");
            str = str.Replace("ó", "o").Replace("ò", "o").Replace("ỏ", "o").Replace("õ", "o").Replace("ọ", "o");
            str = str.Replace("ố", "ô").Replace("ồ", "ô").Replace("ổ", "ô").Replace("ỗ", "ô").Replace("ộ", "ô");
            str = str.Replace("ớ", "ơ").Replace("ờ", "ơ").Replace("ở", "ơ").Replace("ỡ", "ơ").Replace("ợ", "ơ");
            str = str.Replace("ú", "u").Replace("ù", "u").Replace("ủ", "u").Replace("ũ", "u").Replace("ụ", "u");
            str = str.Replace("ứ", "ư").Replace("ừ", "ư").Replace("ử", "ư").Replace("ữ", "ư").Replace("ự", "ư");
            str = str.Replace("ý", "y").Replace("ỳ", "y").Replace("ỷ", "y").Replace("ỹ", "y").Replace("ỵ", "y");

            // Special treatments
            // 特別な処理
            str = str.Replace("âu", "ou");
            str = str.Replace("ây", "ei");

            str = str.Replace("ă", "a");
            str = str.Replace("â", "a");
            str = str.Replace("ê", "e");
            str = str.Replace("ô", "o");
            str = str.Replace("ơ", "o");
            str = str.Replace("ư", "u");

            // Declare variables
            // 変数を宣言する
            string res = "";
            string firstPart = "";
            string secondPart = "";
            string thirdPart = "";
            int firstVowel = 0;
            int secondVowel = 0;
            char[] strc = str.ToCharArray();

            // Detect the location of first vowel in word
            // １つ目の母音を探す
            for (int i = 0; i < str.Length; i++)
            {
                if (strc[i].Equals('a') || strc[i].Equals('e') || strc[i].Equals('i') || strc[i].Equals('o') || strc[i].Equals('u') || strc[i].Equals('y'))
                {
                    firstVowel = i;
                    secondVowel = i;
                    i = str.Length;
                }
            }

            // Special treatment for "qu" and "gi"
            // 特別な処理 ("qu"と"gi"の場合）
            if (str.Length >= 3)
            {
                if (str.Substring(0, 2) == "qu")
                {
                    firstVowel += 1;
                    secondVowel += 1;
                }
                if (str.Substring(0, 3) == "gia" || str.Substring(0, 3) == "gie" || str.Substring(0, 3) == "gio" || str.Substring(0, 3) == "giu")
                {
                    firstVowel += 1;
                    secondVowel += 1;
                }
            }

            // Detect the location of second vowel in word (if exists)
            // ２つ目の母音を探す
            for (int i = firstVowel + 1; i < str.Length; i++)
            {
                if (strc[i].Equals('a') || strc[i].Equals('e') || strc[i].Equals('i') || strc[i].Equals('o') || strc[i].Equals('u') || strc[i].Equals('y'))
                {
                    secondVowel = i;
                    i = str.Length;
                }
            }




            // Depending on location of first (and second) vowels, split the word into three parts
            // 母音の位置によって、語を３パートに分割する
            firstPart = str.Substring(0, firstVowel + 1);
            secondPart = str.Substring(firstVowel + 1, secondVowel - firstVowel);
            thirdPart = str.Substring(secondVowel + 1);

            // Convert first part into katakana
            // １パート目をカタカナに変換する
            firstPart = firstPart.Replace("y", "i");
            switch (firstPart)
            {
                case "a": res += "ア"; break;
                case "ba": res += "バ"; break;
                case "be": res += "ベ"; break;
                case "bi": res += "ビ"; break;
                case "bo": res += "ボ"; break;
                case "bu": res += "ブ"; break;
                case "ca": res += "カ"; break;
                case "co": res += "コ"; break;
                case "cu": res += "ク"; break;
                case "cha": res += "チャ"; break;
                case "che": res += "チェ"; break;
                case "chi": res += "チ"; break;
                case "cho": res += "チョ"; break;
                case "chu": res += "チュ"; break;
                case "da": res += "ザ"; break;
                case "de": res += "ゼ"; break;
                case "di": res += "ジ"; break;
                case "do": res += "ゾ"; break;
                case "du": res += "ズ"; break;
                case "đa": res += "ダ"; break;
                case "đe": res += "デ"; break;
                case "đi": res += "ディ"; break;
                case "đo": res += "ド"; break;
                case "đu": res += "ドゥ"; break;
                case "e": res += "エ"; break;
                case "ga": res += "ガ"; break;
                case "ghe": res += "ゲ"; break;
                case "ghi": res += "ギ"; break;
                case "gi": res += "ジ"; break;
                case "gia": res += "ジャ"; break;
                case "gie": res += "ジェ"; break;
                case "gio": res += "ジョ"; break;
                case "giu": res += "ジュ"; break;
                case "go": res += "ゴ"; break;
                case "gu": res += "グ"; break;
                case "ha": res += "ハ"; break;
                case "he": res += "ヘ"; break;
                case "hi": res += "ヒ"; break;
                case "ho": res += "ホ"; break;
                case "hu": res += "フ"; break;
                case "i": res += "イ"; break;
                case "ke": res += "ケ"; break;
                case "kha": res += "カ"; break;
                case "khe": res += "ケ"; break;
                case "khi": res += "キ"; break;
                case "kho": res += "コ"; break;
                case "khu": res += "ク"; break;
                case "ki": res += "キ"; break;
                case "la": res += "ラ"; break;
                case "le": res += "レ"; break;
                case "li": res += "リ"; break;
                case "lo": res += "ロ"; break;
                case "lu": res += "ル"; break;
                case "ma": res += "マ"; break;
                case "me": res += "メ"; break;
                case "mi": res += "ミ"; break;
                case "mo": res += "モ"; break;
                case "mu": res += "ム"; break;
                case "na": res += "ナ"; break;
                case "ne": res += "ネ"; break;
                case "nga": res += "ガ"; break;
                case "nghe": res += "ゲ"; break;
                case "nghi": res += "ギ"; break;
                case "ngo": res += "ゴ"; break;
                case "ngu": res += "グ"; break;
                case "nha": res += "ニャ"; break;
                case "nhe": res += "ニェ"; break;
                case "nhi": res += "ニ"; break;
                case "nho": res += "ニョ"; break;
                case "nhu": res += "ヌ"; break;
                case "ni": res += "ニ"; break;
                case "no": res += "ノ"; break;
                case "nu": res += "ヌ"; break;
                case "o": res += "オ"; break;
                case "pa": res += "パ"; break;
                case "pe": res += "ペ"; break;
                case "pha": res += "ファ"; break;
                case "phe": res += "フェ"; break;
                case "phi": res += "フィ"; break;
                case "pho": res += "フォ"; break;
                case "phu": res += "フ"; break;
                case "pi": res += "ピ"; break;
                case "po": res += "ポ"; break;
                case "pu": res += "プ"; break;
                case "qua": res += "クア"; break;
                case "que": res += "クエ"; break;
                case "qui": res += "クイ"; break;
                case "quo": res += "クオ"; break;
                case "ra": res += "ラ"; break;
                case "re": res += "レ"; break;
                case "ri": res += "リ"; break;
                case "ro": res += "ロ"; break;
                case "ru": res += "ル"; break;
                case "sa": res += "サ"; break;
                case "se": res += "セ"; break;
                case "si": res += "シ"; break;
                case "so": res += "ソ"; break;
                case "su": res += "ス"; break;
                case "ta": res += "タ"; break;
                case "te": res += "テ"; break;
                case "tha": res += "タ"; break;
                case "the": res += "テ"; break;
                case "thi": res += "ティ"; break;
                case "tho": res += "ト"; break;
                case "thu": res += "トゥ"; break;
                case "ti": res += "ティ"; break;
                case "to": res += "ト"; break;
                case "tra": res += "チャ"; break;
                case "tre": res += "チェ"; break;
                case "tri": res += "チ"; break;
                case "tro": res += "チョ"; break;
                case "tru": res += "チュ"; break;
                case "tu": res += "トゥ"; break;
                case "u": res += "ウ"; break;
                case "va": res += "ヴァ"; break;
                case "ve": res += "ヴェ"; break;
                case "vi": res += "ヴィ"; break;
                case "vo": res += "ヴォ"; break;
                case "vu": res += "ヴ"; break;
                case "xa": res += "サ"; break;
                case "xe": res += "セ"; break;
                case "xi": res += "シ"; break;
                case "xo": res += "ソ"; break;
                case "xu": res += "ス"; break;
                default: res += firstPart; break;
            }

            // Convert second part into katakana
            // ２パート目をカタカナに変換する
            switch (secondPart)
            {
                case "a": res += "ア"; break;
                case "e": res += "エ"; break;
                case "i": res += "イ"; break;
                case "o": res += "オ"; break;
                case "u": if (res.Equals("ウ") || res.Equals("ク") || res.Equals("グ") || res.Equals("ス") || res.Equals("ズ") || res.Equals("ツ") || res.Equals("トゥ") || res.Equals("ドゥ") || res.Equals("ヌ") || res.Equals("フ") || res.Equals("ブ") || res.Equals("プ") || res.Equals("ム") || res.Equals("ユ") || res.Equals("ル") || res.Equals("チュ") || res.Equals("ジュ")) { res += "ー"; } else { res += "ウ"; } break;
                case "y": if (!thirdPart.Equals("e") && !thirdPart.Equals("en") && !thirdPart.Equals("et")) { res += "イ"; } break;
                default: res += secondPart; break;
            }

            // Convert third part into katakana
            // ３パート目をカタカナに変換する
            switch (thirdPart)
            {
                case "": if (res.Length == 1 || (res.Length == 2 && (res.IndexOf("ャ") != -1 || res.IndexOf("ュ") != -1 || res.IndexOf("ョ") != -1 || res.IndexOf("ァ") != -1 || res.IndexOf("ェ") != -1 || res.IndexOf("ォ") != -1))) { res += "ー"; } break;
                case "a": res += "ア"; break;
                case "c": res += "ック"; break;
                case "ch": res += "ック"; break;
                //case "e": res += "エ"; break;
                case "en": res += "エン"; break;
                case "et": res += "エット"; break;
                case "i": res += "イ"; break;
                case "m": res += "ム"; break;
                case "n": res += "ン"; break;
                case "ng": res += "ン"; break;
                case "nh": if (str.Length >= 3) { if (strc[str.Length - 3].Equals('a')) { res += "イ"; } } res += "ン"; break;
                case "o": res += "オ"; break;
                case "p": res += "ップ"; break;
                case "t": res += "ット"; break;
                case "u": res += "ウ"; break;
                case "y": res += "イ"; break;
                default: res += thirdPart; break;
            }

            return res;
        }

        // ==============================================
        // CONVERT VIETNAMESE SENTENCE TO KATAKANA
        // Description: This function converts a Vietnamese sentence into katakana, replacing spaces with ・
        // (for example: Nguyễn Nhật Minh -> グエン・ニャット・ミン)
        // 
        // ベトナム語の文をカタカナに変換する
        // 説明：このメソッドは、ベトナム語の文をカタカナに変換する。２語以上があったら、「 」が「・」になる。
        //　　　（例：Nguyễn Nhật Minh　→　グエン・ニャット・ミン)
        // ==============================================
        public string ConvertFullName(string str)
        {
            str.Trim();
            while (str.Contains("  "))
            {
                str = str.Replace("  ", " ");
            }
            // Remove punctuation marks
            // 約物を削除する
            str = str.Replace(",", "");
            str = str.Replace(".", "");
            str = str.Replace(";", "");
            str = str.Replace(":", "");
            str = str.Replace("(", "");
            str = str.Replace(")", "");
            str = str.Replace("[", "");
            str = str.Replace("]", "");
            str = str.Replace("{", "");
            str = str.Replace("}", "");
            str = str.Replace("<", "");
            str = str.Replace(">", "");
            str = str.Replace("'", "");
            str = str.Replace("\"", "");
            str = str.Replace("?", "");
            str = str.Replace("!", "");
            str = str.Replace("-", "");

            // Split the input into a string array, each element contains a word
            // strを分割して、string配列を作る
            string[] stra = str.Split(' ');

            // Declare variable
            // 変数を宣言する
            string res = "";

            // Convert each word into katakana, separating them with ・
            // １語ずつをカタカナに変換する
            for (int i = 0; i < stra.Length; i++)
            {
                if (i != 0)
                {
                    res += "・";
                }
                res += ConvertName(stra[i]);
            }

            return res;
        }

        // ==============================================
        // CONVERT VIETNAMESE WORD TO KATAKANA (HTML)
        // Description: This function converts a Vietnamese word (e.g. a person's first name) into katakana (for example: Minh -> ミン)
        // 
        // ベトナム語の１語をカタカナに変換する（HTML）
        // 説明：このメソッドは、ベトナム語の１語をカタカナに変換する。（例：Minh　→　ミン)
        // ==============================================
        private string ConvertNameAdv(string str)
        {
            str.Trim();

            // Return empty string if input is empty
            // strパラメーターが""なら、戻り値も""
            string origStr = str;

            if (str.Equals(""))
            {
                return "";
            }

            // Convert string to lowercase
            // strを小文字にする
            str = str.ToLower();

            // Fix for Unicode tổ hợp
            // Unicode tổ hợp入力を修正
            str = str.Replace("\u0300", "");
            str = str.Replace("\u0301", "");
            str = str.Replace("\u0303", "");
            str = str.Replace("\u0309", "");
            str = str.Replace("\u0323", "");

            // Remove diacritics
            // ダイアクリティックを削除する
            str = str.Replace("á", "a").Replace("à", "a").Replace("ả", "a").Replace("ã", "a").Replace("ạ", "a");
            str = str.Replace("ắ", "ă").Replace("ằ", "ă").Replace("ẳ", "ă").Replace("ẵ", "ă").Replace("ặ", "ă");
            str = str.Replace("ấ", "â").Replace("ầ", "â").Replace("ẩ", "â").Replace("ẫ", "â").Replace("ậ", "â");
            str = str.Replace("é", "e").Replace("è", "e").Replace("ẻ", "e").Replace("ẽ", "e").Replace("ẹ", "e");
            str = str.Replace("ế", "ê").Replace("ề", "ê").Replace("ể", "ê").Replace("ễ", "ê").Replace("ệ", "e");
            str = str.Replace("í", "i").Replace("ì", "i").Replace("ỉ", "i").Replace("ĩ", "i").Replace("ị", "i");
            str = str.Replace("ó", "o").Replace("ò", "o").Replace("ỏ", "o").Replace("õ", "o").Replace("ọ", "o");
            str = str.Replace("ố", "ô").Replace("ồ", "ô").Replace("ổ", "ô").Replace("ỗ", "ô").Replace("ộ", "ô");
            str = str.Replace("ớ", "ơ").Replace("ờ", "ơ").Replace("ở", "ơ").Replace("ỡ", "ơ").Replace("ợ", "ơ");
            str = str.Replace("ú", "u").Replace("ù", "u").Replace("ủ", "u").Replace("ũ", "u").Replace("ụ", "u");
            str = str.Replace("ứ", "ư").Replace("ừ", "ư").Replace("ử", "ư").Replace("ữ", "ư").Replace("ự", "ư");
            str = str.Replace("ý", "y").Replace("ỳ", "y").Replace("ỷ", "y").Replace("ỹ", "y").Replace("ỵ", "y");

            // Special treatments
            // 特別な処理
            str = str.Replace("âu", "ou");
            str = str.Replace("ây", "ei");

            str = str.Replace("ă", "a");
            str = str.Replace("â", "a");
            str = str.Replace("ê", "e");
            str = str.Replace("ô", "o");
            str = str.Replace("ơ", "o");
            str = str.Replace("ư", "u");

            // Declare variables
            // 変数を宣言する
            string viet = "";
            string kata = "";
            string roma = "";
            string viet2 = "";
            string kata2 = "";
            string roma2 = "";
            string res = "";
            string tmp = "";
            string firstPart = "";
            string secondPart = "";
            string thirdPart = "";
            int firstVowel = 0;
            int secondVowel = 0;
            char[] strc = str.ToCharArray();

            // Detect the location of first vowel in word
            // １つ目の母音を探す
            for (int i = 0; i < str.Length; i++)
            {
                if (strc[i].Equals('a') || strc[i].Equals('e') || strc[i].Equals('i') || strc[i].Equals('o') || strc[i].Equals('u') || strc[i].Equals('y'))
                {
                    firstVowel = i;
                    secondVowel = i;
                    i = str.Length;
                }
            }

            // Special treatment for "qu" and "gi"
            // 特別な処理 ("qu"と"gi"の場合）
            if (str.Length >= 3)
            {
                if (str.Substring(0, 2) == "qu")
                {
                    firstVowel += 1;
                    secondVowel += 1;
                }
                if (str.Substring(0, 3) == "gia" || str.Substring(0, 3) == "gie" || str.Substring(0, 3) == "gio" || str.Substring(0, 3) == "giu")
                {
                    firstVowel += 1;
                    secondVowel += 1;
                }
            }

            // Detect the location of second vowel in word (if exists)
            // ２つ目の母音を探す
            for (int i = firstVowel + 1; i < str.Length; i++)
            {
                if (strc[i].Equals('a') || strc[i].Equals('e') || strc[i].Equals('i') || strc[i].Equals('o') || strc[i].Equals('u') || strc[i].Equals('y'))
                {
                    secondVowel = i;
                    i = str.Length;
                }
            }

            // Depending on location of vowels, split the word into three parts
            // 母音の位置によって、語を３パートに分割する
            firstPart = str.Substring(0, firstVowel + 1);
            secondPart = str.Substring(firstVowel + 1, secondVowel - firstVowel);
            thirdPart = str.Substring(secondVowel + 1);

            // Convert first part into katakana
            // １パート目をカタカナに変換する
            firstPart = firstPart.Replace("y", "i");
            viet = origStr.Substring(0, firstVowel + 1);
            kata = "";
            roma = "";
            viet2 = "";
            kata2 = "";
            roma2 = "";
            switch (firstPart)
            {
                case "a": kata = "ア"; roma = "a"; break;
                case "ba": kata = "バ"; roma = "ba"; break;
                case "be": kata = "ベ"; roma = "be"; break;
                case "bi": kata = "ビ"; roma = "bi"; break;
                case "bo": kata = "ボ"; roma = "bo"; break;
                case "bu": kata = "ブ"; roma = "bu"; break;
                case "ca": kata = "カ"; roma = "ka"; break;
                case "co": kata = "コ"; roma = "ko"; break;
                case "cu": kata = "ク"; roma = "ku"; break;
                case "cha": kata = "チャ"; roma = "cha"; break;
                case "che": kata = "チェ"; roma = "che"; break;
                case "chi": kata = "チ"; roma = "chi"; break;
                case "cho": kata = "チョ"; roma = "cho"; break;
                case "chu": kata = "チュ"; roma = "chu"; break;
                case "da": kata = "ザ"; roma = "za"; break;
                case "de": kata = "ゼ"; roma = "ze"; break;
                case "di": kata = "ジ"; roma = "ji"; break;
                case "do": kata = "ゾ"; roma = "zo"; break;
                case "du": kata = "ズ"; roma = "zu"; break;
                case "đa": kata = "ダ"; roma = "da"; break;
                case "đe": kata = "デ"; roma = "de"; break;
                case "đi": kata = "ディ"; roma = "di"; break;
                case "đo": kata = "ド"; roma = "do"; break;
                case "đu": kata = "ドゥ"; roma = "du"; break;
                case "e": kata = "エ"; roma = "e"; break;
                case "ga": kata = "ガ"; roma = "ga"; break;
                case "ghe": kata = "ゲ"; roma = "ge"; break;
                case "ghi": kata = "ギ"; roma = "gi"; break;
                case "gi": kata = "ジ"; roma = "ji"; break;
                case "gia": kata = "ジャ"; roma = "ja"; break;
                case "gie": kata = "ジェ"; roma = "je"; break;
                case "gio": kata = "ジョ"; roma = "jo"; break;
                case "giu": kata = "ジュ"; roma = "ju"; break;
                case "go": kata = "ゴ"; roma = "go"; break;
                case "gu": kata = "グ"; roma = "gu"; break;
                case "ha": kata = "ハ"; roma = "ha"; break;
                case "he": kata = "ヘ"; roma = "he"; break;
                case "hi": kata = "ヒ"; roma = "hi"; break;
                case "ho": kata = "ホ"; roma = "ho"; break;
                case "hu": kata = "フ"; roma = "fu"; break;
                case "i": kata = "イ"; roma = "i"; break;
                case "ke": kata = "ケ"; roma = "ke"; break;
                case "kha": kata = "カ"; roma = "ka"; break;
                case "khe": kata = "ケ"; roma = "ke"; break;
                case "khi": kata = "キ"; roma = "ki"; break;
                case "kho": kata = "コ"; roma = "ko"; break;
                case "khu": kata = "ク"; roma = "ku"; break;
                case "ki": kata = "キ"; roma = "ki"; break;
                case "la": kata = "ラ"; roma = "ra"; break;
                case "le": kata = "レ"; roma = "re"; break;
                case "li": kata = "リ"; roma = "ri"; break;
                case "lo": kata = "ロ"; roma = "ro"; break;
                case "lu": kata = "ル"; roma = "ru"; break;
                case "ma": kata = "マ"; roma = "ma"; break;
                case "me": kata = "メ"; roma = "me"; break;
                case "mi": kata = "ミ"; roma = "mi"; break;
                case "mo": kata = "モ"; roma = "mo"; break;
                case "mu": kata = "ム"; roma = "mu"; break;
                case "na": kata = "ナ"; roma = "na"; break;
                case "ne": kata = "ネ"; roma = "ne"; break;
                case "nga": kata = "ガ"; roma = "ga"; break;
                case "nghe": kata = "ゲ"; roma = "ge"; break;
                case "nghi": kata = "ギ"; roma = "gi"; break;
                case "ngo": kata = "ゴ"; roma = "go"; break;
                case "ngu": kata = "グ"; roma = "gu"; break;
                case "nha": kata = "ニャ"; roma = "nya"; break;
                case "nhe": kata = "ニェ"; roma = "nye"; break;
                case "nhi": kata = "ニ"; roma = "ni"; break;
                case "nho": kata = "ニョ"; roma = "nyo"; break;
                case "nhu": kata = "ヌ"; roma = "nu"; break;
                case "ni": kata = "ニ"; roma = "ni"; break;
                case "no": kata = "ノ"; roma = "no"; break;
                case "nu": kata = "ヌ"; roma = "nu"; break;
                case "o": kata = "オ"; roma = "o"; break;
                case "pa": kata = "パ"; roma = "pa"; break;
                case "pe": kata = "ペ"; roma = "pe"; break;
                case "pha": kata = "ファ"; roma = "fa"; break;
                case "phe": kata = "フェ"; roma = "fe"; break;
                case "phi": kata = "フィ"; roma = "fi"; break;
                case "pho": kata = "フォ"; roma = "fo"; break;
                case "phu": kata = "フ"; roma = "fu"; break;
                case "pi": kata = "ピ"; roma = "pi"; break;
                case "po": kata = "ポ"; roma = "po"; break;
                case "pu": kata = "プ"; roma = "pu"; break;
                case "qua": kata = "ク"; roma = "ku"; kata2 = "ア"; roma2 = "a"; viet2 = viet.Substring(2); viet = "qu"; break;
                case "que": kata = "ク"; roma = "ku"; kata2 = "エ"; roma2 = "e"; viet2 = viet.Substring(2); viet = "qu"; break;
                case "qui": kata = "ク"; roma = "ku"; kata2 = "イ"; roma2 = "i"; viet2 = viet.Substring(2); viet = "qu"; break;
                case "quo": kata = "ク"; roma = "ku"; kata2 = "オ"; roma2 = "o"; viet2 = viet.Substring(2); viet = "qu"; break;
                case "ra": kata = "ラ"; roma = "ra"; break;
                case "re": kata = "レ"; roma = "re"; break;
                case "ri": kata = "リ"; roma = "ri"; break;
                case "ro": kata = "ロ"; roma = "ro"; break;
                case "ru": kata = "ル"; roma = "ru"; break;
                case "sa": kata = "サ"; roma = "sa"; break;
                case "se": kata = "セ"; roma = "se"; break;
                case "si": kata = "シ"; roma = "shi"; break;
                case "so": kata = "ソ"; roma = "so"; break;
                case "su": kata = "ス"; roma = "su"; break;
                case "ta": kata = "タ"; roma = "ta"; break;
                case "te": kata = "テ"; roma = "te"; break;
                case "tha": kata = "タ"; roma = "ta"; break;
                case "the": kata = "テ"; roma = "te"; break;
                case "thi": kata = "ティ"; roma = "ti"; break;
                case "tho": kata = "ト"; roma = "to"; break;
                case "thu": kata = "トゥ"; roma = "tu"; break;
                case "ti": kata = "ティ"; roma = "ti"; break;
                case "to": kata = "ト"; roma = "to"; break;
                case "tra": kata = "チャ"; roma = "cha"; break;
                case "tre": kata = "チェ"; roma = "che"; break;
                case "tri": kata = "チ"; roma = "chi"; break;
                case "tro": kata = "チョ"; roma = "cho"; break;
                case "tru": kata = "チュ"; roma = "chu"; break;
                case "tu": kata = "トゥ"; roma = "tu"; break;
                case "u": kata = "ウ"; roma = "u"; break;
                case "va": kata = "ヴァ"; roma = "va"; break;
                case "ve": kata = "ヴェ"; roma = "ve"; break;
                case "vi": kata = "ヴィ"; roma = "vi"; break;
                case "vo": kata = "ヴォ"; roma = "vo"; break;
                case "vu": kata = "ヴ"; roma = "vu"; break;
                case "xa": kata = "サ"; roma = "sa"; break;
                case "xe": kata = "セ"; roma = "se"; break;
                case "xi": kata = "シ"; roma = "shi"; break;
                case "xo": kata = "ソ"; roma = "so"; break;
                case "xu": kata = "ス"; roma = "su"; break;
                default: kata = firstPart; roma = ""; break;
            }
            tmp += kata;
            viet = viet.ToUpper();
            viet2 = viet2.ToUpper();
            if (!kata.Equals(""))
            {
                if (viet.Equals("") || roma.Equals(""))
                {
                    res += "<div class=\"jp-button-name jp-button-emp\"><p class=\"jp-name-viet jp-emp\">・</p><p class=\"jp-name-text\">" + kata + "</p><p class=\"jp-name-romaji jp-emp\">・</p></div>";
                }
                else {
                    res += "<div class=\"jp-button-name\"><p class=\"jp-name-viet\">" + viet + "</p><p class=\"jp-name-text\">" + kata + "</p><p class=\"jp-name-romaji\">" + roma + "</p></div>";
                }
            }
            if (!kata2.Equals(""))
            {
                if (viet2.Equals("") || roma2.Equals(""))
                {
                    res += "<div class=\"jp-button-name jp-button-emp\"><p class=\"jp-name-viet jp-emp\">・</p><p class=\"jp-name-text\">" + kata2 + "</p><p class=\"jp-name-romaji jp-emp\">・</p></div>";
                }
                else
                {
                    res += "<div class=\"jp-button-name\"><p class=\"jp-name-viet\">" + viet2 + "</p><p class=\"jp-name-text\">" + kata2 + "</p><p class=\"jp-name-romaji\">" + roma2 + "</p></div>";
                }
            }

            // Convert second part into katakana
            // ２パート目をカタカナに変換する
            if (secondVowel + 1 > str.Length && firstVowel + 1 > str.Length)
            {
                viet = "";
            }
            else
            {
                viet = origStr.Substring(firstVowel + 1, secondVowel - firstVowel);
            }
            kata = "";
            roma = "";
            viet2 = "";
            kata2 = "";
            roma2 = "";
            switch (secondPart)
            {
                case "a": kata = "ア"; roma = "a"; break;
                case "e": kata = "エ"; roma = "e"; break;
                case "i": kata = "イ"; roma = "i"; break;
                case "o": kata = "オ"; roma = "o"; break;
                case "u": if (res.Equals("ウ") || res.Equals("ク") || res.Equals("グ") || res.Equals("ス") || res.Equals("ズ") || res.Equals("ツ") || res.Equals("トゥ") || res.Equals("ドゥ") || res.Equals("ヌ") || res.Equals("フ") || res.Equals("ブ") || res.Equals("プ") || res.Equals("ム") || res.Equals("ユ") || res.Equals("ル") || res.Equals("チュ") || res.Equals("ジュ")) { kata = "ー"; roma = ""; } else { kata = "ウ"; roma = "u"; } break;
                case "y": if (!thirdPart.Equals("e") && !thirdPart.Equals("en") && !thirdPart.Equals("et")) { kata = "イ"; roma = "i"; } break;
                default: kata = secondPart; roma = ""; break;
            }
            
            tmp += kata;
            viet = viet.ToUpper();
            viet2 = viet2.ToUpper();
            if (!kata.Equals(""))
            {
                if (viet.Equals("") || roma.Equals(""))
                {
                    res += "<div class=\"jp-button-name jp-button-emp\"><p class=\"jp-name-viet jp-emp\">・</p><p class=\"jp-name-text\">" + kata + "</p><p class=\"jp-name-romaji jp-emp\">・</p></div>";
                }
                else
                {
                    res += "<div class=\"jp-button-name\"><p class=\"jp-name-viet\">" + viet + "</p><p class=\"jp-name-text\">" + kata + "</p><p class=\"jp-name-romaji\">" + roma + "</p></div>";
                }
            }
            if (!kata2.Equals(""))
            {
                if (viet2.Equals("") || roma2.Equals(""))
                {
                    res += "<div class=\"jp-button-name jp-button-emp\"><p class=\"jp-name-viet jp-emp\">・</p><p class=\"jp-name-text\">" + kata2 + "</p><p class=\"jp-name-romaji jp-emp\">・</p></div>";
                }
                else
                {
                    res += "<div class=\"jp-button-name\"><p class=\"jp-name-viet\">" + viet2 + "</p><p class=\"jp-name-text\">" + kata2 + "</p><p class=\"jp-name-romaji\">" + roma2 + "</p></div>";
                }
            }

            // Convert third part into katakana
            // ３パート目をカタカナに変換する
            if (secondVowel + 1 > str.Length && firstVowel + 1 > str.Length)
            {
                viet = "";
            }
            else
            {
                viet = origStr.Substring(secondVowel + 1);
            }
            
            kata = "";
            roma = "";
            viet2 = "";
            kata2 = "";
            roma2 = "";
            switch (thirdPart)
            {
                case "": if (tmp.Length == 1) { kata = "ー"; roma = ""; } break;
                case "a": kata = "ア"; roma = "a"; break;
                case "c": kata = "ック"; roma = "kku"; break;
                case "ch": kata = "ック"; roma = "kku"; break;
                //case "e": kata = "エ"; roma = "e"; break;
                case "en": kata = "エ"; roma = "e"; kata2 = "ン"; roma2 = "n"; viet2 = "n"; viet = viet.Substring(0, 1); break;
                case "et": kata = "エ"; roma = "e"; kata2 = "ット"; roma2 = "tto"; viet2 = "t"; viet.Substring(0, 1); break;
                case "i": kata = "イ"; roma = "i"; break;
                case "m": kata = "ム"; roma = "mu"; break;
                case "n": kata = "ン"; roma = "n"; break;
                case "ng": kata = "ン"; roma = "n"; break;
                case "nh": if (str.Length >= 3) { if (strc[str.Length - 3].Equals('a')) { kata = "イ"; roma = "i"; } } kata += "ン"; roma += "n"; break;
                case "o": kata = "オ"; roma = "o"; break;
                case "p": kata = "ップ"; roma = "ppu"; break;
                case "t": kata = "ット"; roma = "tto"; break;
                case "u": kata = "ウ"; roma = "u"; break;
                case "y": kata = "イ"; roma = "i"; break;
                default: kata = thirdPart; roma = ""; break;
            }
            if (secondPart.Equals("y"))
            {
                if (!thirdPart.Equals("e") && !thirdPart.Equals("en") && !thirdPart.Equals("et"))
                {
                }
                else
                {
                    viet = "y" + viet;
                }
            }
            tmp += kata;
            viet = viet.ToUpper();
            viet2 = viet2.ToUpper();
            if (!kata.Equals(""))
            {
                if (viet.Equals("") || roma.Equals(""))
                {
                    res += "<div class=\"jp-button-name jp-button-emp\"><p class=\"jp-name-viet jp-emp\">・</p><p class=\"jp-name-text\">" + kata + "</p><p class=\"jp-name-romaji jp-emp\">・</p></div>";
                }
                else
                {
                    res += "<div class=\"jp-button-name\"><p class=\"jp-name-viet\">" + viet + "</p><p class=\"jp-name-text\">" + kata + "</p><p class=\"jp-name-romaji\">" + roma + "</p></div>";
                }
            }
            if (!kata2.Equals(""))
            {
                if (viet2.Equals("") || roma2.Equals(""))
                {
                    res += "<div class=\"jp-button-name jp-button-emp\"><p class=\"jp-name-viet jp-emp\">・</p><p class=\"jp-name-text\">" + kata2 + "</p><p class=\"jp-name-romaji jp-emp\">・</p></div>";
                }
                else
                {
                    res += "<div class=\"jp-button-name\"><p class=\"jp-name-viet\">" + viet2 + "</p><p class=\"jp-name-text\">" + kata2 + "</p><p class=\"jp-name-romaji\">" + roma2 + "</p></div>";
                }
            }

            return res;
        }

        // ==============================================
        // CONVERT VIETNAMESE SENTENCE TO KATAKANA (HTML)
        // Description: This function converts a Vietnamese sentence into katakana, replacing spaces with ・
        // (for example: Nguyễn Nhật Minh -> グエン・ニャット・ミン)
        // 
        // ベトナム語の文をカタカナに変換する（HTML）
        // 説明：このメソッドは、ベトナム語の文をカタカナに変換する。２語以上があったら、「 」が「・」になる。
        //　　　（例：Nguyễn Nhật Minh　→　グエン・ニャット・ミン)
        // ==============================================
        public string ConvertFullNameAdv(string str)
        {
            str.Trim();
            while(str.Contains("  "))
            {
                str = str.Replace("  ", " ");
            }

            // Remove punctuation marks
            // 約物を削除する
            str = str.Replace(",", "");
            str = str.Replace(".", "");
            str = str.Replace(";", "");
            str = str.Replace(":", "");
            str = str.Replace("(", "");
            str = str.Replace(")", "");
            str = str.Replace("[", "");
            str = str.Replace("]", "");
            str = str.Replace("{", "");
            str = str.Replace("}", "");
            str = str.Replace("<", "");
            str = str.Replace(">", "");
            str = str.Replace("'", "");
            str = str.Replace("\"", "");
            str = str.Replace("?", "");
            str = str.Replace("!", "");
            str = str.Replace("-", "");

            // Split the input into a string array, each element contains a word
            // strを分割して、string配列を作る
            string[] stra = str.Split(' ');

            // Declare variable
            // 変数を宣言する
            string res = "";

            // Convert each word into katakana, separating them with ・
            // １語ずつをカタカナに変換する
            for (int i = 0; i < stra.Length; i++)
            {
                if (i != 0)
                {
                    res += "<div class=\"jp-button-name jp-button-emp\"><p class=\"jp-name-viet jp-emp\">・</p><p class=\"jp-name-text\">・</p><p class=\"jp-name-romaji jp-emp\">・</p></div>";
                }
                res += ConvertNameAdv(stra[i]);
            }

            return res;
        }
    }
}