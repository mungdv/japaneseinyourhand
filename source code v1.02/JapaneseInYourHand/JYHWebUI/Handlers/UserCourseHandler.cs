﻿using System;
using JYHDataAccess.DbService;
using JYHDataAccess.Entities;
using JYHWebUI.Models.DTOs;
using JYHDataAccess.Utilities;
using System.Data.Entity;
using JYHWebUI.Models.ViewRequest;
using System.Data;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace JYHWebUI.Handlers
{
    public class UserCourseHandler
    {
        private JYHDbContext _jyhDbContext;
        public UserCourseHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserCourseHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public int GetProcgressByCourseID(int courseID)
        {
            try
            {
                var userCourse =  _jyhDbContext.UserCourses.Where(uc => uc.CourseID == courseID).FirstOrDefault();
                if(userCourse == null)
                {
                    return 0;
                }else
                {
                    return userCourse.Progress;
                }
            }catch
            {
                return 0;
            }
        }

        public bool CheckExist(int counseID, int userID)
        {
            try
            {
                var userCourse = _jyhDbContext.UserCourses.Where(uc => uc.CourseID == counseID && uc.UserID == userID).FirstOrDefault();
                if(userCourse == null)
                {
                    return false;
                }
                return true;
            }catch
            {
                return false;
            }
        }

        public bool Add(UserCourseDto userCourseDto)
        {
            if (CheckExist(userCourseDto.CourseID, userCourseDto.UserID))
            {
                return false;
            }
            else
            {
                UserCourse userCourse = new UserCourse
                {
                    UserID = userCourseDto.UserID,
                    CourseID = userCourseDto.CourseID,
                    LastLearnedDate = DateTime.Now,
                    Progress = 0,
                    StartDate = DateTime.Now,
                };
                try
                {
                    _jyhDbContext.UserCourses.Add(userCourse);
                    _jyhDbContext.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }



        public bool Update(UserCourseDto userCourseDto)
        {
            if (!CheckExist(userCourseDto.CourseID, userCourseDto.UserID))
            {
                return false;
            }
            try
            {
                UserCourse userCourse = _jyhDbContext.UserCourses
                .FirstOrDefault(ul => ul.CourseID == userCourseDto.CourseID && ul.UserID == userCourseDto.UserID);
                userCourse.Progress = userCourseDto.Progress;
                userCourse.LastLearnedDate = DateTime.Now;
                userCourse.CompleteDate = userCourseDto.CompleteDate;
                _jyhDbContext.Entry(userCourse).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}