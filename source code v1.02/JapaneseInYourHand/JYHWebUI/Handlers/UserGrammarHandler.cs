﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JYHWebUI.Handlers
{
    using JYHDataAccess.DbService;
    using JYHDataAccess.Entities;
    using JYHWebUI.Models.DTOs;
    using JYHDataAccess.Utilities;
    using System.Data.Entity;
    using JYHWebUI.Models.ViewRequest;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;
    public class UserGrammarHandler
    {
        private JYHDbContext _jyhDbContext;
        public UserGrammarHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserGrammarHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public bool checkExist(UserGrammarDto userGrammarDto)
        {
            try
            {
                var userGrammar = _jyhDbContext.UserGrammars.Where(uk => uk.GrammarID == userGrammarDto.GrammarID && uk.UserID == userGrammarDto.UserID).FirstOrDefault();
                if (userGrammar == null)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(UserGrammarDto userGrammarDto)
        {
            if (checkExist(userGrammarDto))
            {
                return false;
            }
            else
            {
                UserGrammar userGrammar = new UserGrammar
                {
                    GrammarID = userGrammarDto.GrammarID,
                    UserID = userGrammarDto.UserID,
                    LearnedDate = DateTime.Now
                };
                try
                {
                    _jyhDbContext.UserGrammars.Add(userGrammar);
                    _jyhDbContext.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public List<UserGrammarDto> Get(int userID, int lessonID)
        {
            try
            {
                var list = _jyhDbContext.LessonGrammars.Where(lk => lk.LessonID == lessonID).Join(_jyhDbContext.UserGrammars.Where(uk => uk.UserID == userID),
                                                            lk => lk.GrammarID,
                                                            uk => uk.GrammarID,
                                                            (lk, uk) => new UserGrammarDto
                                                            {
                                                                UserID = uk.UserID,
                                                                GrammarID = uk.GrammarID,
                                                                LearnedDate = uk.LearnedDate
                                                            }).ToList();
                if(list == null)
                {
                    return new List<UserGrammarDto>();
                }
                return list;
            }
            catch
            {
                return new List<UserGrammarDto>();
            }
        }

        public List<UserGrammarDto> Get(int userID)
        {
            try
            {
                var list = _jyhDbContext.UserGrammars.Where(uk => uk.UserID == userID).Select(uk => new UserGrammarDto
                                                            {
                                                                UserID = uk.UserID,
                                                                GrammarID = uk.GrammarID,
                                                                LearnedDate = uk.LearnedDate
                                                            }).ToList();
                if (list == null)
                {
                    return new List<UserGrammarDto>();
                }
                return list;
            }
            catch
            {
                return new List<UserGrammarDto>();
            }
        }

    }
}