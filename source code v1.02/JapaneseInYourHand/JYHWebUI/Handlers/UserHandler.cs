﻿using System;

namespace JYHWebUI.Handlers
{

    using JYHDataAccess.DbService;
    using JYHDataAccess.Entities;
    using JYHWebUI.Models.DTOs;
    using JYHDataAccess.Utilities;
    using System.Data.Entity;
    using JYHWebUI.Models.ViewRequest;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    public class UserHandler
    {
        private JYHDbContext _jyhDbContext;
        private static readonly object locker = new object();
        public UserHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        private UserDto CreaterAttributeNull(UserDto user)
        {
            if(user.FullName == null)
            {
                user.FullName = "";
            }

            if (user.Address == null)
            {
                user.Address = "";
            }

            if (user.Phone == null)
            {
                user.Phone = "";
            }

            if (user.Identify == null)
            {
                user.Identify = "";
            }


            if (user.Token == null)
            {
                user.Token = "";
            }
            return user;
        }

        #region authenticate
        public string GenerateIDEC()
        {
            string IDEC = "";
            bool check = true;
            Random random = new Random();
            do {
                StringBuilder str = new StringBuilder(IDEC);
                for (int i = 0; i < 8; i++)
                {
                    str.Append(random.Next(0, 9));
                }
                string validate = str.ToString();
                if (_jyhDbContext.Users.Any(u => u.IdentifierID == validate))
                {
                    check = true;
                }
                else
                {
                    check = false;
                    IDEC = str.ToString();
                }
            } while (check);
            return IDEC;
        }

        public string GenerateToken(string header, string IDEC)
        {
            return Utility.TokendHash(IDEC, header);
        }

        public bool CheckSession(CookieDto cookie)
        {
            try
            {
                User user = _jyhDbContext.Users.FirstOrDefault(u => u.Email == cookie.Account);
                if (user != null)
                {
                    if (user.IdentifierID == cookie.IDEC && user.Token == cookie.token)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }catch
            {
                return false;
            }
            
        }


        #endregion
        //convert data from entity of Db to data tranfer object
        public UserDto ConvertUserToDto(User user)
        {
            UserDto userDto = new UserDto();
            userDto.ID = user.ID;
            userDto.Email = user.Email;
            userDto.Password = user.Password;
            userDto.Phone = user.Phone;
            userDto.FullName = user.FullName;
            userDto.Address = user.Address;
            userDto.Level = user.Level;
            userDto.CreatedDate = user.CreatedDate;
            userDto.LastLogin = user.LastLogin;

            return userDto;
        }
        public List<UserDto> Get()
        {
            try
            {
                var list = _jyhDbContext.Users.Where(u => u.Available == true).Select(
                    u => new UserDto
                    {
                        ID = u.ID,
                        Address = u.Address,
                        CreatedDate = u.CreatedDate,
                        Email = u.Email,
                        FullName = u.FullName,
                        LastLogin = u.LastLogin,
                        Level = u.Level,
                        Phone = u.Phone,
                    }).ToList();
                if (list == null) {
                    return new List<UserDto>();
                }
                return list;
            }
            catch
            {
                return new List<UserDto>();
            }
        }

        public List<UserDto> Get(int role)
        {
            try
            {
                var list = new List<UserDto>();
                if (role != 0)
                {
                    list = _jyhDbContext.Users.Where(u => u.Available == true).Join(_jyhDbContext.UserRoles.Where(ul => ul.RoleID == role),
                        u => u.ID,
                        ul => ul.UserID,
                        (u, ul) => new UserDto
                        {
                            ID = u.ID,
                            Address = u.Address,
                            Email = u.Email,
                            FullName = u.FullName,
                            Level = u.Level == null ? 6 : u.Level,
                            Phone = u.Phone,
                        }).Distinct().ToList();
                }
                else
                {
                    list = _jyhDbContext.Users.Where(u => u.Available == true).Select(u => new UserDto
                        {
                            ID = u.ID,
                            Address = u.Address,
                            Email = u.Email,
                            FullName = u.FullName,
                            Level = u.Level == null ? 6 : u.Level,
                            Phone = u.Phone,
                        }).ToList();
                }
                if (list == null)
                {
                    return new List<UserDto>();
                }
                return list;
            }
            catch
            {
                return new List<UserDto>();
            }
        }

        public UserDto GetByID(int id)
        {
            try
            {
                UserDto user = _jyhDbContext.Users.Where(u => u.ID == id && u.Available == true).Select(
                    u => new UserDto
                    {
                        ID = u.ID,
                        Address = u.Address,
                        CreatedDate = u.CreatedDate,
                        Email = u.Email,
                        FullName = u.FullName,
                        LastLogin = u.LastLogin,
                        Level = u.Level,
                        Phone = u.Phone,
                    }).FirstOrDefault();

                if (user != null)
                {
                    return user;
                }
                else
                {
                    return new UserDto();
                }

            }
            catch
            {
                return new UserDto();
            }
        }

        public UserDto Get(string Account)
        {
            try
            {
                UserDto user = _jyhDbContext.Users.Where(u => (u.Email == Account || u.Phone == Account) && u.Available == true).Select(
                    u => new UserDto {
                        ID = u.ID,
                        Address = u.Address,
                        CreatedDate = u.CreatedDate,
                        Email = u.Email,
                        FullName = u.FullName,
                        LastLogin = u.LastLogin,
                        Level = u.Level,
                        Phone = u.Phone,
                        Identify = u.IdentifierID,
                        Token = u.Token
                    }).FirstOrDefault();
            
                if (user != null)
                {
                    return user;
                }
                else
                {
                    return new UserDto();
                }

            }
            catch
            {
                return new UserDto();
            }
        }

        public bool checkUserExist(string email)
        {
            try
            {
                if (_jyhDbContext.Users.Any(u => u.Email == email))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }catch
            {
                return false;
            }
            
        }

        public bool checkPhoneExits(string phone)
        {
            if (_jyhDbContext.Users.Any(u => u.Phone == phone))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool Add(UserDto userDto)
        {
            userDto = CreaterAttributeNull(userDto);
            try
            {
                lock (locker)
                {
                    if (!checkUserExist(userDto.Email))
                    {
                        User user = new User()
                        {
                            Email = userDto.Email,
                            Password = Utility.PasswordHash(userDto.Password),
                            FullName = userDto.FullName,
                            Phone = userDto.Phone,
                            Level = userDto.Level,
                            CreatedDate = DateTime.Now,
                            LastLogin = DateTime.Now,
                            Available = true,
                            IdentifierID = userDto.Identify,
                            Token = userDto.Token,
                            Address = userDto.Address,

                        };
                        _jyhDbContext.Users.Add(user);
                        _jyhDbContext.SaveChanges();

                        userDto = Get(user.Email);
                        new UserStatHandler().Add(userDto.ID);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public bool ChangePassword(UserDto userDto, string newPassword)
        {

            var changePass = _jyhDbContext.Users.FirstOrDefault(u => u.ID == userDto.ID);
            try
            {
                if (changePass.Password.Equals(Utility.PasswordHash(userDto.Password)))
                {
                    changePass.Password = Utility.PasswordHash(newPassword);
                    _jyhDbContext.Entry(changePass).State = EntityState.Modified;
                    _jyhDbContext.SaveChanges();
                    return true;
                }
            }
            catch
            {

            }
            return false;

        }

        public bool ResetPassword(UserDto userDto)
        {
            return true;
        }

        public bool UpdateUserProfile(UserDto userDto, bool password)
        {
            try
            {
                User user = _jyhDbContext.Users.FirstOrDefault(u => u.ID == userDto.ID);
                if (user != null)
                {
                    user.FullName = userDto.FullName;
                    user.Phone = userDto.Phone;
                    user.Address = userDto.Address;
                    user.LastLogin = userDto.LastLogin;
                    
                    if(password)
                    {
                        user.Password = Utility.PasswordHash(userDto.Password);
                    }else
                    {
                        user.Token = userDto.Token;
                        user.IdentifierID = user.IdentifierID;
                    }
                    try
                    {
                        _jyhDbContext.Entry(user).State = EntityState.Modified;
                        _jyhDbContext.SaveChanges();
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
            }catch
            {

            }
            return false;
        }

        public bool CheckAccountExits(string Account, string Password)
        {
            try
            {
                string Pass = Utility.PasswordHash(Password);

                bool check = _jyhDbContext.Users.Where(u => u.Email == Account || u.Phone == Account).Where(u => u.Password == Pass).Any();
                if (check)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }catch
            {
                return false;
            }
            
        }

        public bool Available(string session)
        {
            bool check = false;
            try
            {
                List<User>  users = _jyhDbContext.Users.Where(u => u.IdentifierID == session).ToList();
                foreach (User user in users)
                {
                    string token = GenerateToken(user.Email, session);
                    if (token == user.Token)
                    {
                        check = true;
                        break;
                    }
                }
                if (check)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}