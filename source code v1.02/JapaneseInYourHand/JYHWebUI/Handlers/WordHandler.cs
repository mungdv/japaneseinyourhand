﻿using System;


namespace JYHWebUI.Handlers
{
    using JYHDataAccess.DbService;
    using JYHDataAccess.Entities;
    using JYHWebUI.Models.DTOs;
    using System.Linq;
    using System.Collections.Generic;
    using Excel = Microsoft.Office.Interop.Excel;
    using System.Data;
    using System.Runtime.InteropServices;
    using System.Web;
    using JYHWebUI.Models.ViewRequest;
    using System.Data.Entity;
    using OfficeOpenXml;
    using System.Web.Mvc;
    using JYHDataAccess.Utilities;
    public class WordHandler
    {
        private JYHDbContext _jyhDbContext;

        public WordHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public WordHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public WordDto ConvertWordToDto(Word word)
        {
            WordDto wordDto = new WordDto();
            wordDto.ID = word.ID;
            wordDto.Word = word.Word1;
            wordDto.Reading = word.Reading;
            wordDto.Meaning = word.Meaning;
            wordDto.Type = word.Type;
            wordDto.Common = word.Common;
            wordDto.UsuallyKana = word.UsuallyKana;
            wordDto.SpecialReading = word.SpecialReading;

            return wordDto;
        }


        public List<WordDto> Get(string prefix)
        {
            try
            {
                var list = _jyhDbContext.Words.Where(k => k.Word1.StartsWith(prefix) ||
               k.Reading.StartsWith(prefix) || k.Meaning.StartsWith(prefix)
                ).Select(k => new WordDto
                {
                    ID = k.ID,
                    Word = k.Word1,
                    Reading = k.Reading,
                    Type = k.Type,
                    Meaning = k.Meaning
                }).Take(5).ToList();
                if (list != null)
                {
                    return list;
                }
            }catch
            {

            }
            return new List<WordDto>();
        }

        private bool CheckExist(int id)
        {
            try
            {
                if (_jyhDbContext.Words.FirstOrDefault(wo => wo.ID == id) != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }catch
            {
                return false;
            }
        }
       
        public List<WordDto> DicSearch(string text)
        {
            text = text.Trim();

            try
            {
                var list = _jyhDbContext.Words.OrderBy(w => w.Word1).ToList().Where(w => (w.Word1.StartsWith(text))
                || (w.Reading.StartsWith(text)) 
                || (Utility.RemoveDiacritics(w.Meaning).Contains(Utility.RemoveDiacritics(text)))).Select(word => new WordDto
                {
                    ID = word.ID,
                    Word = word.Word1,
                    Reading = word.Reading,
                    Meaning = word.Meaning,
                    Type = word.Type,
                }).Take(5).ToList();
                if(list != null)
                {
                    return list;
                }
            }catch(Exception)
            {
            }
            return new List<WordDto>();
        }
        public WordDto DicSearch(int id)
        {
            try
            {
                var ws = _jyhDbContext.Words.ToList().Where(w => w.ID == id).Select(word => new WordDto
                {
                    ID = word.ID,
                    Word = word.Word1,
                    Reading = word.Reading,
                    Meaning = word.Meaning,
                    Type = word.Type,
                    Common = word.Common,
                    SpecialReading = word.SpecialReading,
                    UsuallyKana = word.UsuallyKana,
                    WMeaning1 = word.WMeaning1,
                    WMeaning2 = word.WMeaning2,
                    WReading1 = word.WReading1,
                    WReading2 = word.WReading2
                }).FirstOrDefault();
                if(ws != null)
                {
                    return ws;
                }
            }catch
            {
            }
            return new WordDto();
        }

        public List<WordDto> GetRandomVerb()
        {
            try
            {
                
                var ws = _jyhDbContext.Words.Where(w => (w.Type.Equals("động từ nhóm I") || w.Type.Equals("động từ nhóm II"))).Select(word => new WordDto
                {
                    ID = word.ID,
                    Word = word.Word1,
                    Reading = word.Reading,
                    Type = word.Type,
                }).ToList();
                if(ws != null)
                {
                    return ws;
                }
            }
            catch
            {
            }
            return new List<WordDto>();
        }



        public string Conjugate(string word, int wordType, int conjugType) {
            // wordType:
            //  1 - godan verb
            //  2 - ichidan verb
            //  3 - suru/kuru verb
            
            // conjugType:
            //  1 - masu
            //  2 - te
            //  3 - ta
            //  4 - nai
            //  5 - conditional
            //  6 - potential
            //  7 - volitional
            //  8 - command
            //  9 - passive
            // 10 - causative
            try { 
                string res = word;
                string aCol = "";
                string iCol = "";
                string uCol = "";
                string eCol = "";
                string oCol = "";
	    
		        if (wordType == 1) {
			        uCol = res.Substring(res.Length - 1, 1);
			        if (uCol == "う") {
				        aCol = "わ";
				        iCol = "い";
				        eCol = "え";
				        oCol = "お";
			        }
			        else if (uCol == "く") {
				        aCol = "か";
				        iCol = "き";
				        eCol = "け";
				        oCol = "こ";
			        }
			        else if (uCol == "ぐ") {
				        aCol = "が";
				        iCol = "ぎ";
				        eCol = "げ";
				        oCol = "ご";
			        }
			        else if (uCol == "す") {
				        aCol = "さ";
				        iCol = "し";
				        eCol = "せ";
				        oCol = "そ";
			        }
			        else if (uCol == "つ") {
				        aCol = "た";
				        iCol = "ち";
				        eCol = "て";
				        oCol = "と";
			        }
			        else if (uCol == "ぬ") {
				        aCol = "な";
				        iCol = "に";
				        eCol = "ね";
				        oCol = "の";
			        }
			        else if (uCol == "ぶ") {
				        aCol = "ば";
				        iCol = "び";
				        eCol = "べ";
				        oCol = "ぼ";
			        }
			        else if (uCol == "む") {
				        aCol = "ま";
				        iCol = "み";
				        eCol = "め";
				        oCol = "も";
			        }
			        else if (uCol == "る") {
				        aCol = "ら";
				        iCol = "り";
				        eCol = "れ";
				        oCol = "ろ";
			        }
				    if (conjugType == 1) {
					    res = res.Substring(0, res.Length - 1) + iCol + "ます";
				    }
				    else if (conjugType == 2) {
					    if (uCol == "う" || uCol == "つ" || uCol == "る") {
                            if (res.Substring(res.Length - 2, 2).Equals("問う") || res.Substring(res.Length - 2, 2).Equals("乞う") || res.Substring(res.Length - 2, 2).Equals("請う") || res.Substring(res.Length - 2, 2).Equals("恋う"))
                            {
                                res = res.Substring(0, res.Length - 1) + "うて";
                            }
                            else
                            {
                                res = res.Substring(0, res.Length - 1) + "って";
                            }
                        }
					    else if (uCol == "く") {
                            if (res.Substring(res.Length - 2, 2).Equals("いく") || res.Substring(res.Length - 2, 2).Equals("行く"))
                            { 
						        res = res.Substring(0, res.Length - 1) + "って";
                            }
                            else
                            {
                                res = res.Substring(0, res.Length - 1) + "いて";
                            }
                        }
					    else if (uCol == "ぐ") {
						    res = res.Substring(0, res.Length - 1) + "いで";
					    }
					    else if (uCol == "む" || uCol == "ぶ" || uCol == "ぬ") {
						    res = res.Substring(0, res.Length - 1) + "んで";
					    }
					    else if (uCol == "す") {
						    res = res.Substring(0, res.Length - 1) + "して";
					    }
				    }
				    else if (conjugType == 3) {
					    if (uCol == "う" || uCol == "つ" || uCol == "る") {
                            if (res.Substring(res.Length - 2, 2).Equals("問う") || res.Substring(res.Length - 2, 2).Equals("乞う") || res.Substring(res.Length - 2, 2).Equals("請う") || res.Substring(res.Length - 2, 2).Equals("恋う"))
                            {
                                res = res.Substring(0, res.Length - 1) + "うた";
                            }
                            else
                            {
                                res = res.Substring(0, res.Length - 1) + "った";
                            }
                        }
					    else if (uCol == "く") {
                            if (res.Substring(res.Length - 2, 2).Equals("いく") || res.Substring(res.Length - 2, 2).Equals("行く"))
                            {
                                res = res.Substring(0, res.Length - 1) + "った";
                            }
                            else
                            {
                                res = res.Substring(0, res.Length - 1) + "いた";
                            }
                        }
					    else if (uCol == "ぐ") {
						    res = res.Substring(0, res.Length - 1) + "いだ";
					    }
					    else if (uCol == "む" || uCol == "ぶ" || uCol == "ぬ") {
						    res = res.Substring(0, res.Length - 1) + "んだ";
					    }
					    else if (uCol == "す") {
						    res = res.Substring(0, res.Length - 1) + "した";
					    }
				    }
				    else if (conjugType == 4) {
					    res = res.Substring(0, res.Length - 1) + aCol + "ない";
				    }
				    else if (conjugType == 5) {
					    res = res.Substring(0, res.Length - 1) + eCol + "ば";
				    }
				    else if (conjugType == 6) {
					    res = res.Substring(0, res.Length - 1) + eCol + "る";
				    }
				    else if (conjugType == 7) {
					    res = res.Substring(0, res.Length - 1) + oCol + "う";
				    }
				    else if (conjugType == 8) {
					    res = res.Substring(0, res.Length - 1) + eCol;
				    }
				    else if (conjugType == 9) {
					    res = res.Substring(0, res.Length - 1) + aCol + "れる";
				    }
				    else if (conjugType == 10) {
					    res = res.Substring(0, res.Length - 1) + aCol + "せる";
				    }
		        }
		        else if (wordType == 2) {
			        if (conjugType == 1) {
				        res = res.Substring(0, res.Length - 1) + "ます";
			        }
			        else if (conjugType == 2) {
				        res = res.Substring(0, res.Length - 1) + "て";
			        }
			        else if (conjugType == 3) {
				        res = res.Substring(0, res.Length - 1) + "た";
			        }
			        else if (conjugType == 4) {
				        res = res.Substring(0, res.Length - 1) + "ない";
			        }
			        else if (conjugType == 5) {
				        res = res.Substring(0, res.Length - 1) + "れば";
			        }
			        else if (conjugType == 6) {
				        res = res.Substring(0, res.Length - 1) + "られる";
			        }
			        else if (conjugType == 7) {
				        res = res.Substring(0, res.Length - 1) + "よう";
			        }
			        else if (conjugType == 8) {
				        res = res.Substring(0, res.Length - 1) + "ろ";
			        }
			        else if (conjugType == 9) {
				        res = res.Substring(0, res.Length - 1) + "られる";
			        }
			        else if (conjugType == 10) {
				        res = res.Substring(0, res.Length - 1) + "させる";
			        }
		        }
		        else if (wordType == 3) {
                    if (res.Substring(res.Length - 2, 2).Equals("する"))
                    {
                        if (conjugType == 1)
                        {
                            res = res.Substring(0, res.Length - 2) + "します";
                        }
                        else if (conjugType == 2)
                        {
                            res = res.Substring(0, res.Length - 2) + "して";
                        }
                        else if (conjugType == 3)
                        {
                            res = res.Substring(0, res.Length - 2) + "した";
                        }
                        else if (conjugType == 4)
                        {
                            res = res.Substring(0, res.Length - 2) + "しない";
                        }
                        else if (conjugType == 5)
                        {
                            res = res.Substring(0, res.Length - 2) + "すれば";
                        }
                        else if (conjugType == 6)
                        {
                            res = res.Substring(0, res.Length - 2) + "できる";
                        }
                        else if (conjugType == 7)
                        {
                            res = res.Substring(0, res.Length - 2) + "しよう";
                        }
                        else if (conjugType == 8)
                        {
                            res = res.Substring(0, res.Length - 2) + "しろ";
                        }
                        else if (conjugType == 9)
                        {
                            res = res.Substring(0, res.Length - 2) + "される";
                        }
                        else if (conjugType == 10)
                        {
                            res = res.Substring(0, res.Length - 2) + "させる";
                        }
                    }
                    else
                    {
                        if (conjugType == 1)
                        {
                            res = res.Substring(0, res.Length - 2) + "きます";
                        }
                        else if (conjugType == 2)
                        {
                            res = res.Substring(0, res.Length - 2) + "きて";
                        }
                        else if (conjugType == 3)
                        {
                            res = res.Substring(0, res.Length - 2) + "きた";
                        }
                        else if (conjugType == 4)
                        {
                            res = res.Substring(0, res.Length - 2) + "こない";
                        }
                        else if (conjugType == 5)
                        {
                            res = res.Substring(0, res.Length - 2) + "くれば";
                        }
                        else if (conjugType == 6)
                        {
                            res = res.Substring(0, res.Length - 2) + "こられる";
                        }
                        else if (conjugType == 7)
                        {
                            res = res.Substring(0, res.Length - 2) + "こよう";
                        }
                        else if (conjugType == 8)
                        {
                            res = res.Substring(0, res.Length - 2) + "こい";
                        }
                        else if (conjugType == 9)
                        {
                            res = res.Substring(0, res.Length - 2) + "こられる";
                        }
                        else if (conjugType == 10)
                        {
                            res = res.Substring(0, res.Length - 2) + "こさせる";
                        }
                    }
		        }
                return res;
            }
            catch (Exception ex)
            {
                return "(không có thông tin)";
            }
        }

        public Verb AnswerConjugPractice(string word, string reading, int wordType, int conjugType, string inp)
        {
            string res = "";
            string ans1 = Conjugate(word, wordType, conjugType);
            string ans2 = Conjugate(reading, wordType, conjugType);
            if (inp.Equals(ans1) || inp.Equals(ans2))
            {
                res = "OK";
            }
            else
            {
                res = "<div class=\"jp-prac-result\"><p class=\"jp-prac-text\">" + word + "</p><p class=\"jp-prac-wrong\">" + inp + "</p><p class=\"jp-prac-correct\">" + ans2 + "</p></div>";
            }
            var list = GetRandomVerb();
            int index = new Random().Next(list.Count);
            return new Verb {Answer = ans1, Result = res, WordDto = list[index]};
            
        }


        #region import export
        public string Import(HttpPostedFileBase fileUpload)
        {
            try { 
                string fileName = fileUpload.FileName;
                string fileContentType = fileUpload.ContentType;
                byte[] fileBytes = new byte[fileUpload.ContentLength];

                var data = fileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(fileUpload.ContentLength));
                List<Word> listInsert = new List<Word>();
                List<Word> listUpdate = new List<Word>();
                using (var package = new ExcelPackage(fileUpload.InputStream))
                {
                    var currentSheet = package.Workbook.Worksheets;
                    var workSheet = currentSheet.First();
                    var noOfCol = workSheet.Dimension.End.Column;
                    var noOfRow = workSheet.Dimension.End.Row;
                    for (int rowIterator = 2; rowIterator < noOfRow; rowIterator++)
                    {
                        if (workSheet.Cells[rowIterator, 1].Value == null)
                        {
                            workSheet.Cells[rowIterator, 1].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 2].Value == null)
                        {
                            workSheet.Cells[rowIterator, 2].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 3].Value == null)
                        {
                            workSheet.Cells[rowIterator, 3].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 4].Value == null)
                        {
                            workSheet.Cells[rowIterator, 4].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 5].Value == null)
                        {
                            workSheet.Cells[rowIterator, 5].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 6].Value == null)
                        {
                            workSheet.Cells[rowIterator, 6].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 7].Value == null)
                        {
                            workSheet.Cells[rowIterator, 7].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 8].Value == null)
                        {
                            workSheet.Cells[rowIterator, 8].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 9].Value == null)
                        {
                            workSheet.Cells[rowIterator, 9].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 10].Value == null)
                        {
                            workSheet.Cells[rowIterator, 10].Value = "";

                        }
                        if (workSheet.Cells[rowIterator, 11].Value == null)
                        {
                            workSheet.Cells[rowIterator, 11].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 12].Value == null)
                        {
                            workSheet.Cells[rowIterator, 12].Value = "";
                        }

                        if (workSheet.Cells[rowIterator, 13].Value == null)
                        {
                            workSheet.Cells[rowIterator, 13].Value = "0";
                        }
                        var word = new Word();

                        word.ID = int.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                        word.Word1 = workSheet.Cells[rowIterator, 2].Value.ToString();
                        word.Meaning = workSheet.Cells[rowIterator, 3].Value.ToString();
                        word.WMeaning1 = workSheet.Cells[rowIterator, 4].Value.ToString();
                        word.WMeaning2 = workSheet.Cells[rowIterator, 5].Value.ToString();
                        word.Reading = workSheet.Cells[rowIterator, 6].Value.ToString();
                        word.WReading1 = workSheet.Cells[rowIterator, 7].Value.ToString();
                        word.WReading2 = workSheet.Cells[rowIterator, 8].Value.ToString();
                        word.Type = workSheet.Cells[rowIterator, 9].Value.ToString();
                        word.Common = workSheet.Cells[rowIterator, 10].Value.ToString() == "1" ? true : false;
                        word.UsuallyKana = workSheet.Cells[rowIterator, 11].Value.ToString() == "1" ? true : false;
                        word.SpecialReading = workSheet.Cells[rowIterator, 12].Value.ToString() == "1" ? true : false;
                        word.Available = workSheet.Cells[rowIterator, 12].Value.ToString() == "1" ? true : false;
                    

                        if (CheckExist(word.ID))
                        {
                            listUpdate.Add(word);
                        } else
                        {
                            word.Available = true;
                            listInsert.Add(word);
                        }
                    }
                    using (JYHDbContext db = new JYHDbContext())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.BulkInsert(listInsert);
                        db.BulkUpdate(listUpdate);
                        db.SaveChanges();

                    }
                }

                return "Nhập dữ liệu từ vựng thành công. Thêm mới: " + listInsert.Count + ". Cập nhật: " + listUpdate.Count + ". Nhấn phím Back trên trình duyệt để quay trở lại.";
            }
            catch (Exception ex)
            {
                return "Nhập dữ liệu từ vựng thất bại. Hãy kiểm tra lại định dạng file Excel.";
            }
        }

        public FileContentResult Export()
        {

            var fileDownloadName = String.Format("ExportWord.xlsx");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


            // Pass your ef data to method
            ExcelPackage package = GenerateExcelFile(_jyhDbContext.Words.ToList());

            var fsr = new FileContentResult(package.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;
        }

        private static ExcelPackage GenerateExcelFile(IEnumerable<Word> datasource)
        {

            ExcelPackage pck = new ExcelPackage();

            //Create the worksheet 
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Word");

            // Sets Headers
            ws.Cells[1, 1].Value = "ID";
            ws.Cells[1, 2].Value = "Word";
            ws.Cells[1, 3].Value = "Meaning";
            ws.Cells[1, 4].Value = "WMeaning1";
            ws.Cells[1, 5].Value = "WMeaning2";
            ws.Cells[1, 6].Value = "Reading";
            ws.Cells[1, 7].Value = "WReading1";
            ws.Cells[1, 8].Value = "WReading2";
            ws.Cells[1, 9].Value = "Type";
            ws.Cells[1, 10].Value = "Common";
            ws.Cells[1, 11].Value = "UsuallyKana";
            ws.Cells[1, 12].Value = "SpecialReading";
            ws.Cells[1, 13].Value = "Available";

            // Inserts Data
            for (int i = 0; i < datasource.Count(); i++)
            {
                ws.Cells[i + 2, 1].Value = datasource.ElementAt(i).ID;
                ws.Cells[i + 2, 2].Value = datasource.ElementAt(i).Word1;
                ws.Cells[i + 2, 3].Value = datasource.ElementAt(i).Meaning;
                ws.Cells[i + 2, 4].Value = datasource.ElementAt(i).WMeaning1;
                ws.Cells[i + 2, 5].Value = datasource.ElementAt(i).WMeaning2;
                ws.Cells[i + 2, 6].Value = datasource.ElementAt(i).Reading;
                ws.Cells[i + 2, 7].Value = datasource.ElementAt(i).WReading1;
                ws.Cells[i + 2, 8].Value = datasource.ElementAt(i).WReading2;
                ws.Cells[i + 2, 9].Value = datasource.ElementAt(i).Type;
                ws.Cells[i + 2, 10].Value = datasource.ElementAt(i).Common ? "1" : "0";
                ws.Cells[i + 2, 11].Value = datasource.ElementAt(i).UsuallyKana ? "1" : "0";
                ws.Cells[i + 2, 12].Value = datasource.ElementAt(i).SpecialReading ? "1" : "0";
                ws.Cells[i + 2, 13].Value = datasource.ElementAt(i).Available ? "1" : "0";
            }

            // Format Header of Table
            using (ExcelRange rng = ws.Cells["A1:Z1"])
            {
                rng.Style.Font.Bold = true;
            }
            return pck;
        }
        #endregion




    }

}