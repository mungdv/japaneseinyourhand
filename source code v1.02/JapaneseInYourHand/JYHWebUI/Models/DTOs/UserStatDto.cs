﻿
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class UserStatDto
    {
        [DataMember(Name = "userID", EmitDefaultValue = false)]
        public int UserID { get; set; }

        [DataMember(Name = "correctKanji", EmitDefaultValue = false)]
        public int CorrectKanji { get; set; }

        [DataMember(Name = "wrongKanji", EmitDefaultValue = false)]
        public int WrongKanji { get; set; }

        [DataMember(Name = "correctWord", EmitDefaultValue = false)]
        public int CorrectWord { get; set; }

        [DataMember(Name = "wrongWord", EmitDefaultValue = false)]
        public int WrongWord { get; set; }

        [DataMember(Name = "correctGrammar", EmitDefaultValue = false)]
        public int CorrectGrammar { get; set; }

        [DataMember(Name = "wrongGrammar", EmitDefaultValue = false)]
        public int WrongGrammar { get; set; }

        [DataMember(Name = "correctReading", EmitDefaultValue = false)]
        public int CorrectReading { get; set; }

        [DataMember(Name = "wrongReading", EmitDefaultValue = false)]
        public int WrongReading { get; set; }

        [DataMember(Name = "correctListening", EmitDefaultValue = false)]
        public int CorrectListening { get; set; }

        [DataMember(Name = "wrongListening", EmitDefaultValue = false)]
        public int WrongListening { get; set; }

        [DataMember(Name = "totalTime", EmitDefaultValue = false)]
        public int TotalTime { get; set; }
    }
}