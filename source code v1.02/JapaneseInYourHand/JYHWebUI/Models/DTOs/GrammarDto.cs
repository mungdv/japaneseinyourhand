﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class GrammarDto
    {

        [DataMember(Name ="id", EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "structureKanji", EmitDefaultValue = false)]
        public string StructureKanji { get; set; }

        [DataMember(Name = "structureKana", EmitDefaultValue = false)]
        public string StructureKana { get; set; }

        [DataMember(Name = "structureForms", EmitDefaultValue = false)]
        public string StructureForms { get; set; }

        [DataMember(Name = "meaning", EmitDefaultValue = false)]
        public string Meaning { get; set; }

        [DataMember(Name = "usage", EmitDefaultValue = false)]
        public string Usage { get; set; }

        [DataMember(Name = "level", EmitDefaultValue = false)]
        public byte? Level { get; set; }

        [DataMember(Name = "example", EmitDefaultValue = false)]
        public string Example { get; set; }

        [DataMember(Name = "notes", EmitDefaultValue = false)]
        public string Notes { get; set; }
    }
}