﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class WordDto
    {
        [DataMember(Name ="id",EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "word", EmitDefaultValue = false)]
        public string Word { get; set; }
       
        [DataMember(Name = "reading", EmitDefaultValue = false)]
        public string Reading { get; set; }

        [DataMember(Name = "wreading1", EmitDefaultValue = false)]
        public string WReading1 { get; set; }

        [DataMember(Name = "wreading2", EmitDefaultValue = false)]
        public string WReading2 { get; set; }

        [DataMember(Name = "meaning", EmitDefaultValue = false)]
        public string Meaning { get; set; }

        [DataMember(Name = "wmeaning1", EmitDefaultValue = false)]
        public string WMeaning1 { get; set; }

        [DataMember(Name = "wmeaning2", EmitDefaultValue = false)]
        public string WMeaning2 { get; set; }

        [DataMember(Name = "type", EmitDefaultValue = false)]
        public string Type { get; set; }

        [DataMember(Name = "common", EmitDefaultValue = false)]
        public bool Common { get; set; }

        [DataMember(Name = "usuallyKana", EmitDefaultValue = false)]
        public bool UsuallyKana { get; set; }

        [DataMember(Name = "specialReading", EmitDefaultValue = false)]
        public bool SpecialReading { get; set; }
    }
}