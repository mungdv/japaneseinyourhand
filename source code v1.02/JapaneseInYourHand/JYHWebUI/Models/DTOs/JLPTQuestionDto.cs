﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class JLPTQuestionDto
    {
        [DataMember(Name ="id", EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "level", EmitDefaultValue = false)]
        public int Level { get; set; }

        [DataMember(Name = "questionType", EmitDefaultValue = false)]
        public int QuestionType { get; set; }

        [DataMember(Name = "question", EmitDefaultValue = false)]
        public string Question { get; set; }

        [DataMember(Name = "correctAnswer", EmitDefaultValue = false)]
        public string CorrectAnswer { get; set; }

        [DataMember(Name = "wrongAnswer1", EmitDefaultValue = false)]
        public string WrongAnswer1 { get; set; }

        [DataMember(Name = "wrongAnswer2", EmitDefaultValue = false)]
        public string WrongAnswer2 { get; set; }

        [DataMember(Name = "wrongAnswer3", EmitDefaultValue = false)]
        public string WrongAnswer3 { get; set; }

        [DataMember(Name = "audio", EmitDefaultValue = false)]
        public string Audio { get; set; }

        [DataMember(Name = "explanation", EmitDefaultValue = false)]
        public string Explanation { get; set; }
    }
}