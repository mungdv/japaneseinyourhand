﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class LessonWordDto
    {
        [DataMember(Name = "lessonID", EmitDefaultValue = false)]
        public int LessonID { get; set; }

        [DataMember(Name = "wordID", EmitDefaultValue = false)]
        public int WordID { get; set; }
    }
}