﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class LessonKanjiDto
    {
        [DataMember(Name = "lessonID", EmitDefaultValue = false)]
        public int LessonID { get; set; }

        [DataMember(Name = "kanjiID", EmitDefaultValue = false)]
        public int KanjiID { get; set; }
    }
}