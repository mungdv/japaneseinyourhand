﻿using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class UserRoleDto
    {
        [DataMember(Name = "userID", EmitDefaultValue = false)]
        public int UserID { get; set; }

        [DataMember(Name = "roleID", EmitDefaultValue = false)]
        public int RoleID { get; set; }
    }
}