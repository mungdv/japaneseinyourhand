﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JYHWebUI.Models.ViewRequest
{
    public class Alphabet
    {
        public string Answer { get; set; }

        public string Result { get; set; }

        public string New { get; set; }
    }
}