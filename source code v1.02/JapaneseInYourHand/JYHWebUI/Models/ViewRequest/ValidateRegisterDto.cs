﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace JYHWebUI.Models.ViewRequest
{
    public class ValidateRegisterDto
    {
        [DataMember(Name = "attr", EmitDefaultValue = false)]
        public string Attr { get; set; }
        [DataMember(Name = "value", EmitDefaultValue = false)]
        public string Value { get; set; }
    }
}