﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace JYHWebUI.Models.ViewRequest
{
    public class LoginDto
    {
        [DataMember(Name = "Account", EmitDefaultValue = false)]
        public string Account { get; set; }
        [DataMember(Name = "Password", EmitDefaultValue = false)]
        public string Password { get; set; }
        [DataMember(Name = "Expires", EmitDefaultValue = false)]
        public bool Expires { get; set; }
    }
}