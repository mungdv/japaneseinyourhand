﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using JYHDataAccess.Entities;

namespace JYHDataAccess.DbService
{
    public class JYHDbContext : DbContext
    {
        public JYHDbContext(string connstring) : base(connstring)
        {
            Database.SetInitializer<JYHDbContext>(null);
            Database.Connection.ConnectionString = connstring;
        }

        public JYHDbContext()
            : base("JYHDB")
        {
            Database.SetInitializer<JYHDbContext>(null);
        }

        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<JLPTQuestion> JLPTQuestions { get; set; }
        public virtual DbSet<Grammar> Grammars { get; set; }
        public virtual DbSet<Kanji> Kanjis { get; set; }
        public virtual DbSet<Lesson> Lessons { get; set; }
        public virtual DbSet<LessonGrammar> LessonGrammars { get; set; }
        public virtual DbSet<LessonKanji> LessonKanjis { get; set; }
        public virtual DbSet<LessonQuiz> LessonQuizs { get; set; }
        public virtual DbSet<LessonWord> LessonWords { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserStat> UserStats { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserKanji> UserKanjis { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserLesson> UserLessons { get; set; }
        public virtual DbSet<UserCourse> UserCourses { get; set; }
        public virtual DbSet<UserGrammar> UserGrammars { get; set; }
        public virtual DbSet<UserWord> UserWords { get; set; }
        public virtual DbSet<Word> Words { get; set; }

        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
            modelBuilder.Entity<UserLesson>()
            .HasRequired(c => c.User)
            .WithMany()
            .WillCascadeOnDelete(false);
        }
    }

}
