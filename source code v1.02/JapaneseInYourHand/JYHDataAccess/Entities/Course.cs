namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Course
    {

        public Course()
        {
            this.Lessons = new List<Lesson>();
            this.UserCourses = new List<UserCourse>();
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Image { get; set; }

        public Nullable<byte> Level { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public bool Available { get; set; }

        public virtual List<Lesson> Lessons { get; set; }

        public virtual List<UserCourse> UserCourses { get; set; }
    }
}
