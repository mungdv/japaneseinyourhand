

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class Role
    {
        public Role()
        {
            this.UserRoles = new List<UserRole>();
        }
    
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        public bool Available { get; set; }
    
        public virtual List<UserRole> UserRoles { get; set; }
    }
}
