

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class Grammar
    {

        public Grammar()
        {
            this.LessonGrammars = new List<LessonGrammar>();
            this.UserGrammars = new List<UserGrammar>();
        }

        [Key]
        public int ID { get; set; }

        [MaxLength(100)]
        public string StructureKanji { get; set; }

        [MaxLength(100)]
        public string StructureKana { get; set; }

        [MaxLength(500)]
        public string StructureForms { get; set; }

        [MaxLength(500)]
        public string Meaning { get; set; }

        [MaxLength(500)]
        public string Usage { get; set; }

        public Nullable<byte> Level { get; set; }

        public string Example { get; set; }

        public string Notes { get; set; }

        public bool Available { get; set; }

        public virtual List<LessonGrammar> LessonGrammars { get; set; }

        public virtual List<UserGrammar> UserGrammars { get; set; }
    }
}
