

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class LessonKanji
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Lesson")]
        public int LessonID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Kanji")]
        public int KanjiID { get; set; }

    
        public virtual Kanji Kanji { get; set; }

        public virtual Lesson Lesson { get; set; }
    }
}
