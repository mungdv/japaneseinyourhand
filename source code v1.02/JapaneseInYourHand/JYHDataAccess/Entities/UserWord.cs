

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class UserWord
    {
        [Key, Column(Order = 0)]
        [ForeignKey("User")]
        public int UserID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Word")]
        public int WordID { get; set; }

        public DateTime LearnedDate { get; set; }
    
        public virtual User User { get; set; }

        public virtual Word Word { get; set; }
    }
}
