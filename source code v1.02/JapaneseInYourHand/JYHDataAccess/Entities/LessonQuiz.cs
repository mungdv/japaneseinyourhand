

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class LessonQuiz
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("Lesson")]
        public int LessonID { get; set; }

        public string Question { get; set; }

        [MaxLength(200)]
        public string CorrectAnswer { get; set; }

        [MaxLength(200)]
        public string WrongAnswer1 { get; set; }

        [MaxLength(200)]
        public string WrongAnswer2 { get; set; }

        [MaxLength(200)]
        public string WrongAnswer3 { get; set; }

        public string Explanation { get; set; }

        public Nullable<byte> Type { get; set; }

        public virtual Lesson Lesson { get; set; }
    }
}
