﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;


namespace JYHDataAccess.Utilities
{
    public class Utility
    {
        public static string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        /// <summary>
        /// Tạo ra 1 password mới không giống và dài hơn password cũ
        /// PW mới này khó trùng với 1 từ có ý nghĩa
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string PasswordHash(string password)
        {
            string firsthash = GetHashSha256(password);
            int middleindex = password.Length / 2;
            string randomizedpassword = password.Insert(middleindex, firsthash.Substring(0, middleindex));
            return GetHashSha256(randomizedpassword);
        }
        public static string TokendHash(string token,string header)
        {
            HashAlgorithm algorithm = new RIPEMD160Managed();
            byte[] tokenBytes = Encoding.UTF8.GetBytes(header + token);
            byte[] hashedBytes = algorithm.ComputeHash(tokenBytes);
            string hashToken = BitConverter.ToString(hashedBytes);
            return hashToken.ToLower().Replace("-", "");
        }

        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
