﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JYHWebUI.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JYHWebUI.Handlers.Tests
{
    [TestClass()]
    public class ConjugateTest
    {
        // --------------------------------------------------------------------------------
        // ConjugateTestGodanVerb001 ~ ConjugateTestGodanVerb150
        // 
        // These test methods check whether godan verbs are correctly conjugated.
        // これらのテストメソッドは、五段活用を確認する。
        // --------------------------------------------------------------------------------
        [TestMethod]
        public void ConjugateTestAbnormal001()
        {
            var word = "";
            var wordType = 1;
            var conjugType = 1;
            var expected = "(không có thông tin)";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestGodanVerb001()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 1;
            var expected = "言います";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb002()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 2;
            var expected = "言って";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb003()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 3;
            var expected = "言った";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb004()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 4;
            var expected = "言わない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb005()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 5;
            var expected = "言えば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb006()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 6;
            var expected = "言える";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb007()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 7;
            var expected = "言おう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb008()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 8;
            var expected = "言え";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb009()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 9;
            var expected = "言われる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb010()
        {
            var word = "言う";
            var wordType = 1;
            var conjugType = 10;
            var expected = "言わせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb011()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 1;
            var expected = "問います";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb012()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 2;
            var expected = "問うて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb013()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 3;
            var expected = "問うた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb014()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 4;
            var expected = "問わない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb015()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 5;
            var expected = "問えば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb016()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 6;
            var expected = "問える";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb017()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 7;
            var expected = "問おう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb018()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 8;
            var expected = "問え";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb019()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 9;
            var expected = "問われる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb020()
        {
            var word = "問う";
            var wordType = 1;
            var conjugType = 10;
            var expected = "問わせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb021()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 1;
            var expected = "乞います";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb022()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 2;
            var expected = "乞うて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb023()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 3;
            var expected = "乞うた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb024()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 4;
            var expected = "乞わない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb025()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 5;
            var expected = "乞えば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb026()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 6;
            var expected = "乞える";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb027()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 7;
            var expected = "乞おう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb028()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 8;
            var expected = "乞え";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb029()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 9;
            var expected = "乞われる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb030()
        {
            var word = "乞う";
            var wordType = 1;
            var conjugType = 10;
            var expected = "乞わせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb031()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 1;
            var expected = "請います";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb032()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 2;
            var expected = "請うて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb033()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 3;
            var expected = "請うた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb034()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 4;
            var expected = "請わない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb035()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 5;
            var expected = "請えば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb036()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 6;
            var expected = "請える";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb037()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 7;
            var expected = "請おう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb038()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 8;
            var expected = "請え";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb039()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 9;
            var expected = "請われる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb040()
        {
            var word = "請う";
            var wordType = 1;
            var conjugType = 10;
            var expected = "請わせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb041()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 1;
            var expected = "恋います";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb042()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 2;
            var expected = "恋うて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb043()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 3;
            var expected = "恋うた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb044()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 4;
            var expected = "恋わない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb045()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 5;
            var expected = "恋えば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb046()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 6;
            var expected = "恋える";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb047()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 7;
            var expected = "恋おう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb048()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 8;
            var expected = "恋え";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb049()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 9;
            var expected = "恋われる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb050()
        {
            var word = "恋う";
            var wordType = 1;
            var conjugType = 10;
            var expected = "恋わせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb051()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 1;
            var expected = "聞きます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb052()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 2;
            var expected = "聞いて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb053()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 3;
            var expected = "聞いた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb054()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 4;
            var expected = "聞かない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb055()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 5;
            var expected = "聞けば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb056()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 6;
            var expected = "聞ける";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb057()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 7;
            var expected = "聞こう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb058()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 8;
            var expected = "聞け";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb059()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 9;
            var expected = "聞かれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb060()
        {
            var word = "聞く";
            var wordType = 1;
            var conjugType = 10;
            var expected = "聞かせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb061()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 1;
            var expected = "行きます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb062()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 2;
            var expected = "行って";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb063()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 3;
            var expected = "行った";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb064()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 4;
            var expected = "行かない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb065()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 5;
            var expected = "行けば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb066()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 6;
            var expected = "行ける";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb067()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 7;
            var expected = "行こう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb068()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 8;
            var expected = "行け";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb069()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 9;
            var expected = "行かれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb070()
        {
            var word = "行く";
            var wordType = 1;
            var conjugType = 10;
            var expected = "行かせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb071()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 1;
            var expected = "迎えに行きます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb072()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 2;
            var expected = "迎えに行って";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb073()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 3;
            var expected = "迎えに行った";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb074()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 4;
            var expected = "迎えに行かない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb075()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 5;
            var expected = "迎えに行けば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb076()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 6;
            var expected = "迎えに行ける";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb077()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 7;
            var expected = "迎えに行こう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb078()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 8;
            var expected = "迎えに行け";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb079()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 9;
            var expected = "迎えに行かれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb080()
        {
            var word = "迎えに行く";
            var wordType = 1;
            var conjugType = 10;
            var expected = "迎えに行かせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb081()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 1;
            var expected = "持っていきます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb082()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 2;
            var expected = "持っていって";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb083()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 3;
            var expected = "持っていった";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb084()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 4;
            var expected = "持っていかない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb085()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 5;
            var expected = "持っていけば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb086()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 6;
            var expected = "持っていける";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb087()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 7;
            var expected = "持っていこう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb088()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 8;
            var expected = "持っていけ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb089()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 9;
            var expected = "持っていかれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb090()
        {
            var word = "持っていく";
            var wordType = 1;
            var conjugType = 10;
            var expected = "持っていかせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb091()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 1;
            var expected = "継ぎます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb092()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 2;
            var expected = "継いで";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb093()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 3;
            var expected = "継いだ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb094()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 4;
            var expected = "継がない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb095()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 5;
            var expected = "継げば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb096()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 6;
            var expected = "継げる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb097()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 7;
            var expected = "継ごう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb098()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 8;
            var expected = "継げ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb099()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 9;
            var expected = "継がれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb100()
        {
            var word = "継ぐ";
            var wordType = 1;
            var conjugType = 10;
            var expected = "継がせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb101()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 1;
            var expected = "読みます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb102()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 2;
            var expected = "読んで";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb103()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 3;
            var expected = "読んだ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb104()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 4;
            var expected = "読まない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb105()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 5;
            var expected = "読めば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb106()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 6;
            var expected = "読める";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb107()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 7;
            var expected = "読もう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb108()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 8;
            var expected = "読め";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb109()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 9;
            var expected = "読まれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb110()
        {
            var word = "読む";
            var wordType = 1;
            var conjugType = 10;
            var expected = "読ませる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb111()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 1;
            var expected = "呼びます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb112()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 2;
            var expected = "呼んで";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb113()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 3;
            var expected = "呼んだ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb114()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 4;
            var expected = "呼ばない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb115()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 5;
            var expected = "呼べば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb116()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 6;
            var expected = "呼べる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb117()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 7;
            var expected = "呼ぼう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb118()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 8;
            var expected = "呼べ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb119()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 9;
            var expected = "呼ばれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb120()
        {
            var word = "呼ぶ";
            var wordType = 1;
            var conjugType = 10;
            var expected = "呼ばせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb121()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 1;
            var expected = "死にます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb122()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 2;
            var expected = "死んで";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb123()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 3;
            var expected = "死んだ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb124()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 4;
            var expected = "死なない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb125()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 5;
            var expected = "死ねば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb126()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 6;
            var expected = "死ねる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb127()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 7;
            var expected = "死のう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb128()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 8;
            var expected = "死ね";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb129()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 9;
            var expected = "死なれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb130()
        {
            var word = "死ぬ";
            var wordType = 1;
            var conjugType = 10;
            var expected = "死なせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb131()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 1;
            var expected = "話します";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb132()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 2;
            var expected = "話して";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb133()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 3;
            var expected = "話した";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb134()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 4;
            var expected = "話さない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb135()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 5;
            var expected = "話せば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb136()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 6;
            var expected = "話せる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb137()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 7;
            var expected = "話そう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb138()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 8;
            var expected = "話せ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb139()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 9;
            var expected = "話される";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb140()
        {
            var word = "話す";
            var wordType = 1;
            var conjugType = 10;
            var expected = "話させる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb141()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 1;
            var expected = "待ちます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb142()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 2;
            var expected = "待って";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb143()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 3;
            var expected = "待った";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb144()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 4;
            var expected = "待たない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb145()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 5;
            var expected = "待てば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb146()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 6;
            var expected = "待てる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb147()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 7;
            var expected = "待とう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb148()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 8;
            var expected = "待て";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb149()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 9;
            var expected = "待たれる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestGodanVerb150()
        {
            var word = "待つ";
            var wordType = 1;
            var conjugType = 10;
            var expected = "待たせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        // --------------------------------------------------------------------------------
        // ConjugateTestIchidanVerb001 ~ ConjugateTestIchidanVerb020
        // 
        // These test methods check whether ichidan verbs are correctly conjugated.
        // これらのテストメソッドは、一段活用を確認する。
        // --------------------------------------------------------------------------------

        [TestMethod]
        public void ConjugateTestIchidanVerb001()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 1;
            var expected = "見ます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb002()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 2;
            var expected = "見て";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb003()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 3;
            var expected = "見た";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb004()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 4;
            var expected = "見ない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb005()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 5;
            var expected = "見れば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb006()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 6;
            var expected = "見られる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb007()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 7;
            var expected = "見よう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb008()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 8;
            var expected = "見ろ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb009()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 9;
            var expected = "見られる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb010()
        {
            var word = "見る";
            var wordType = 2;
            var conjugType = 10;
            var expected = "見させる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb011()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 1;
            var expected = "食べます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb012()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 2;
            var expected = "食べて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb013()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 3;
            var expected = "食べた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb014()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 4;
            var expected = "食べない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb015()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 5;
            var expected = "食べれば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb016()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 6;
            var expected = "食べられる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb017()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 7;
            var expected = "食べよう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb018()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 8;
            var expected = "食べろ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb019()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 9;
            var expected = "食べられる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestIchidanVerb020()
        {
            var word = "食べる";
            var wordType = 2;
            var conjugType = 10;
            var expected = "食べさせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        // --------------------------------------------------------------------------------
        // ConjugateTestSpecialVerb001 ~ ConjugateTestSpecialVerb030
        // 
        // These test methods check whether suru/kuru verbs are correctly conjugated.
        // これらのテストメソッドは、「する・来る」活用を確認する。
        // --------------------------------------------------------------------------------

        [TestMethod]
        public void ConjugateTestSpecialVerb001()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 1;
            var expected = "勉強します";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb002()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 2;
            var expected = "勉強して";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb003()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 3;
            var expected = "勉強した";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb004()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 4;
            var expected = "勉強しない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb005()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 5;
            var expected = "勉強すれば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb006()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 6;
            var expected = "勉強できる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb007()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 7;
            var expected = "勉強しよう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb008()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 8;
            var expected = "勉強しろ";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb009()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 9;
            var expected = "勉強される";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb010()
        {
            var word = "勉強する";
            var wordType = 3;
            var conjugType = 10;
            var expected = "勉強させる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb011()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 1;
            var expected = "きます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb012()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 2;
            var expected = "きて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb013()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 3;
            var expected = "きた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb014()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 4;
            var expected = "こない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb015()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 5;
            var expected = "くれば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb016()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 6;
            var expected = "こられる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb017()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 7;
            var expected = "こよう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb018()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 8;
            var expected = "こい";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb019()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 9;
            var expected = "こられる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb020()
        {
            var word = "来る";
            var wordType = 3;
            var conjugType = 10;
            var expected = "こさせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConjugateTestSpecialVerb021()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 1;
            var expected = "持ってきます";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb022()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 2;
            var expected = "持ってきて";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb023()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 3;
            var expected = "持ってきた";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb024()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 4;
            var expected = "持ってこない";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb025()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 5;
            var expected = "持ってくれば";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb026()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 6;
            var expected = "持ってこられる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb027()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 7;
            var expected = "持ってこよう";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb028()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 8;
            var expected = "持ってこい";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb029()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 9;
            var expected = "持ってこられる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConjugateTestSpecialVerb030()
        {
            var word = "持ってくる";
            var wordType = 3;
            var conjugType = 10;
            var expected = "持ってこさせる";
            var actual = new WordHandler().Conjugate(word, wordType, conjugType);
            Assert.AreEqual(expected, actual);
        }
    }
}