﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Models.Dic
{
    public class Dictionary
    {
        public List<WordDto> ListWord { get; set; }
        public List<KanjiDto> ListKanji{ get; set; }
        public List<GrammarDto> ListGrammar { get; set; }
    }
}