﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace JYHWebUI.Models.ViewRequest
{
    public class FBInforDto
    {
        [DataMember(Name = "FbId", EmitDefaultValue = false)]
        public string FbId { get; set; }
        [DataMember(Name = "FullName", EmitDefaultValue = false)]
        public string FullName { get; set; }
        [DataMember(Name = "Email", EmitDefaultValue = false)]
        public string Email { get; set; }
    }
}