﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class UserCourseDto
    {
        [DataMember(Name = "userID", EmitDefaultValue = false)]
        public int UserID { get; set; }

        [DataMember(Name = "courseID", EmitDefaultValue = false)]
        public int CourseID { get; set; }

        [DataMember(Name = "progress", EmitDefaultValue = false)]
        public int Progress { get; set; }

        [DataMember(Name = "startDate", EmitDefaultValue = false)]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "completeDate", EmitDefaultValue = false)]
        public DateTime CompleteDate { get; set; }

        [DataMember(Name = "lastLearnedDate", EmitDefaultValue = false)]
        public DateTime LastLearnedDate { get; set; }
    }
}