﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class UserKanjiDto
    {
        [DataMember(Name = "userID", EmitDefaultValue = false)]
        public int UserID { get; set; }

        [DataMember(Name = "KanjiID", EmitDefaultValue = false)]
        public int KanjiID { get; set; }

        [DataMember(Name = "learnedDate", EmitDefaultValue = false)]
        public DateTime LearnedDate { get; set; }
    }
}