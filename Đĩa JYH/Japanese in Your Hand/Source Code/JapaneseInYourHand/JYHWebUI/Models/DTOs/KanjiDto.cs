﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class KanjiDto
    {
        [DataMember(Name ="id",EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "kanji", EmitDefaultValue = false)]
        public string Kanji { get; set; }

        [DataMember(Name = "radical", EmitDefaultValue = false)]
        public string Radical { get; set; }

        [DataMember(Name = "onyomi", EmitDefaultValue = false)]
        public string Onyomi { get; set; }

        [DataMember(Name = "wonyomi1", EmitDefaultValue = false)]
        public string WOnyomi1 { get; set; }

        [DataMember(Name = "wonyomi2", EmitDefaultValue = false)]
        public string WOnyomi2 { get; set; }

        [DataMember(Name = "kunyomi", EmitDefaultValue = false)]
        public string Kunyomi { get; set; }

        [DataMember(Name = "wkunyomi1", EmitDefaultValue = false)]
        public string WKunyomi1 { get; set; }

        [DataMember(Name = "wkunyomi2", EmitDefaultValue = false)]
        public string WKunyomi2 { get; set; }

        [DataMember(Name = "sino", EmitDefaultValue = false)]
        public string Sino { get; set; }

        [DataMember(Name = "meaning", EmitDefaultValue = false)]
        public string Meaning { get; set; }

        [DataMember(Name = "strokeCount", EmitDefaultValue = false)]
        public int StrokeCount { get; set; }

        [DataMember(Name = "mnemonic", EmitDefaultValue = false)]
        public string Mnemonic { get; set; }

        [DataMember(Name = "grade", EmitDefaultValue = false)]
        public int Grade { get; set; }

        [DataMember(Name = "level", EmitDefaultValue = false)]
        public byte? Level { get; set; }

        [DataMember(Name = "notes", EmitDefaultValue = false)]
        public string Notes { get; set; }

    }
}