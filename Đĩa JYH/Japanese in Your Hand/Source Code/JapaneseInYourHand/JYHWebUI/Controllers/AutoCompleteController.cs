﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Controllers
{
    public class AutoCompleteController : Controller
    {
        // GET: AutoComplete
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(string Prefix)
        {
            List<KanjiDto> listKanji = new JYHWebUI.Handlers.KanjiHandler().Get(Prefix);
            return Json(listKanji, JsonRequestBehavior.AllowGet);
        }
    }
}