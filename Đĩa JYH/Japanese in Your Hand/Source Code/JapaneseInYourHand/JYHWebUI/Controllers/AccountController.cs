﻿using JYHDataAccess.Entities;
using JYHDataAccess.Utilities;
using JYHWebUI.Handlers;
using JYHWebUI.Models.DTOs;
using JYHWebUI.Models.ViewRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Facebook;
using System.Configuration;
using Google;

namespace JYHWebUI.Controllers
{
    public class AccountController : Controller
    {
        UserHandler userHandler = new UserHandler();

        

        #region Login
        private void SetCookeAndSession(LoginDto form)
        {
            if (userHandler.checkUserExist(form.Account))
            {
                UserDto user = userHandler.Get(form.Account);
                user.LastLogin = DateTime.Now;
                userHandler.UpdateUserProfile(user);
                string IDEC = userHandler.GenerateIDEC();// IDEC viết tắt của IdentifierCode chuỗi tự sinh để sinh cookie lưu phiên đăng nhập
                string token = userHandler.GenerateToken(user.Email, IDEC);//tránh việc IDEC ramdom ra trùng hợp thì thêm account của người dùng vào đầu chuỗi token được hash từ IDEC (mỗi user có 1 account không trùng lặp)
                CookieDto cookie = new CookieDto { Account = user.Email, IDEC = IDEC, token = token };
                HttpCookie aCookie = new HttpCookie(Common.Common.account, cookie.Account);
                HttpCookie iCookie = new HttpCookie(Common.Common.identifier, cookie.IDEC);
                HttpCookie tCookie = new HttpCookie(Common.Common.token, cookie.token);
                if (form.Expires)
                {
                    DateTime time = DateTime.Now.AddDays(30);
                    tCookie.Expires = time;
                    iCookie.Expires = time;
                    aCookie.Expires = time;
                }
                else
                {
                    tCookie.Expires.AddHours(0);
                    iCookie.Expires.AddHours(0);
                    aCookie.Expires.AddHours(0);
                }

                aCookie.Domain = Request.Url.Host;
                iCookie.Domain = Request.Url.Host;
                tCookie.Domain = Request.Url.Host;
                HttpContext.Response.SetCookie(aCookie);
                HttpContext.Response.SetCookie(iCookie);
                HttpContext.Response.SetCookie(tCookie);
                user.Token = cookie.token;
                user.Identify = cookie.IDEC;
                Session[Common.Common.user] = user;
                userHandler.UpdateUserProfile(user); //lưu cả IDEC với token vào db để xác thực
                

            }
        }

        public ActionResult Login()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            if (session == null)
            {
                return View("Login");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }
        [HttpPost]
        public string CheckLogin(string account, string password)
        {
            if(string.IsNullOrEmpty(account) || string.IsNullOrEmpty(password))
            {
                return "Tài khoản và mật khẩu không được để trống";
            }
            if (!userHandler.CheckAccountExits(account, password))
            {
                return "Sai tài khoản hoặc mật khẩu.";
            }
            else
            {
                return "";
            }
        }
        [HttpPost]
        public string Login(LoginDto form)
        {
            if (userHandler.CheckAccountExits(form.Account, form.Password))
            {
                SetCookeAndSession(form);
            }
            return "";
        }
        #endregion

        #region Logout
        [HttpPost]
        public bool Logout()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            session.Identify = "";
            session.Token = "";
            
            HttpCookie ACookie = Request.Cookies[Common.Common.account];
            HttpCookie ICookie = Request.Cookies[Common.Common.identifier];
            HttpCookie TCookie = Request.Cookies[Common.Common.token];
            if (ACookie != null || ICookie != null || TCookie != null)
            {
                HttpContext.Response.Cookies.Remove(Common.Common.account);
                HttpContext.Response.Cookies.Remove(Common.Common.identifier);
                HttpContext.Response.Cookies.Remove(Common.Common.token);
                ACookie.Expires = DateTime.Now.AddDays(-1);
                ICookie.Expires = DateTime.Now.AddDays(-1);
                TCookie.Expires = DateTime.Now.AddDays(-1);
                ACookie.Value = null;
                ICookie.Value = null;
                TCookie.Value = null;
                HttpContext.Response.SetCookie(ACookie);
                HttpContext.Response.SetCookie(ICookie);
                HttpContext.Response.SetCookie(TCookie);
            }
            Session[Common.Common.user] = null;
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            userHandler.UpdateUserProfile(session);
            RedirectToAction("Login", "Account");

            return true;
        }

        #endregion

        #region Login With facebook

        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallback");
                return uriBuilder.Uri;
            }

        }


        public ActionResult LoginFacebook()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FbAppId"],
                client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email"
            });
            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult FacebookCallback(string code)
        {
            try
            {
                var fb = new FacebookClient();
                dynamic result = fb.Post("oauth/access_token", new
                {
                    client_id = ConfigurationManager.AppSettings["FbAppId"],
                    client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                    redirect_uri = RedirectUri.AbsoluteUri,
                    code = code
                });

                var accessToken = result.access_token;
                if (!string.IsNullOrEmpty(accessToken))
                {
                    fb.AccessToken = accessToken;
                    dynamic me = fb.Get("me?fields=first_name,middle_name,last_name,email");//,user_address, user_mobile_phone,,avatar
                    var user = new UserDto();
                    user.Email = me.email;
                    user.FullName = me.first_name + " " + me.middle_name + " " + me.last_name;
                    //user.Avatar = me.avatar;
                    //user.Address = me.address;
                    //user.Phone = me.phone;

                    user.Level = 5;
                    if (string.IsNullOrEmpty(user.Email))
                    {
                        user.Email = me.id;
                    }
                    if (!userHandler.checkUserExist(user.Email))
                    {
                        user.Password = "Abc@123";
                        userHandler.Add(user);
                    }
                    SetCookeAndSession(new LoginDto { Account = user.Email, Password = user.Password, Expires = false});
                    
                }
                return RedirectToAction("Index", "Home");
            }
            catch(Exception e)
            {
                return RedirectToAction("Login", "Account");
            }
        }

        #endregion

        #region Login with Google
        [HttpPost]
        public ActionResult LoginGoogle([Bind(Include = "FullName,Email")]UserDto user)
        {
            if (!userHandler.checkUserExist(user.Email))
            {
                user.Password = "Abc@123";
                userHandler.Add(user);
            }else
            {
                user = userHandler.Get(user.Email);
            }
            SetCookeAndSession(new LoginDto { Account = user.Email,  Password = user.Password, Expires = false});
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Register
        public ActionResult Register()
        {
            return View("Register");
        }

        [HttpPost]
        public JsonResult CheckExist([Bind(Include = "Attr,Value")]ValidateRegisterDto obj)
        {
            string result = "";
            if (obj.Attr.Equals("Email"))
            {
                var address = true;
                try
                {
                    var mail = new System.Net.Mail.MailAddress(obj.Value);
                    address = mail.Address.Equals(obj.Value);
                }catch (Exception)
                {
                    address = false; 
                }

                if(!address)
                {
                    result = "Địa chỉ email của bạn nhập không hợp lệ";
                }
                else if (userHandler.checkUserExist(obj.Value))
                {
                    result = "Địa chỉ email đã tồn tại!";
                }
                else { result = ""; }
            }
            else if (obj.Attr.Equals("Phone"))
            {
                var isNum = false;
                try
                {
                    long.Parse(obj.Value);
                    isNum = true;
                    if(obj.Value.Length < 8 || obj.Value.Length > 15)
                    {
                        isNum = false;
                    }
                }catch (Exception)
                {
                    isNum = false;
                }

                if (!isNum)
                {
                    result = "Số điện thoại không hợp lệ";
                }
                else if (userHandler.checkPhoneExits(obj.Value))
                {
                    result = "Số điện thoại đã tồn tại!";
                }
                else { result = ""; }
            }
            return Json(result);
        }

        [HttpPost]
        public void Register([Bind(Include = "FullName,Email,Phone,Level,Password")]UserDto user)
        {
            userHandler.Add(user);
        }
        #endregion

        #region update profile
        [HttpGet]
        public ActionResult Profile()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            if(session != null)
            {
                ViewBag.UserInfo = userHandler.Get(session.Email);
                ViewBag.LearnInfor = new UserStatHandler().Get(session.ID);
                ViewBag.LessonLearned = new UserLessonHandler().GetUserLessonLearned(session.ID);
                ViewBag.KanjiLearned = new UserKanjiHandler().Get(session.ID);
                ViewBag.WordLearned = new UserWordHandler().Get(session.ID);
                ViewBag.GrammarLearned = new UserGrammarHandler().Get(session.ID);
            }
            return View("Profile");
        }
        #endregion
    }
}