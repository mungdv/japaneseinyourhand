﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JYHWebUI.Controllers
{
    public class AlphabetController : Controller
    {

        // GET: Alphabet
        public ActionResult Index()
        {
            return View(new Handlers.AlphabetHandler().GetAlphabet());
        }

        public ActionResult Convert()
        {
            return View(new Handlers.AlphabetHandler().GetAlphabet());
        }

        public ActionResult PracticeHiragana()
        {
            return View();
        }

        public ActionResult PracticeKatakana()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Hiragana()
        {
            return PartialView("ok", new Handlers.AlphabetHandler().GetAlphabet());
        }

        public JsonResult AnswerPractice(string kana, string roma)
        {
            return Json(new Handlers.AlphabetHandler().AnswerPractice(kana, roma), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAnswer(string kana)
        {
            return Json(new Handlers.AlphabetHandler().GetAnswer(kana), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRandomHiragana()
        {
            return Json(new Handlers.AlphabetHandler().GetRandomHiragana(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRandomKatakana()
        {
            return Json(new Handlers.AlphabetHandler().GetRandomKatakana(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ConvertName(string name)
        {
            return Json(new Handlers.AlphabetHandler().ConvertFullName(name.Trim()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ConvertNameAdv(string name)
        {
            return Json(new Handlers.AlphabetHandler().ConvertFullNameAdv(name.Trim()), JsonRequestBehavior.AllowGet);
        }
    }
}
