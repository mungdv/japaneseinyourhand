﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using JYHWebUI.Handlers;
using System.Web.UI.WebControls;
using System.Web.UI;
using JYHWebUI.Models.DTOs;
using OfficeOpenXml;

using Excel = Microsoft.Office.Interop.Excel;
namespace JYHWebUI.Controllers
{
    public class AdminController : Controller
    {
        CourseHandler courseHandler = new CourseHandler();
        LessonHandler lessonHandler = new LessonHandler();
        LessonWordHandler lessonWordHandler = new LessonWordHandler();
        LessonKanjiHandler lessonKanjiHandler = new LessonKanjiHandler();
        LessonGrammarHandler lessonGrammarHandler = new LessonGrammarHandler();
        LessonQuizHandler lessonQuizHandler = new LessonQuizHandler();
        UserRoleHandler userRoleHandler = new UserRoleHandler();



        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            UserDto user = Session[Common.Common.user] as UserDto;
            if (user != null) { 
                var listRole = userRoleHandler.Get(user.ID);
                if(listRole == null || listRole.Count == 0)
                {
                    filterContext.Result = new RedirectResult(Url.Action("Index", "Home"));
                }
            }
        }

        

        #region checkuserRole
        private bool ManageDictionaryRole()
        {
            UserDto user = Session[Common.Common.user] as UserDto;
            // Role 1: Quản lí từ điển
            return userRoleHandler.Check(user.ID, 1);
        }

        private bool ManageCourseRole()
        {
            UserDto user = Session[Common.Common.user] as UserDto;
            // Role 2: Quản lí khóa học và bài học
            return userRoleHandler.Check(user.ID, 2);
        }

       

        private bool ManageAdminRole()
        {
            UserDto user = Session[Common.Common.user] as UserDto;
            // Role 3: Phân quyền
            return userRoleHandler.Check(user.ID, 3);
        }
        #endregion


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManageDictionary()
        {
            if(!ManageDictionaryRole())
            {
                return View("Index", "Admin");
            }

            return View();
        }
        #region role
        public ActionResult ManageRoles()
        {
            return View("ManageRoles");
        }

        [HttpGet]
        public ActionResult AddUserRole(int userID)
        {
            UserDto user = new UserHandler().GetByID(userID);
            if (user != null)
            {
                @ViewBag.username = user.Email;
            }
            
            ViewBag.listRole = new UserRoleHandler().Get(userID);
            return View("AddUserRole");
        }

        [HttpPost]
        public JsonResult UpdateUserRole(List<int> roles, string email)
        {
            if((Session[Common.Common.user] as UserDto).Email.Equals(email))
            {
                return Json("Bạn Không Thể Tự Thay Đổi Quyền Của Bản Thân");
            }
            var user = new UserHandler().Get(email);
            UserRoleHandler userRoleHandler = new UserRoleHandler();
            var s = new UserRoleHandler().UpdateUserRole(roles, user);
            if(string.IsNullOrEmpty(s))
            {
                return Json("Không Có Gì Thay Đổi");
            }
            return Json(s);
        }

        [HttpPost]
        public JsonResult UserFilter(int role)
        {
            return Json(new UserHandler().Get(role));
        }

        #endregion


        #region import export

        [HttpGet]
        public ActionResult Import()
        {
            return View("Import");
        }

        [HttpPost]
        public JsonResult Import(HttpPostedFileBase fileUpload, string type)
        {
            
            if (fileUpload == null || fileUpload.ContentLength == 0)
            {
                return Json("Hãy chọn file để nhập.");
            }

            if (!fileUpload.FileName.EndsWith("xls") && !fileUpload.FileName.EndsWith("xlsx"))
            {
                return Json("File nhập vào phải là file Excel (.xls hoặc .xlsx).");
            }

            switch(type)
            {
                //1 word, 2 kanji, 3 grammar
                case "1":
                    return Json(new WordHandler().Import(fileUpload));
                case "2":
                    return Json(new KanjiHandler().Import(fileUpload));
                case "3":
                    return Json(new GrammarHandler().Import(fileUpload));
                default:
                    return Json("");
            }
        }

        public FileContentResult Export(int type)
        {
            switch (type)
            {
                //case 0:
                //    return (new WordHandler().ExportAll());
                case 1:
                    return (new WordHandler().Export());
                case 2:
                    return (new KanjiHandler().Export());
                case 3:
                    return (new GrammarHandler().Export());
                default:
                    return null;

            }

        }
        #endregion


        #region lesson
        [HttpPost]
        public bool AddLesson([Bind(Include = "Name,CourseID,Description,Video,Dialogue,DialogueAudio,CultureNotes")]LessonDto lessonDto)
        {
            return new LessonHandler().Add(lessonDto);
        }
        
        [HttpPost, ValidateInput(false)]
        public bool UpdateLesson([Bind(Include = "ID,Name,Description,Video,Dialogue,DialogueAudio,CultureNotes")]LessonDto lessonDto)
        {
            return new JYHWebUI.Handlers.LessonHandler().Update(lessonDto);
        }

        [HttpPost]
        public bool DeleteLesson(int id)
        {
            return new LessonHandler().Delete(id);
        }

        public ActionResult AddNewLesson(int courseID)
        {
            if (!ManageCourseRole())
            {
                return View("Index", "Admin");
            }
            ViewBag.CourseID = courseID;
            return View();
        }

        public ActionResult EditLesson(int lessonID)
        {
            if (!ManageCourseRole())
            {
                return View("Index", "Admin");
            }
            ViewBag.LessonDetail = lessonHandler.LessonDetail(lessonID);
            return View();
        }

        public ActionResult ManageLessonContent(int lessonID)
        {
            if (!ManageCourseRole())
            {
                return View("Index", "Admin");
            }
            ViewBag.LessonID = lessonID;
            ViewBag.LessonName = lessonHandler.LessonDetail(lessonID).Name;
            ViewBag.LessonDetail = lessonHandler.LessonDetailD(lessonID);
            return View();
        }
        public ActionResult ManageLesson(int courseID)
        {
            if (!ManageCourseRole())
            {
                return View("Index", "Admin");
            }
            ViewBag.CourseID = courseID;
            ViewBag.CourseDetail = courseHandler.CourseDetail(courseID);
            ViewBag.LessonList = lessonHandler.GetD(courseID);
            return View();
        }
        #endregion

        #region course
        [HttpPost]
        public bool AddCourse([Bind(Include = "Name,Level,Description")]CourseDto courseDto)
        {
            return new CourseHandler().Add(courseDto);
        }

        [HttpPost]
        public bool UpdateCourse([Bind(Include = "ID,Name,Level,Description")]CourseDto courseDto)
        {
            return new CourseHandler().Update(courseDto);
        }

        [HttpPost]
        public bool DeleteCourse(int id)
        {
            return new CourseHandler().Delete(id);
        }

        public ActionResult ManageCourse()
        {
            if (!ManageCourseRole())
            {
                return View("Index", "Admin");
            }
            ViewBag.CourseList = courseHandler.Get();
            return View();
        }

        public ActionResult AddCourse()
        {
            if (!ManageCourseRole())
            {
                return View("Index", "Admin");
            }
            return View();
        }

        public ActionResult EditCourse(int courseID)
        {
            if (!ManageCourseRole())
            {
                return View("Index", "Admin");
            }
            ViewBag.CourseDetail = courseHandler.CourseDetail(courseID);
            return View();
        }
        #endregion

        #region lesson word
        public bool AddLessonWord([Bind(Include = "LessonID, WordID")]LessonWordDto lessonWordDto)
        {
            return new LessonWordHandler().Add(lessonWordDto);
        }

        public bool DeleteLessonWord([Bind(Include = "LessonID, WordID")]LessonWordDto lessonWordDto)
        {
            return new LessonWordHandler().Delete(lessonWordDto);
        }

        [HttpGet]
        public ActionResult AddLessonWord(int lessonID)
        {
            //listKanji = new KanjiHandler().Get();
            ViewBag.Lesson = lessonHandler.LessonDetail(lessonID);
            return View("AddLessonWord");
        }

        #endregion

        #region lesson kanji
        [HttpGet]
        public ActionResult AddLessonKanji(int lessonID)
        {
            //listKanji = new KanjiHandler().Get();
            ViewBag.Lesson = lessonHandler.LessonDetail(lessonID);
            return View("AddLessonKanji");
        }
        
        public bool AddLessonKanji([Bind(Include = "LessonID, KanjiID")]LessonKanjiDto lessonKanjiDto)
        {
            return new LessonKanjiHandler().Add(lessonKanjiDto);
        }

        public bool DeleteLessonKanji([Bind(Include = "LessonID, KanjiID")]LessonKanjiDto lessonKanjiDto)
        {
            return new LessonKanjiHandler().Delete(lessonKanjiDto);
        }
        #endregion

        #region lesson grammar
        [HttpGet]
        public ActionResult AddLessonGrammar(int lessonID)
        {
            //listKanji = new KanjiHandler().Get();
            ViewBag.Lesson = lessonHandler.LessonDetail(lessonID);
            return View("AddLessonGrammar");
        }

        public bool AddLessonGrammar([Bind(Include = "LessonID, GrammarID")]LessonGrammarDto lessonGrammarDto)
        {
            return new LessonGrammarHandler().Add(lessonGrammarDto);
        }

        public bool DeleteLessonGrammar([Bind(Include = "LessonID, GrammarID")]LessonGrammarDto lessonGrammarDto)
        {
            return new LessonGrammarHandler().Delete(lessonGrammarDto);
        }
        #endregion

        #region lesson quiz
        public bool AddLessonQuiz([Bind(Include = "LessonID, Question, CorrectAnswer, wrongAnswer1, wrongAnswer2, wrongAnswer3")]LessonQuizDto lessonQuizDto)
        {
            return new LessonQuizHandler().Add(lessonQuizDto);
        }

        public bool UpdateLessonQuiz([Bind(Include = "LessonID, Question, CorrectAnswer, wrongAnswer1, wrongAnswer2, wrongAnswer3")]LessonQuizDto lessonQuizDto)
        {
            return new LessonQuizHandler().Update(lessonQuizDto);
        }

        public bool DeleteLessonQuiz(int id)
        {
            return new LessonQuizHandler().Delete(id);
        }

        [HttpGet]
        public ActionResult AddLessonQuiz(int lessonID)
        {
            //listKanji = new KanjiHandler().Get();
            ViewBag.Lesson = lessonHandler.LessonDetail(lessonID);
            return View("AddLessonQuiz");
        }

        #endregion

    }
}
