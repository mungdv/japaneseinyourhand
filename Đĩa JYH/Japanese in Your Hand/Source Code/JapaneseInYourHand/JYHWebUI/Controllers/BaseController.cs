﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JYHWebUI.Models.ViewRequest;
using JYHWebUI.Handlers;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Controllers
{
    public class BaseController : Controller
    {
        UserHandler userHandler = new UserHandler();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            
            UserDto user = HttpContext.Session[Common.Common.user] as UserDto;
            if (user == null)
            {
                HttpCookie aCookie = Request.Cookies[Common.Common.account];
                HttpCookie iCookie = Request.Cookies[Common.Common.identifier];
                HttpCookie tCookie = Request.Cookies[Common.Common.token];

                if (aCookie != null && iCookie != null && tCookie != null)
                {
                    CookieDto cookie = new CookieDto();
                    cookie.Account = aCookie.Value;
                    cookie.IDEC = iCookie.Value;
                    cookie.token = tCookie.Value;
                    if (userHandler.CheckSession(cookie))
                    {
                        user = userHandler.Get(cookie.Account);
                        
                        if (user == null)
                        {
                            filterContext.Result = new RedirectResult(Url.Action("Login", "Account"));
                        }
                        else // set session and redirect
                        {
                            //save session
                            Session[Common.Common.user] = user;
                            //filterContext.Result = new RedirectResult(Request.FilePath);
                            //set direct for user or admin

                            //filterContext.Result = new RedirectResult(Url.Action("Index", "Home"));
                        }
                    }else
                    {
                        filterContext.Result = new RedirectResult(Url.Action("Login", "Account"));
                    }


                }else
                {
                    filterContext.Result = new RedirectResult(Url.Action("Login", "Account"));
                }
            }

            
        }
    }
}