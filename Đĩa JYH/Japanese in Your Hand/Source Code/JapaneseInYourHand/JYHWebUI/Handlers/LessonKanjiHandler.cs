﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.Entities;
using JYHDataAccess.DbService;
using JYHWebUI.Models.DTOs;
using JYHWebUI.Models.Dic;

namespace JYHWebUI.Handlers
{
    public class LessonKanjiHandler
    {
        JYHDbContext _jyhDbContext;
        public LessonKanjiHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public LessonKanjiHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        private bool CheckExist(LessonKanjiDto lessonKanjiDto)
        {
            try
            {
                LessonKanji lessonKanji = _jyhDbContext.LessonKanjis.FirstOrDefault(lw =>
                lw.LessonID == lessonKanjiDto.LessonID && lw.KanjiID == lessonKanjiDto.KanjiID);
                if (lessonKanji == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Add(LessonKanjiDto lessonKanjiDto)
        {
            if (CheckExist(lessonKanjiDto))
            {
                return false;
            }

            LessonKanji lesson = new LessonKanji
            {
                LessonID = lessonKanjiDto.LessonID,
                KanjiID = lessonKanjiDto.KanjiID,
                // Available = true

            };
            try
            {
                _jyhDbContext.LessonKanjis.Add(lesson);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(LessonKanjiDto lessonKanjiDto)
        {
            if (!CheckExist(lessonKanjiDto))
            {
                return false;
            }
            try
            {
                LessonKanji lesson = _jyhDbContext.LessonKanjis.FirstOrDefault(lw => lw.KanjiID == lessonKanjiDto.KanjiID && lw.LessonID == lessonKanjiDto.LessonID);
                _jyhDbContext.LessonKanjis.Remove(lesson);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<KanjiDto> Get(int lessonID)
        {
            try
            {
                var list = _jyhDbContext.LessonKanjis.Where(lk => lk.LessonID == lessonID).Join(_jyhDbContext.Kanjis,
                                                            lk => lk.KanjiID,
                                                            k => k.ID,
                                                            (lk, k) => new KanjiDto
                                                            {
                                                                ID = k.ID,
                                                                Kanji = k.Kanji1,
                                                                Sino = k.Sino,
                                                                Meaning = k.Meaning,
                                                                Onyomi = k.Onyomi,
                                                                Kunyomi = k.Kunyomi,
                                                                Notes = k.Notes,
                                                                Grade = k.Grade,
                                                                Level = k.Level,
                                                                Mnemonic = k.Mnemonic,
                                                                Radical = k.Radical,
                                                                StrokeCount = k.StrokeCount,
                                                                WKunyomi1 = k.WKunyomi1,
                                                                WKunyomi2 = k.WKunyomi2,
                                                                WOnyomi1 = k.WOnyomi1,
                                                                WOnyomi2 = k.WOnyomi2
                                                            }).ToList();

                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}
