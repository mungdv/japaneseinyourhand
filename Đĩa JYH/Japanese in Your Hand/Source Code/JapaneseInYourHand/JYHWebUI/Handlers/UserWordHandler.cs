﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JYHWebUI.Handlers
{
    using JYHDataAccess.DbService;
    using JYHDataAccess.Entities;
    using JYHWebUI.Models.DTOs;
    using JYHDataAccess.Utilities;
    using System.Data.Entity;
    using JYHWebUI.Models.ViewRequest;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;
    public class UserWordHandler
    {
        private JYHDbContext _jyhDbContext;
        public UserWordHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserWordHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public bool checkExist(UserWordDto userWordDto)
        {
            try
            {
                var userWord = _jyhDbContext.UserWords.Where(uk => uk.WordID == userWordDto.WordID && uk.UserID == userWordDto.UserID).FirstOrDefault();
                if (userWord == null)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(UserWordDto userWordDto)
        {
            if (checkExist(userWordDto))
            {
                return false;
            }
            else
            {
                UserWord userWord = new UserWord
                {
                    WordID = userWordDto.WordID,
                    UserID = userWordDto.UserID,
                    LearnedDate = DateTime.Now
                };
                try
                {
                    _jyhDbContext.UserWords.Add(userWord);
                    _jyhDbContext.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }


        public List<UserWordDto> Get(int userID, int lessonID)
        {
            try
            {
                var list = _jyhDbContext.LessonWords.Where(lk => lk.LessonID == lessonID).Join(_jyhDbContext.UserWords.Where(uk => uk.UserID == userID),
                                                            lk => lk.WordID,
                                                            uk => uk.WordID,
                                                            (lk, uk) => new UserWordDto
                                                            {
                                                                UserID = uk.UserID,
                                                                WordID = uk.WordID,
                                                                LearnedDate = uk.LearnedDate
                                                            }).ToList();

                return list;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<UserWordDto> Get(int userID)
        {
            try
            {
                var list = _jyhDbContext.UserWords.Where(uk => uk.UserID == userID).Select(uk => new UserWordDto
                                                            {
                                                                UserID = uk.UserID,
                                                                WordID = uk.WordID,
                                                                LearnedDate = uk.LearnedDate
                                                            }).ToList();

                return list;
            }
            catch
            {
                return null;
            }
        }

    }
}