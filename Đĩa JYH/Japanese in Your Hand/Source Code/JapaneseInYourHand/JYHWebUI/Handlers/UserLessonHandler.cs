﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.DbService;
using JYHWebUI.Models.DTOs;
using JYHDataAccess.Entities;
using System.Data.Entity;

namespace JYHWebUI.Handlers
{
    public class UserLessonHandler
    {
        private JYHDbContext _jyhDbContext;

        public UserLessonHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserLessonHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public bool CheckExist(int userID, int lessonID)
        {
            try
            {
                var userLesson = _jyhDbContext.UserLessons.Where(ul => ul.LessonID == lessonID && ul.UserID == userID).FirstOrDefault();
                if(userLesson == null)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public UserLessonDto GetUserLesson(int userID, int lessonID)
        {
            try
            {
                return _jyhDbContext.UserLessons.Where(ul => ul.LessonID == lessonID && ul.UserID == userID).Select(ul => new UserLessonDto
                {
                    UserID = ul.UserID,
                    CompleteDate = ul.CompleteDate,
                    LastLearnedDate = ul.LastLearnedDate,
                    LessonID = ul.LessonID,
                    Progress = ul.Progress,
                    Score = ul.Progress,
                    StartDate = ul.StartDate
                }).FirstOrDefault();
            }catch
            {
                return null;
            }
        }

        public List<UserLessonDto> GetUserLessonLearned(int userID)
        {
            try
            {
                return _jyhDbContext.UserLessons.Where(ul => ul.UserID == userID && ul.Progress >= 80 && ul.Score >= 50).Select(ul => new UserLessonDto
                {
                    UserID = ul.UserID,
                    CompleteDate = ul.CompleteDate,
                    LastLearnedDate = ul.LastLearnedDate,
                    LessonID = ul.LessonID,
                    Progress = ul.Progress,
                    Score = ul.Progress,
                    StartDate = ul.StartDate
                }).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<UserLessonDto> GetUserLessonByCourse(int courseID)
        {
            try
            {
                var list = _jyhDbContext.Lessons.Where(l => l.CourseID == courseID && l.Available == true)
                    .Join(_jyhDbContext.UserLessons,
                                                            l => l.ID,
                                                            ul => ul.LessonID,
                                                            (l, ul) => new UserLessonDto
                                                            {
                                                                UserID = ul.UserID,
                                                                LessonID = ul.LessonID,
                                                                Progress =ul.Progress,
                                                                Score = ul.Score,
                                                                CompleteDate = ul.CompleteDate,
                                                                LastLearnedDate = ul.LastLearnedDate,
                                                                StartDate = ul.StartDate
                                                            }).ToList();

                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool Add(UserLessonDto userLessonDto)
        {
            if (CheckExist(userLessonDto.UserID, userLessonDto.LessonID))
            {
                return false;
            }
            else
            {
                UserLesson userLesson = new UserLesson
                {
                    LessonID = userLessonDto.LessonID,
                    LastLearnedDate = DateTime.Now,
                    Progress = userLessonDto.Progress,
                    StartDate = DateTime.Now,
                    Score = userLessonDto.Score,
                    UserID = userLessonDto.UserID,
                    
                };
                try
                {
                    _jyhDbContext.UserLessons.Add(userLesson);
                    _jyhDbContext.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool Update(UserLessonDto userLessonDto)
        {
            if (!CheckExist(userLessonDto.UserID, userLessonDto.LessonID))
            {
                return false;
            }
            try
            {
                UserLesson userLesson = _jyhDbContext.UserLessons
                .FirstOrDefault(ul => ul.LessonID == userLessonDto.LessonID && ul.UserID == userLessonDto.UserID);
                userLesson.Score = userLessonDto.Score;
                userLesson.LastLearnedDate = DateTime.Now;
                _jyhDbContext.Entry(userLesson).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void CaculatorProgress (int userID, int lessonID)
        {
            try
            {
                UserLesson lesson = _jyhDbContext.UserLessons.Where(ul => ul.LessonID == lessonID && ul.UserID == userID).FirstOrDefault();
                if(lesson != null)
                {
                    int total = new LessonKanjiHandler().Get(lesson.LessonID).Count
                                + new LessonWordHandler().Get(lesson.LessonID).Count
                                + new LessonGrammarHandler().Get(lesson.LessonID).Count;

                    int learned = new UserKanjiHandler().Get(userID, lessonID).Count
                                + new UserWordHandler().Get(userID, lessonID).Count
                                + new UserGrammarHandler().Get(userID, lessonID).Count;

                    lesson.Progress = (learned * 100) / total;
                    lesson.LastLearnedDate = DateTime.Now;
                    if (lesson.Progress >= 80 && lesson.Score >= 50)
                    {
                        UserCourseHandler userCourseHandler = new UserCourseHandler();
                        try
                        {
                            CourseDto course = new CourseHandler().GetCourseByLessonID(lesson.LessonID);
                            UserCourseDto userCourseDto = new UserCourseDto();
                            userCourseDto.Progress = (GetUserLessonByCourse(course.ID).Count * 100)/ new LessonHandler().GetD(course.ID).Count;
                            userCourseDto.UserID = lesson.UserID;
                            userCourseDto.CourseID = course.ID;
                            if(userCourseDto.Progress == 100)
                            {
                                userCourseDto.CompleteDate = DateTime.Now;
        
                            }
                            userCourseHandler.Update(userCourseDto);

                        }
                        catch
                        {

                        }

                        lesson.CompleteDate = DateTime.Now;
                    }
                    _jyhDbContext.Entry(lesson).State = EntityState.Modified;
                    _jyhDbContext.SaveChanges();

                    
                }
            }catch
            {

            }
           
        }

    }
}