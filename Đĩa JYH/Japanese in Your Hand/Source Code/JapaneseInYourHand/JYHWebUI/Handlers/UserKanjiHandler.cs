﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JYHWebUI.Handlers
{
    using JYHDataAccess.DbService;
    using JYHDataAccess.Entities;
    using JYHWebUI.Models.DTOs;
    using JYHDataAccess.Utilities;
    using System.Data.Entity;
    using JYHWebUI.Models.ViewRequest;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    public class UserKanjiHandler
    {
        private JYHDbContext _jyhDbContext;
        public UserKanjiHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserKanjiHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public bool CheckExist(UserKanjiDto userKanjiDto)
        {
            try
            {
                var userKanji = _jyhDbContext.UserKanjis.Where(uk => uk.KanjiID == userKanjiDto.KanjiID && uk.UserID == userKanjiDto.UserID).FirstOrDefault();
                if(userKanji == null)
                {
                    return false;
                }
                return true;
            }catch
            {
                return false;
            }
        }


        public bool Add(UserKanjiDto userKanjiDto)
        {
            if (CheckExist(userKanjiDto))
            {
                return false;
            } else
            {
                UserKanji userKanji = new UserKanji
                {
                    KanjiID = userKanjiDto.KanjiID,
                    UserID = userKanjiDto.UserID,
                    LearnedDate = DateTime.Now
                };
                try
                {
                    _jyhDbContext.UserKanjis.Add(userKanji);
                    _jyhDbContext.SaveChanges();
                    return true;
                }catch
                {
                    return false;
                }
            }
        }

        public List<UserKanjiDto> Get(int userID, int lessonID)
        {
            try
            {
                var list = _jyhDbContext.LessonKanjis.Where(lk => lk.LessonID == lessonID).Join(_jyhDbContext.UserKanjis.Where(uk => uk.UserID == userID),
                                                            lk => lk.KanjiID,
                                                            uk => uk.KanjiID,
                                                            (lk, uk) => new UserKanjiDto
                                                            {
                                                                UserID = uk.UserID,
                                                                KanjiID = uk.KanjiID,
                                                                LearnedDate = uk.LearnedDate
                                                            }).ToList();

                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<UserKanjiDto> Get(int userID)
        {
            try
            {
                var list = _jyhDbContext.UserKanjis.Where(uk => uk.UserID == userID).Select(uk => new UserKanjiDto
                                                            {
                                                                UserID = uk.UserID,
                                                                KanjiID = uk.KanjiID,
                                                                LearnedDate = uk.LearnedDate
                                                            }).ToList();

                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}