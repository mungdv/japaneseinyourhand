﻿using System;
using JYHDataAccess.DbService;
using JYHDataAccess.Entities;
using JYHWebUI.Models.DTOs;
using JYHDataAccess.Utilities;
using System.Data.Entity;
using System.Data;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace JYHWebUI.Handlers
{
    public class UserRoleHandler
    {
        private JYHDbContext _jyhDbContext;
        public UserRoleHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserRoleHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public string UpdateUserRole(List<int> roles, UserDto user)
        {
            string text = "";
            var listCurrentRole = Get(user.ID);
            if (roles != null)
            {
                foreach (int role in roles)
                {
                    bool exits = false;
                    foreach (int crole in listCurrentRole)
                    {
                        if (crole == role)
                        {
                            exits = true;
                            break;
                        }
                    }
                    if (!exits)
                    {
                        if(Add(user.ID, role))
                        {
                            if(role == 1)
                            {
                                text += " Bạn Đã Thêm Quyền Quản Lí từ Điển Cho Người Dùng '" + user.Email + "' \n\r";
                            }
                            else if(role == 2)
                            {
                                text += " Bạn Đã Thêm Quyền Quản Lí Bài Học Cho Người Dùng '" + user.Email + "'\n\r";
                            }
                            else
                            {
                                text += " Bạn Đã Thêm Quyền Quản Lí Người Dùng Cho Người Dùng '" + user.Email + "'\n\r";
                            }
                             
                        }
                    }
                }

                foreach (int crole in listCurrentRole)
                {
                    bool exits = false;
                    foreach (int role in roles)
                    {
                        if (crole == role)
                        {
                            exits = true;
                            break;
                        }
                    }
                    if (!exits)
                    {
                        if(Delete(user.ID, crole))
                        {
                            if (crole == 1)
                            {
                                text += " Bạn Đã Hủy Quyền Quản Lí từ Điển Của Người Dùng '" + user.Email + "' \n\r";
                            }
                            else if (crole == 2)
                            {
                                text += " Bạn Đã Hủy Quyền Quản Lí Bài Học Của Người Dùng '" + user.Email + "'\n\r";
                            }
                            else
                            {
                                text += " Bạn Đã Hủy Quyền Quản Lí Người Dùng Của Người Dùng '" + user.Email + "'\n\r";
                            }
                        }
                    }
                }
            }else
            {
                if (listCurrentRole != null){
                    foreach (int crole in listCurrentRole)
                    {
                        if(Delete(user.ID, crole))
                        {
                            if (crole == 1)
                            {
                                text += " Bạn Đã Hủy Quyền Quản Lí từ Điển Của Người Dùng '" + user.Email + "' \n\r";
                            }
                            else if (crole == 2)
                            {
                                text += " Bạn Đã Hủy Quyền Quản Lí Bài Học Của Người Dùng '" + user.Email + "'\n\r";
                            }
                            else
                            {
                                text += " Bạn Đã Hủy Quyền Quản Lí Người Dùng Của Người Dùng '" + user.Email + "'\n\r";
                            }
                        }
                    }
                }
                
            }
            return text;
            
        }

        public bool Add(int userID, int roleID)
        {
            try
            {
                var user = new UserRole
                {
                    RoleID = roleID,
                    UserID = userID
                };
                _jyhDbContext.UserRoles.Add(user);
                _jyhDbContext.SaveChanges();
                return true;
            }catch(Exception e)
            {
                return false;
            }
        }

        public bool Delete(int userID, int roleID)
        {
            try
            {
                var user = _jyhDbContext.UserRoles.FirstOrDefault(u => u.UserID == userID && u.RoleID == roleID);
                _jyhDbContext.UserRoles.Remove(user);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool Check(int userID, int roleID)
        {
            try
            {
                var userRole = _jyhDbContext.UserRoles.Where(uc => uc.UserID == userID && uc.RoleID == roleID).Select(ul => new UserRoleDto
                {
                    RoleID = ul.RoleID,
                    UserID = ul.UserID
                }).ToList();
                if(userRole == null)
                {
                    return false;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<int> Get(int userID)
        {
            try
            {
                var list = _jyhDbContext.UserRoles.Where(uc => uc.UserID == userID).Select(ul => ul.RoleID).ToList();

                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}




        
