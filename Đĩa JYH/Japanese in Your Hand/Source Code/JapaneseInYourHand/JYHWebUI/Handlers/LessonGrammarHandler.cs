﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.Entities;
using JYHDataAccess.DbService;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Handlers
{
    public class LessonGrammarHandler
    {
        JYHDbContext _jyhDbContext;
        public LessonGrammarHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public LessonGrammarHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        private bool CheckExist(LessonGrammarDto lessonGrammarDto)
        {
            try
            {
                LessonGrammar lessonGrammar = _jyhDbContext.LessonGrammars.FirstOrDefault(lw =>
                    lw.LessonID == lessonGrammarDto.LessonID && lw.GrammarID == lessonGrammarDto.GrammarID);
                if (lessonGrammar == null)
                {
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Add(LessonGrammarDto lessonGrammarDto)
        {
            if (CheckExist(lessonGrammarDto))
            {
                return false;
            }

            LessonGrammar lesson = new LessonGrammar
            {
                LessonID = lessonGrammarDto.LessonID,
                GrammarID = lessonGrammarDto.GrammarID,
                // Available = true

            };
            try
            {
                _jyhDbContext.LessonGrammars.Add(lesson);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(LessonGrammarDto lessonGrammarDto)
        {
            if (!CheckExist(lessonGrammarDto))
            {
                return false;
            }

            try
            {
                LessonGrammar lesson = _jyhDbContext.LessonGrammars.FirstOrDefault(
                lg => lg.LessonID == lessonGrammarDto.LessonID && lg.GrammarID == lessonGrammarDto.GrammarID);
                _jyhDbContext.LessonGrammars.Remove(lesson);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<GrammarDto> Get(int lessonID)
        {
            try
            {
                var list = _jyhDbContext.LessonGrammars.Where(lg => lg.LessonID == lessonID).Join(_jyhDbContext.Grammars,
                                                           lg => lg.GrammarID,
                                                           grammar => grammar.ID,
                                                           (lg, grammar) => new GrammarDto
                                                           {
                                                               ID = grammar.ID,
                                                               Meaning = grammar.Meaning,
                                                               Usage = grammar.Usage,
                                                               Level = grammar.Level,
                                                               Example = grammar.Example,
                                                               Notes = grammar.Notes,
                                                               StructureForms = grammar.StructureForms,
                                                               StructureKana = grammar.StructureKana,
                                                               StructureKanji = grammar.StructureKanji
                                                           }).ToList();
                
                return list;
            }
            catch
            {
                return null;
            }
        }

    }
}
