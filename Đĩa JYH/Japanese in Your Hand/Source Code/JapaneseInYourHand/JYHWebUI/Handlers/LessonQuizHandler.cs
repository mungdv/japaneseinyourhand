﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.DbService;
using JYHWebUI.Models.DTOs;
using JYHDataAccess.Entities;
using System.Data.Entity;
namespace JYHWebUI.Handlers
{
    public class LessonQuizHandler
    {
        JYHDbContext _jyhDbContext;

        public LessonQuizHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public LessonQuizHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public bool Add(LessonQuizDto lessonQuizDto)
        {
            LessonQuiz lessonQuiz = new LessonQuiz {
                LessonID = lessonQuizDto.LessonID,
                Question = lessonQuizDto.Question,
                CorrectAnswer = lessonQuizDto.CorrectAnswer,
                WrongAnswer1 = lessonQuizDto.WrongAnswer1,
                WrongAnswer2 = lessonQuizDto.WrongAnswer2,
                WrongAnswer3 = lessonQuizDto.WrongAnswer3,
                Explanation = lessonQuizDto.Explanation
        };
            try
            {
                _jyhDbContext.LessonQuizs.Add(lessonQuiz);
                _jyhDbContext.SaveChanges();
                return true;
            } catch(Exception)
            {
                return false;
            }

        }

        public bool Update(LessonQuizDto lessonQuizDto)
        {
            LessonQuiz lessonQuiz = null;
            try
            {
                _jyhDbContext.LessonQuizs.FirstOrDefault(lq => lq.ID == lessonQuizDto.ID);
            }
            catch (Exception) { }
            if(lessonQuiz == null)
            {
                return false;
            }

            lessonQuiz.LessonID = lessonQuizDto.LessonID;
            lessonQuiz.Question = lessonQuizDto.Question;
            lessonQuiz.CorrectAnswer = lessonQuizDto.CorrectAnswer;
            lessonQuiz.WrongAnswer1 = lessonQuizDto.WrongAnswer1;
            lessonQuiz.WrongAnswer2 = lessonQuizDto.WrongAnswer2;
            lessonQuiz.WrongAnswer3 = lessonQuizDto.WrongAnswer3;
            lessonQuiz.Explanation = lessonQuizDto.Explanation;
            try
            {
                _jyhDbContext.Entry(lessonQuiz).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool Delete(int id)
        {
            try
            {
                LessonQuiz lessonQuiz = _jyhDbContext.LessonQuizs.FirstOrDefault(lq => lq.ID == id);
                _jyhDbContext.LessonQuizs.Remove(lessonQuiz);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch (Exception e) {
                return false;
            }

        }
        public List<LessonQuizDto> Get(int lessonID)
        {
            try
            {
                return _jyhDbContext.LessonQuizs.Where(lq => lq.LessonID == lessonID).Select(lq => new LessonQuizDto
                {
                    ID = lq.ID,
                    LessonID = lq.LessonID,
                    CorrectAnswer = lq.CorrectAnswer,
                    Question = lq.Question,
                    WrongAnswer1 = lq.WrongAnswer1,
                    WrongAnswer2 = lq.WrongAnswer2,
                    WrongAnswer3 = lq.WrongAnswer3,
                    Explanation = lq.Explanation,
                    Type = lq.Type
                    
                }).ToList();
            }catch
            {
                return null;
            }
        }
    }
}