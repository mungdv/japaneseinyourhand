﻿using System;


namespace JYHWebUI.Handlers
{
    using JYHDataAccess.DbService;
    using JYHDataAccess.Entities;
    using JYHWebUI.Models.DTOs;
    using System.Data.Entity;
    using System.Linq;
    using System.Collections.Generic;

    using Excel = Microsoft.Office.Interop.Excel;
    using System.Data;
    using System.Runtime.InteropServices;
    using JYHWebUI.Models.Dic;
    using System.Web;
    using OfficeOpenXml;
    using System.Web.Mvc;
    using JYHDataAccess.Utilities;

    public class GrammarHandler
    {
        private JYHDbContext _jyhDbContext;

        public GrammarHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public GrammarHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public GrammarDto ConvertGrammarToDto(Grammar grammar)
        {
            GrammarDto grammarDto = new GrammarDto();
            grammarDto.ID = grammar.ID;
            grammarDto.StructureKanji = grammar.StructureKanji;
            grammarDto.StructureKana = grammar.StructureKana;
            grammarDto.StructureForms = grammar.StructureForms;
            grammarDto.Meaning = grammar.Meaning;
            grammarDto.Usage = grammar.Usage;
            grammarDto.Level = grammar.Level;
            grammarDto.Example = grammar.Example;
            grammarDto.Notes = grammar.Notes;

            return grammarDto;
        }
   
        public bool CheckExist(int id)
        {
            if(_jyhDbContext.Grammars.FirstOrDefault(g => g.ID == id) != null)
            {
                return true;
            }
            return false;
        }

        #region insert, update, delete, insertOrUpdate, Get Grammar
        public List<GrammarDto> DicSearch(string text)
        {
            return _jyhDbContext.Grammars.ToList().Where(g => g.StructureForms.Contains(text) 
            || Utility.RemoveDiacritics(g.Meaning).Contains(Utility.RemoveDiacritics(text))).Select(grammar => new GrammarDto
            {
                ID = grammar.ID,
                Meaning = grammar.Meaning,
                Usage = grammar.Usage,
                Level = grammar.Level,
                Example = grammar.Example,
                Notes = grammar.Notes,
                StructureForms = grammar.StructureForms,
                StructureKana = grammar.StructureKana,
                StructureKanji = grammar.StructureKanji
            }).Take(5).ToList();
        }

        public GrammarDto DicSearch(int id)
        {
            return _jyhDbContext.Grammars.ToList().Where(g => g.ID == id).Select(grammar => new GrammarDto
            {
                ID = grammar.ID,
                Meaning = grammar.Meaning,
                Usage = grammar.Usage,
                Level = grammar.Level,
                Example = grammar.Example,
                Notes = grammar.Notes,
                StructureForms = grammar.StructureForms,
                StructureKana = grammar.StructureKana,
                StructureKanji = grammar.StructureKanji
            }).FirstOrDefault();
        }

        public bool Insert(GrammarDto grammarDto)
        {
            if(!CheckExist(grammarDto.ID))
            {
                Grammar grammar = new Grammar
                {
                    ID = grammarDto.ID,
                    StructureKanji = grammarDto.StructureKanji,
                    StructureKana = grammarDto.StructureKana,
                    StructureForms = grammarDto.StructureForms,
                    Meaning = grammarDto.Meaning,
                    Usage = grammarDto.Usage,
                    Level = grammarDto.Level,
                    Example = grammarDto.Example,
                    Notes = grammarDto.Notes,
                    Available = true

                };

                _jyhDbContext.Grammars.Add(grammar);
                _jyhDbContext.SaveChanges();
                return true;
            }
            return false;
               
        }

        public bool Update(GrammarDto grammarDto)
        {
            if(CheckExist(grammarDto.ID)) {
                Grammar grammar = _jyhDbContext.Grammars.FirstOrDefault(g => g.ID == grammarDto.ID);
                grammar.StructureKanji = grammarDto.StructureKanji;
                grammar.StructureKana = grammarDto.StructureKana;
                grammar.StructureForms = grammarDto.StructureForms;
                grammar.Meaning = grammarDto.Meaning;
                grammar.Usage = grammarDto.Usage;
                grammar.Level = grammarDto.Level;
                grammar.Example = grammarDto.Example;
                grammar.Notes = grammarDto.Notes;
                grammar.Available = true;

                _jyhDbContext.Entry(grammar).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();

                return true;
            }
            return false;
            
        }

        public bool Delete(int id)
        {
            Grammar grammar = _jyhDbContext.Grammars.FirstOrDefault(g => g.ID == id);
            if (grammar == null)
            {
                return false;
            }
            grammar.Available = false;
            _jyhDbContext.Entry(grammar).State = EntityState.Modified;
            _jyhDbContext.SaveChanges();
            return true;
        }

        public bool InsertOrUpdate(GrammarDto grammarDto)
        {
            if(CheckExist(grammarDto.ID))
            {
                Update(grammarDto);
                return false;
            } else
            {
                Insert(grammarDto);
                return true;
            }
        }

        #endregion

        #region import export
        public string Import(HttpPostedFileBase fileUpload)
        {
            try
            {
                string fileName = fileUpload.FileName;
                string fileContentType = fileUpload.ContentType;
                byte[] fileBytes = new byte[fileUpload.ContentLength];

                var data = fileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(fileUpload.ContentLength));
                List<Grammar> listInsert = new List<Grammar>();
                List<Grammar> listUpdate = new List<Grammar>();
                using (var package = new ExcelPackage(fileUpload.InputStream))
                {
                    var currentSheet = package.Workbook.Worksheets;
                    var workSheet = currentSheet.First();
                    var noOfCol = workSheet.Dimension.End.Column;
                    var noOfRow = workSheet.Dimension.End.Row;
                    for (int rowIterator = 2; rowIterator < noOfRow; rowIterator++)
                    {
                        if (workSheet.Cells[rowIterator, 1].Value == null)
                        {
                            workSheet.Cells[rowIterator, 1].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 2].Value == null)
                        {
                            workSheet.Cells[rowIterator, 2].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 3].Value == null)
                        {
                            workSheet.Cells[rowIterator, 4].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 4].Value == null)
                        {
                            workSheet.Cells[rowIterator, 4].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 5].Value == null)
                        {
                            workSheet.Cells[rowIterator, 5].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 6].Value == null)
                        {
                            workSheet.Cells[rowIterator, 6].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 7].Value == null)
                        {
                            workSheet.Cells[rowIterator, 7].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 8].Value == null)
                        {
                            workSheet.Cells[rowIterator, 8].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 9].Value == null)
                        {
                            workSheet.Cells[rowIterator, 9].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 9].Value == null)
                        {
                            workSheet.Cells[rowIterator, 10].Value = "0";
                        }
                        var grammar = new Grammar();
                        grammar.ID = int.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                        grammar.StructureKanji = workSheet.Cells[rowIterator, 2].Value.ToString();
                        grammar.StructureKana = workSheet.Cells[rowIterator, 3].Value.ToString();
                        grammar.StructureForms = workSheet.Cells[rowIterator, 4].Value.ToString();
                        grammar.Meaning = workSheet.Cells[rowIterator, 5].Value.ToString();
                        grammar.Usage = workSheet.Cells[rowIterator, 6].Value.ToString();
                        grammar.Level = byte.Parse(workSheet.Cells[rowIterator, 7].Value.ToString());
                        grammar.Example = workSheet.Cells[rowIterator, 8].Value.ToString();
                        grammar.Notes = workSheet.Cells[rowIterator, 9].Value.ToString();
                        grammar.Available = workSheet.Cells[rowIterator, 10].Value.ToString() == "1" ? true : false;

                        if (CheckExist(grammar.ID))
                        {
                            listUpdate.Add(grammar);
                        }
                        else
                        {
                            grammar.Available = true;
                            listInsert.Add(grammar);
                        }
                    }
                    using (JYHDbContext db = new JYHDbContext())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.BulkInsert(listInsert);
                        db.BulkUpdate(listUpdate);
                        db.SaveChanges();

                    }
                }

                return "Nhập dữ liệu ngữ pháp thành công. Thêm mới: " + listInsert.Count + ". Cập nhật: " + listUpdate.Count + ". Nhấn phím Back trên trình duyệt để quay trở lại.";
            }
            catch (Exception ex)
            {
                return "Nhập dữ liệu ngữ pháp thất bại. Hãy kiểm tra lại định dạng file Excel.";
            }
        }

        public FileContentResult Export()
        {

            var fileDownloadName = String.Format("ExportGrammar.xlsx");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


            // Pass your ef data to method
            ExcelPackage package = GenerateExcelFile(_jyhDbContext.Grammars.ToList());

            var fsr = new FileContentResult(package.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;
        }

        private static ExcelPackage GenerateExcelFile(IEnumerable<Grammar> datasource)
        {

            ExcelPackage pck = new ExcelPackage();

            //Create the worksheet 
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Grammar");
            // Sets Headers
            ws.Cells[1, 1].Value = "ID";
            ws.Cells[1, 2].Value = "StructureKanji";
            ws.Cells[1, 3].Value = "StructureKana";
            ws.Cells[1, 4].Value = "StructureForms";
            ws.Cells[1, 5].Value = "Meaning";
            ws.Cells[1, 6].Value = "Usage";
            ws.Cells[1, 7].Value = "Level";
            ws.Cells[1, 8].Value = "Example";
            ws.Cells[1, 9].Value = "Notes";
            ws.Cells[1, 10].Value = "Available";

            // Inserts Data
            for (int i = 0; i < datasource.Count(); i++)
            {
                ws.Cells[i + 2, 1].Value = datasource.ElementAt(i).ID;
                ws.Cells[i + 2, 2].Value = datasource.ElementAt(i).StructureKanji;
                ws.Cells[i + 2, 3].Value = datasource.ElementAt(i).StructureKana;
                ws.Cells[i + 2, 4].Value = datasource.ElementAt(i).StructureForms;
                ws.Cells[i + 2, 5].Value = datasource.ElementAt(i).Meaning;
                ws.Cells[i + 2, 6].Value = datasource.ElementAt(i).Usage;
                ws.Cells[i + 2, 7].Value = datasource.ElementAt(i).Level;
                ws.Cells[i + 2, 8].Value = datasource.ElementAt(i).Example;
                ws.Cells[i + 2, 9].Value = datasource.ElementAt(i).Notes;
                ws.Cells[i + 2, 10].Value = datasource.ElementAt(i).Available ? "1" : "0";
            }

            // Format Header of Table
            using (ExcelRange rng = ws.Cells["A1:Z1"])
            {

                rng.Style.Font.Bold = true;
            }
            return pck;
        }
        #endregion
    }
}