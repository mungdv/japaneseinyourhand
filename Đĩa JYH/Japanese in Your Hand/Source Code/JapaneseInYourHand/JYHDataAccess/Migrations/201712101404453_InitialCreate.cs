namespace JYHDataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Image = c.String(maxLength: 500),
                        Level = c.Byte(),
                        Description = c.String(maxLength: 500),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Lessons",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        CourseID = c.Int(nullable: false),
                        Description = c.String(maxLength: 2000),
                        Image = c.String(maxLength: 500),
                        Dialogue = c.String(),
                        DialogueAudio = c.String(),
                        CultureNotes = c.String(),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Courses", t => t.CourseID, cascadeDelete: true)
                .Index(t => t.CourseID);
            
            CreateTable(
                "dbo.LessonGrammars",
                c => new
                    {
                        LessonID = c.Int(nullable: false),
                        GrammarID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LessonID, t.GrammarID })
                .ForeignKey("dbo.Grammars", t => t.GrammarID, cascadeDelete: true)
                .ForeignKey("dbo.Lessons", t => t.LessonID, cascadeDelete: true)
                .Index(t => t.LessonID)
                .Index(t => t.GrammarID);
            
            CreateTable(
                "dbo.Grammars",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StructureKanji = c.String(maxLength: 100),
                        StructureKana = c.String(maxLength: 100),
                        StructureForms = c.String(maxLength: 500),
                        Meaning = c.String(maxLength: 500),
                        Usage = c.String(maxLength: 500),
                        Level = c.Byte(),
                        Example = c.String(),
                        Notes = c.String(),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserGrammars",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        GrammarID = c.Int(nullable: false),
                        LearnedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => new { t.UserID, t.GrammarID })
                .ForeignKey("dbo.Grammars", t => t.GrammarID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.GrammarID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 255),
                        Password = c.String(maxLength: 255),
                        FullName = c.String(maxLength: 100),
                        Phone = c.String(maxLength: 15),
                        Address = c.String(maxLength: 2000),
                        Level = c.Byte(),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastLogin = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IdentifierID = c.String(),
                        Token = c.String(),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserCourses",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        CourseID = c.Int(nullable: false),
                        Progress = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CompleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastLearnedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => new { t.UserID, t.CourseID })
                .ForeignKey("dbo.Courses", t => t.CourseID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.CourseID);
            
            CreateTable(
                "dbo.UserKanjis",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        KanjiID = c.Int(nullable: false),
                        LearnedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => new { t.UserID, t.KanjiID })
                .ForeignKey("dbo.Kanjis", t => t.KanjiID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.KanjiID);
            
            CreateTable(
                "dbo.Kanjis",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Kanji1 = c.String(maxLength: 1),
                        Radical = c.String(maxLength: 1),
                        Onyomi = c.String(maxLength: 500),
                        WOnyomi1 = c.String(maxLength: 500),
                        WOnyomi2 = c.String(maxLength: 500),
                        Kunyomi = c.String(maxLength: 500),
                        WKunyomi1 = c.String(maxLength: 500),
                        WKunyomi2 = c.String(maxLength: 500),
                        Sino = c.String(maxLength: 50),
                        Meaning = c.String(maxLength: 2000),
                        StrokeCount = c.Int(nullable: false),
                        Mnemonic = c.String(maxLength: 2000),
                        Grade = c.Int(nullable: false),
                        Level = c.Byte(),
                        Notes = c.String(),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LessonKanjis",
                c => new
                    {
                        LessonID = c.Int(nullable: false),
                        KanjiID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LessonID, t.KanjiID })
                .ForeignKey("dbo.Kanjis", t => t.KanjiID, cascadeDelete: true)
                .ForeignKey("dbo.Lessons", t => t.LessonID, cascadeDelete: true)
                .Index(t => t.LessonID)
                .Index(t => t.KanjiID);
            
            CreateTable(
                "dbo.UserLessons",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        LessonID = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        Progress = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CompleteDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastLearnedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserID, t.LessonID })
                .ForeignKey("dbo.Lessons", t => t.LessonID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.UserID)
                .Index(t => t.LessonID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserID, t.RoleID })
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserStats",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        CorrectKanji = c.Int(nullable: false),
                        WrongKanji = c.Int(nullable: false),
                        CorrectWord = c.Int(nullable: false),
                        WrongWord = c.Int(nullable: false),
                        CorrectGrammar = c.Int(nullable: false),
                        WrongGrammar = c.Int(nullable: false),
                        CorrectReading = c.Int(nullable: false),
                        WrongReading = c.Int(nullable: false),
                        CorrectListening = c.Int(nullable: false),
                        WrongListening = c.Int(nullable: false),
                        TotalTime = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.UserWords",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        WordID = c.Int(nullable: false),
                        LearnedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => new { t.UserID, t.WordID })
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Words", t => t.WordID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.WordID);
            
            CreateTable(
                "dbo.Words",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Word1 = c.String(maxLength: 100),
                        Reading = c.String(maxLength: 500),
                        WReading1 = c.String(maxLength: 500),
                        WReading2 = c.String(maxLength: 500),
                        Meaning = c.String(maxLength: 2000),
                        WMeaning1 = c.String(maxLength: 2000),
                        WMeaning2 = c.String(maxLength: 2000),
                        Type = c.String(maxLength: 100),
                        Common = c.Boolean(nullable: false),
                        UsuallyKana = c.Boolean(nullable: false),
                        SpecialReading = c.Boolean(nullable: false),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LessonWords",
                c => new
                    {
                        LessonID = c.Int(nullable: false),
                        WordID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LessonID, t.WordID })
                .ForeignKey("dbo.Lessons", t => t.LessonID, cascadeDelete: true)
                .ForeignKey("dbo.Words", t => t.WordID, cascadeDelete: true)
                .Index(t => t.LessonID)
                .Index(t => t.WordID);
            
            CreateTable(
                "dbo.LessonQuizs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LessonID = c.Int(nullable: false),
                        Question = c.String(),
                        CorrectAnswer = c.String(maxLength: 200),
                        WrongAnswer1 = c.String(maxLength: 200),
                        WrongAnswer2 = c.String(maxLength: 200),
                        WrongAnswer3 = c.String(maxLength: 200),
                        Explanation = c.String(),
                        Type = c.Byte(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Lessons", t => t.LessonID, cascadeDelete: true)
                .Index(t => t.LessonID);
            
            CreateTable(
                "dbo.JLPTQuestions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Level = c.Byte(),
                        QuestionType = c.Int(nullable: false),
                        Question = c.String(),
                        CorrectAnswer = c.String(maxLength: 200),
                        WrongAnswer1 = c.String(maxLength: 200),
                        WrongAnswer2 = c.String(maxLength: 200),
                        WrongAnswer3 = c.String(maxLength: 200),
                        Type = c.Byte(),
                        Audio = c.String(maxLength: 500),
                        Explanation = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LessonQuizs", "LessonID", "dbo.Lessons");
            DropForeignKey("dbo.LessonGrammars", "LessonID", "dbo.Lessons");
            DropForeignKey("dbo.LessonGrammars", "GrammarID", "dbo.Grammars");
            DropForeignKey("dbo.UserGrammars", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserWords", "WordID", "dbo.Words");
            DropForeignKey("dbo.LessonWords", "WordID", "dbo.Words");
            DropForeignKey("dbo.LessonWords", "LessonID", "dbo.Lessons");
            DropForeignKey("dbo.UserWords", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserStats", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.UserLessons", "User_ID", "dbo.Users");
            DropForeignKey("dbo.UserLessons", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserLessons", "LessonID", "dbo.Lessons");
            DropForeignKey("dbo.UserKanjis", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserKanjis", "KanjiID", "dbo.Kanjis");
            DropForeignKey("dbo.LessonKanjis", "LessonID", "dbo.Lessons");
            DropForeignKey("dbo.LessonKanjis", "KanjiID", "dbo.Kanjis");
            DropForeignKey("dbo.UserCourses", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserCourses", "CourseID", "dbo.Courses");
            DropForeignKey("dbo.UserGrammars", "GrammarID", "dbo.Grammars");
            DropForeignKey("dbo.Lessons", "CourseID", "dbo.Courses");
            DropIndex("dbo.LessonQuizs", new[] { "LessonID" });
            DropIndex("dbo.LessonWords", new[] { "WordID" });
            DropIndex("dbo.LessonWords", new[] { "LessonID" });
            DropIndex("dbo.UserWords", new[] { "WordID" });
            DropIndex("dbo.UserWords", new[] { "UserID" });
            DropIndex("dbo.UserStats", new[] { "UserID" });
            DropIndex("dbo.UserRoles", new[] { "RoleID" });
            DropIndex("dbo.UserRoles", new[] { "UserID" });
            DropIndex("dbo.UserLessons", new[] { "User_ID" });
            DropIndex("dbo.UserLessons", new[] { "LessonID" });
            DropIndex("dbo.UserLessons", new[] { "UserID" });
            DropIndex("dbo.LessonKanjis", new[] { "KanjiID" });
            DropIndex("dbo.LessonKanjis", new[] { "LessonID" });
            DropIndex("dbo.UserKanjis", new[] { "KanjiID" });
            DropIndex("dbo.UserKanjis", new[] { "UserID" });
            DropIndex("dbo.UserCourses", new[] { "CourseID" });
            DropIndex("dbo.UserCourses", new[] { "UserID" });
            DropIndex("dbo.UserGrammars", new[] { "GrammarID" });
            DropIndex("dbo.UserGrammars", new[] { "UserID" });
            DropIndex("dbo.LessonGrammars", new[] { "GrammarID" });
            DropIndex("dbo.LessonGrammars", new[] { "LessonID" });
            DropIndex("dbo.Lessons", new[] { "CourseID" });
            DropTable("dbo.JLPTQuestions");
            DropTable("dbo.LessonQuizs");
            DropTable("dbo.LessonWords");
            DropTable("dbo.Words");
            DropTable("dbo.UserWords");
            DropTable("dbo.UserStats");
            DropTable("dbo.Roles");
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserLessons");
            DropTable("dbo.LessonKanjis");
            DropTable("dbo.Kanjis");
            DropTable("dbo.UserKanjis");
            DropTable("dbo.UserCourses");
            DropTable("dbo.Users");
            DropTable("dbo.UserGrammars");
            DropTable("dbo.Grammars");
            DropTable("dbo.LessonGrammars");
            DropTable("dbo.Lessons");
            DropTable("dbo.Courses");
        }
    }
}
