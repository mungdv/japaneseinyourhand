

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class UserLesson
    {
        [Key, Column(Order = 0)]
        [ForeignKey("User")]
        public int UserID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Lesson")]
        public int LessonID { get; set; }

        public int Score { get; set; }

        public int Progress { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime CompleteDate { get; set; }

        public DateTime LastLearnedDate { get; set; }
    
        public virtual Lesson Lesson { get; set; }

        public virtual User User { get; set; }
    }
}
