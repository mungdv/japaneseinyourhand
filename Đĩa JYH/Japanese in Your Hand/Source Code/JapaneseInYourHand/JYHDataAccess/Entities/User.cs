

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class User
    {
        public User()
        {
            this.UserKanjis = new List<UserKanji>();
            this.UserLessons = new List<UserLesson>();
            this.UserWords = new List<UserWord>();
            this.UserRoles = new List<UserRole>();
            this.UserStats = new List<UserStat>();
            this.UserGrammars = new List<UserGrammar>();
            this.UserCourses = new List<UserCourse>();
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(255)]
        public string Email { get; set; }

        [MaxLength(255)]
        public string Password { get; set; }
        [MaxLength(100)]
        public string FullName { get; set; }

        [MaxLength(15)]
        public string Phone { get; set; }

        [MaxLength(2000)]
        public string Address { get; set; }

        public Nullable<byte> Level { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastLogin { get; set; }

        public string IdentifierID { get; set; }

        public string Token { get; set; }

        public bool Available { get; set; }

        public virtual List<UserKanji> UserKanjis { get; set; }

        public virtual List<UserLesson> UserLessons { get; set; }

        public virtual List<UserCourse> UserCourses { get; set; }

        public virtual List<UserWord> UserWords { get; set; }

        public virtual List<UserGrammar> UserGrammars { get; set; }

        public virtual List<UserRole> UserRoles { get; set; }

        public virtual List<UserStat> UserStats { get; set; }
    }
}
