﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public class UserStat
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("User")]
        public int UserID { get; set; }

        public int CorrectKanji { get; set; }

        public int WrongKanji { get; set; }

        public int CorrectWord { get; set; }

        public int WrongWord { get; set; }

        public int CorrectGrammar { get; set; }

        public int WrongGrammar { get; set; }

        public int CorrectReading { get; set; }

        public int WrongReading { get; set; }

        public int CorrectListening { get; set; }

        public int WrongListening { get; set; }

        public int TotalTime { get; set; }

        public virtual User User { get; set; }
    }
}
