﻿


namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    public class UserRole
    {
        [Key, Column(Order = 0)]
        [ForeignKey("User")]
        public int UserID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Role")]
        public int RoleID { get; set; }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}
