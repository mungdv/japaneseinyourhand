

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class LessonWord
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Lesson")]
        public int LessonID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Word")]
        public int WordID { get; set; }
    
        public virtual Lesson Lesson { get; set; }

        public virtual Word Word { get; set; }
    }
}
