

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class JLPTQuestion
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public Nullable<byte> Level { get; set; }

        public int QuestionType { get; set; }

        public string Question { get; set; }

        [MaxLength(200)]
        public string CorrectAnswer { get; set; }

        [MaxLength(200)]
        public string WrongAnswer1 { get; set; }

        [MaxLength(200)]
        public string WrongAnswer2 { get; set; }

        [MaxLength(200)]
        public string WrongAnswer3 { get; set; }
		
		public Nullable<byte> Type { get; set; }

        [MaxLength(500)]
        public string Audio { get; set; }

        public string Explanation { get; set; }
    }
}
