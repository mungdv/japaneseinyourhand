--CREATE DATABASE [Capstone]

USE [Capstone]
GO
CREATE TABLE [User]
(
	[ID] int IDENTITY(1,1) NOT NULL,
	[Email] nvarchar(255) NOT NULL,
	[Password] nvarchar(255) NOT NULL,
	[Role] tinyint NOT NULL,
	[Available] bit NOT NULL,
	CONSTRAINT PK_User PRIMARY KEY (ID)
);

CREATE TABLE [UserDetail]
(
	[UserID] int,
	[Avatar] nvarchar(255),
	[FullName] nvarchar(255),
	[Phone] varchar(11),
	[Address] nvarchar(255),
	CONSTRAINT FK_UserDetail_User FOREIGN KEY ([UserID])
    REFERENCES [User]([ID])
);

CREATE TABLE [Grammar]
(
	[ID] int IDENTITY(1,1) NOT NULL,
	[Structure] nvarchar(500) NOT NULL,
	[Description] nvarchar(2000) NOT NULL,
	[Usage] nvarchar(2000) NOT NULL,
	[Level] tinyint,
	[Example] nvarchar(2000),
	[Note] nvarchar(500),
	[Available] bit NOT NULL,
	CONSTRAINT PK_Grammar PRIMARY KEY (ID)
);

CREATE TABLE [Kanji]
(
	[ID] int IDENTITY(1,1) NOT NULL,
	[Kanji] nvarchar(1),
	[Onyomi] nvarchar(255),
	[Kunyomi] nvarchar(255),
	[MeanE] nvarchar(1000),
	[MeanV] nvarchar(1000) NOT NULL,
	[StrokeCount] tinyint NOT NULL,
	[StrokeOrder] nvarchar(255),
	[SinoV] nvarchar(50) NOT NULL,
	[Available] bit NOT NULL,
	CONSTRAINT PK_Kanji PRIMARY KEY (ID)
);

CREATE TABLE [Word]
(
	[ID] int IDENTITY(1,1) NOT NULL,
	[Word] nvarchar(255) NOT NULL,
	[Reading] nvarchar(255),
	[Type] nvarchar(100),
	[MeanE] nvarchar(1000),
	[MeanV] nvarchar(1000) NOT NULL,
	[Available] bit NOT NULL,
	CONSTRAINT PK_Word PRIMARY KEY (ID)
);

CREATE TABLE [Course]
(
	[ID] int IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(255) NOT NULL,
	[Creator] int,
	[Description] nvarchar(2000) NOT NULL,
	[Available] bit NOT NULL,
	CONSTRAINT PK_Course PRIMARY KEY (ID),
	CONSTRAINT FK_Course_User FOREIGN KEY ([Creator])
    REFERENCES [User]([ID])
);



CREATE TABLE [Lesson]
(
	[ID] int IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(255) NOT NULL,
	[Description] nvarchar(2000) NOT NULL,
	[Creator] int,
	[CourseID] int,
	[Available] bit NOT NULL,
	CONSTRAINT PK_Lesson PRIMARY KEY (ID),
	CONSTRAINT FK_Lesson_User FOREIGN KEY ([Creator])
    REFERENCES [User]([ID]),
    CONSTRAINT FK_Lesson_Course FOREIGN KEY ([CourseID])
    REFERENCES [Course]([ID])
);

CREATE TABLE [LessonKanji]
(
	[LessonID] int,
	[KanjiID] int,
	[Available] bit NOT NULL,
	CONSTRAINT FK_LessonKanji_Lession FOREIGN KEY ([LessonID])
    REFERENCES [Lesson]([ID]),
    CONSTRAINT FK_LessonKanji_Kanji FOREIGN KEY ([KanjiID])
    REFERENCES [Kanji]([ID])
    
);

CREATE TABLE [LessonWord]
(
	[LessonID] int,
	[WordID] int,
	[Available] bit NOT NULL,
	CONSTRAINT FK_LessonWord_Lesson FOREIGN KEY ([LessonID])
    REFERENCES [Lesson]([ID]),
    CONSTRAINT FK_LessonWord_Word FOREIGN KEY ([WordID])
    REFERENCES [Word]([ID])
);

CREATE TABLE [LessonGrammar]
(
	[LessonID] int,
	[GrammarID] int,
	[Available] bit NOT NULL,
	CONSTRAINT FK_LessonGrammar_Lesson FOREIGN KEY ([LessonID])
    REFERENCES [Lesson]([ID]),
    CONSTRAINT FK_LessonGrammar_Grammar FOREIGN KEY ([GrammarID])
    REFERENCES [Grammar]([ID])
);


